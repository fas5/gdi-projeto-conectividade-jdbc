-- CRIACAO DOS TIPOS

DROP TABLE tb_presta;
DROP TABLE tb_pagamento;
DROP TABLE tb_servico_escrevente_cliente;
DROP TABLE tb_atendimento;
DROP TABLE tb_servico;
DROP TABLE tb_tabeliao;
DROP TABLE tb_escrevente;
DROP TABLE tb_pessoafisica;
DROP TABLE tb_pessoajuridica;

DROP SEQUENCE tb_pessoa_seq;
DROP SEQUENCE tb_servico_seq;
DROP SEQUENCE tb_atendimento_seq;

DROP TYPE tp_presta force;
DROP TYPE tp_pagamento force;
DROP TYPE tp_servico_escrevente_cliente force;
DROP TYPE tp_atendimento force;
DROP TYPE tp_servico force;
DROP TYPE tp_tabeliao force;
DROP TYPE tp_escrevente force;
DROP TYPE tp_funcionario force;
DROP TYPE tp_pessoafisica force;
DROP TYPE tp_pessoajuridica force;
DROP TYPE tp_telefones force;
DROP TYPE tp_telefone force;
DROP TYPE tp_pessoa force;
DROP TYPE tp_endereco force;
DROP TYPE tp_nt_sec force;

-- ENDERECO
CREATE OR REPLACE TYPE tp_endereco AS OBJECT (
	Logradouro VARCHAR2(200),
	Numero VARCHAR2(5),
	Bairro VARCHAR2(200),
	Cidade VARCHAR2(100),
	Uf VARCHAR2(2),
	Cep VARCHAR2(8)
);
/

-- TELEFONE
CREATE OR REPLACE TYPE tp_telefone AS OBJECT(
	DDD VARCHAR2(2),
	Numero VARCHAR2(9)
);
/
CREATE OR REPLACE TYPE tp_telefones AS VARRAY(3) OF tp_telefone;
/

-- PESSOA
CREATE OR REPLACE TYPE tp_pessoa AS OBJECT (
  Id INTEGER,
  Nome VARCHAR2 (150),
  DataNascimento DATE,
  Endereco tp_endereco,
  Telefones tp_telefones
) NOT FINAL NOT INSTANTIABLE;
/

--CLIENTE (DEPRECATED, IGNORAR)

-- PESOA FISICA
CREATE OR REPLACE TYPE tp_pessoafisica UNDER tp_pessoa(
	CPF VARCHAR2(11),
	Sexo CHAR,
	Renda NUMBER(15,2),
	Profissao VARCHAR2 (200)
);
/

-- PESSOA JURIDICA
CREATE OR REPLACE TYPE tp_pessoajuridica UNDER tp_pessoa(
	CNPJ VARCHAR2(14),
	AreaAtuacao VARCHAR2(200)
);
/

-- FUNCIONARIO
CREATE OR REPLACE TYPE tp_funcionario UNDER tp_pessoa(
  Matricula INTEGER,
  Supervisor REF tp_funcionario,
  Salario NUMBER(8,2),
  DataAdmissao DATE,
  Foto BLOB
) NOT FINAL NOT INSTANTIABLE;
/

-- ESCREVENTE
CREATE OR REPLACE TYPE tp_escrevente UNDER tp_funcionario(
);
/

-- SERVICO
CREATE OR REPLACE TYPE tp_servico AS OBJECT (
	Codigo INTEGER,
	Descricao VARCHAR2(500),
	Preco NUMBER(10,2),
	Autorizado NUMBER(1)
);
/

-- ATENDIMENTO
CREATE OR REPLACE TYPE tp_atendimento AS OBJECT(
	Id INTEGER,
	Quantidade number(3),
	Desconto number(3),
	Servico REF tp_servico
);
/

-- SERVICO-ESCREVENTE-CLIENTE
CREATE OR REPLACE TYPE tp_servico_escrevente_cliente AS OBJECT(
	Escrevente REF tp_escrevente,
	Atendimento REF tp_atendimento,
	Cliente REF tp_pessoa,
	Datahora DATE
);
/

CREATE OR REPLACE TYPE tp_nt_sec AS TABLE OF REF tp_servico_escrevente_cliente;
/

-- TABELIAO
CREATE OR REPLACE TYPE tp_tabeliao UNDER tp_funcionario(
	Funcao VARCHAR2(200),
	ServicoEscreventeCliente tp_nt_sec
);
/

-- PAGAMENTO
CREATE OR REPLACE TYPE tp_pagamento AS OBJECT(
	Atendimento REF tp_atendimento,
	Datahora DATE
);
/

-- PRESTA
CREATE OR REPLACE TYPE tp_presta AS OBJECT(
	Escrevente REF tp_escrevente,
	Atendimento REF tp_atendimento,
	Cliente REF tp_pessoa,
	DataHora DATE
);
/