package controladores;

import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import negocio.AtendimentoNegocio;
import comunicacao.OracleConnection;
import entidades.RelatorioAtendimento;

public class ControladorRelatorios {	
	public List<RelatorioAtendimento> buscarPorData(String initialDate, String endDate) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		AtendimentoNegocio atendimentoDados = new AtendimentoNegocio(oracle);
		List<RelatorioAtendimento> result = atendimentoDados.buscarPorData(initialDate, endDate);
		Iterator<RelatorioAtendimento> i = result.iterator();
		while(i.hasNext()){
			RelatorioAtendimento ra = i.next();
		}
		return result;
	}
	public List<RelatorioAtendimento> buscarPorCodigo(int codigo) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		AtendimentoNegocio atendimentoDados = new AtendimentoNegocio(oracle);
		List<RelatorioAtendimento> result = atendimentoDados.buscarPorCodigo(codigo);
		Iterator<RelatorioAtendimento> i = result.iterator();
		while(i.hasNext()){
			RelatorioAtendimento ra = i.next();
		}
		return result;
	}
	public List<RelatorioAtendimento> buscarPorEscrevente(int matricula) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		AtendimentoNegocio atendimentoDados = new AtendimentoNegocio(oracle);
		List<RelatorioAtendimento> result = atendimentoDados.buscarPorEscrevente(matricula);
		Iterator<RelatorioAtendimento> i = result.iterator();
		while(i.hasNext()){
			RelatorioAtendimento ra = i.next();
		}
		return result;
	}	
	public List<RelatorioAtendimento> buscarPorTabeliao(int matricula) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		AtendimentoNegocio atendimentoDados = new AtendimentoNegocio(oracle);
		List<RelatorioAtendimento> result = atendimentoDados.buscarPorTabeliao(matricula);
		Iterator<RelatorioAtendimento> i = result.iterator();
		while(i.hasNext()){
			RelatorioAtendimento ra = i.next();
		}
		return result;
	}
	public List<RelatorioAtendimento> buscarPorPessoaFisica(String cpf) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		AtendimentoNegocio atendimentoDados = new AtendimentoNegocio(oracle);
		List<RelatorioAtendimento> result = atendimentoDados.buscarPorPessoaFisica(cpf);
		Iterator<RelatorioAtendimento> i = result.iterator();
		while(i.hasNext()){
			RelatorioAtendimento ra = i.next();
		}
		return result;
	}
	public List<RelatorioAtendimento> buscarPorPessoaJuridica(String cnpj) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		AtendimentoNegocio atendimentoDados = new AtendimentoNegocio(oracle);
		List<RelatorioAtendimento> result = atendimentoDados.buscarPorPessoaJuridica(cnpj);
		Iterator<RelatorioAtendimento> i = result.iterator();
		while(i.hasNext()){
			RelatorioAtendimento ra = i.next();
		}
		return result;
	}
}
