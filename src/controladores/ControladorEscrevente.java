package controladores;

import java.sql.SQLException;
import java.util.List;

import negocio.EscreventeNegocio;
import negocio.TabeliaoNegocio;
import comunicacao.OracleConnection;
import entidades.Endereco;
import entidades.Escrevente;
import entidades.Telefone;

public class ControladorEscrevente {
	
	public void cadastrar(String nome, String dataNascimento, String logradouro, String numero, String bairro, String cidade, String uf, String cep, Telefone[] telefones, int matricula, int matriculaSupervisor, double salario, String dataAdmissao, byte[] foto) throws SQLException{
		
		Endereco endereco = new Endereco(logradouro, numero, bairro, cidade, uf, cep);
		Escrevente escreventeSupervisor = new Escrevente(0, null, null, null, null, matriculaSupervisor, null, 0, null, null);
		Escrevente escrevente = new Escrevente(0, nome, dataNascimento, endereco, telefones, matricula, escreventeSupervisor, salario, dataAdmissao, foto);
		OracleConnection oracle = new OracleConnection();
		EscreventeNegocio escreventeDados = new EscreventeNegocio(oracle);
		escreventeDados.inserir(escrevente);
	}
	
	public void atualizar(String nome, String dataNascimento, String logradouro, String numero, String bairro, String cidade, String uf, String cep, Telefone[] telefones, int matricula, int matriculaSupervisor, double salario, String dataAdmissao, byte[] foto) throws SQLException{
		
		Endereco endereco = new Endereco(logradouro, numero, bairro, cidade, uf, cep);
		Escrevente escreventeSupervisor = new Escrevente(0, null, null, null, null, matriculaSupervisor, null, 0, null, null);
		Escrevente escrevente = new Escrevente(0, nome, dataNascimento, endereco, telefones, matricula, escreventeSupervisor, salario, dataAdmissao, foto);
		OracleConnection oracle = new OracleConnection();
		EscreventeNegocio escreventeDados = new EscreventeNegocio(oracle);
		escreventeDados.atualizar(escrevente);
	}
	
	public List<Escrevente> buscarMatriculas() throws SQLException{
		OracleConnection oracle = new OracleConnection();
		EscreventeNegocio escreventeDados = new EscreventeNegocio(oracle);
		
		return escreventeDados.buscarTodasMatriculas();
	}
	
	public List<Object> buscarPorNome(String nome) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		EscreventeNegocio escreventeDados = new EscreventeNegocio(oracle);
		
		String nomeLower = nome.toLowerCase();
		
		List<Object> lista = escreventeDados.buscarPorNome(nomeLower);
		
		return lista;
	}
	
	public Object buscar(int matricula) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		EscreventeNegocio escreventeDados = new EscreventeNegocio(oracle);
		
		Object escrevente = escreventeDados.buscar(matricula);
		
		return escrevente;
	}
	
	public void remover(int matricula) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		EscreventeNegocio escreventeDados = new EscreventeNegocio(oracle);
		escreventeDados.remover(matricula);
	}
}
