package controladores;

import java.sql.SQLException;
import java.util.List;

import negocio.PessoaFisicaNegocio;
import comunicacao.OracleConnection;
import entidades.Endereco;
import entidades.Pessoa;
import entidades.PessoaFisica;
import entidades.Telefone;

public class ControladorPessoaFisica {
	
	OracleConnection oracle;
	PessoaFisicaNegocio pessoaFisicaDados;
	
	public ControladorPessoaFisica() throws SQLException {
		 oracle = new OracleConnection();
		 pessoaFisicaDados = new PessoaFisicaNegocio(oracle);
	}
	
	public void atualizar (String nome, String dataNascimento, String cpf, char sexo, double renda,
			String profissao, String logradouro, String numero, String bairro, String cidade, String uf, String cep, Telefone [] telefones){
		Endereco endereco = new Endereco(logradouro, numero, bairro, cidade, uf, cep);
		
		PessoaFisica pessoaFisica = new PessoaFisica(0, nome, dataNascimento, endereco, telefones, cpf, sexo, renda, profissao);
		
		try {
			pessoaFisicaDados.atualizar(pessoaFisica);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void cadastrar(String nome, String dataNascimento, String cpf, char sexo, double renda,
		String profissao, String logradouro, String numero, String bairro, String cidade, String uf, String cep, Telefone [] telefones){
		
		Endereco endereco = new Endereco(logradouro, numero, bairro, cidade, uf, cep);
		
		PessoaFisica pessoaFisica = new PessoaFisica(0, nome, dataNascimento, endereco, telefones, cpf, sexo, renda, profissao);
		
		try {
			pessoaFisicaDados.inserir(pessoaFisica);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Object> pesquisarPorNome(String nome) {
		List<Object> listaPessoas = null;
		
		try {
			listaPessoas = pessoaFisicaDados.buscarPorNome((String) nome);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listaPessoas;
	}
	
	public Object pesquisarPorCpf(String cpf) {
		Object pf = null;
		
		try {
			pf = pessoaFisicaDados.buscar(cpf);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pf;
	}

	public void deletar(String cpf) {
		try {
			pessoaFisicaDados.remover(cpf);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
