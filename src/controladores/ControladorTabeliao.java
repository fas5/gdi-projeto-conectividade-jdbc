package controladores;

import java.sql.SQLException;
import java.util.List;

import negocio.EscreventeNegocio;
import negocio.ServicoNegocio;
import negocio.TabeliaoNegocio;
import comunicacao.OracleConnection;
import entidades.Endereco;
import entidades.Tabeliao;
import entidades.Telefone;

public class ControladorTabeliao {

	public void cadastrar(String nome, String dataNascimento, String logradouro, String numero, String bairro, String cidade, String uf, String cep, Telefone[] telefones, int matricula, int matriculaSupervisor, double salario, String dataAdmissao, String funcao, byte[] foto) throws SQLException{
		
		Endereco endereco = new Endereco(logradouro, numero, bairro, cidade, uf, cep);
		Tabeliao tabeliaoSupervisor = new Tabeliao(0, null, null, null, null, matriculaSupervisor, null, 0, null, null, null, null);
		Tabeliao tabeliao = new Tabeliao(0, nome, dataNascimento, endereco, telefones, matricula, tabeliaoSupervisor, salario, dataAdmissao, funcao, null, foto);
		OracleConnection oracle = new OracleConnection();
		TabeliaoNegocio tabeliaoDados = new TabeliaoNegocio(oracle);
		tabeliaoDados.inserir(tabeliao);
	}
	
	public void atualizar(String nome, String dataNascimento, String logradouro, String numero, String bairro, String cidade, String uf, String cep, Telefone[] telefones, int matricula, int matriculaSupervisor, double salario, String dataAdmissao, String funcao, byte[] foto) throws SQLException{
		
		Endereco endereco = new Endereco(logradouro, numero, bairro, cidade, uf, cep);
		Tabeliao tabeliaoSupervisor = new Tabeliao(0, null, null, null, null, matriculaSupervisor, null, 0, null, null, null, null);
		Tabeliao tabeliao = new Tabeliao(0, nome, dataNascimento, endereco, telefones, matricula, tabeliaoSupervisor, salario, dataAdmissao, funcao, null, foto);
		OracleConnection oracle = new OracleConnection();
		TabeliaoNegocio tabeliaoDados = new TabeliaoNegocio(oracle);
		tabeliaoDados.atualizar(tabeliao);
	}
	
	public List<Tabeliao> buscarMatriculas() throws SQLException{
		OracleConnection oracle = new OracleConnection();
		TabeliaoNegocio tabeliaoDados = new TabeliaoNegocio(oracle);
		
		return tabeliaoDados.buscarTodasMatriculas();
	}
	
	public List<Object> buscarPorNome(String nome) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		TabeliaoNegocio tabeliaoDados = new TabeliaoNegocio(oracle);
		
		String nomeLower = nome.toLowerCase();
		
		List<Object> lista = tabeliaoDados.buscarPorNome(nomeLower);
		
		return lista;
	}
	
	public Object buscar(int matricula) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		TabeliaoNegocio tabeliaoDados = new TabeliaoNegocio(oracle);
		
		Object tabeliao = tabeliaoDados.buscar(matricula);
		
		return tabeliao;
	}
	
	public void remover(int matricula) throws SQLException{
		OracleConnection oracle = new OracleConnection();
		TabeliaoNegocio tabeliaoDados = new TabeliaoNegocio(oracle);
		tabeliaoDados.remover(matricula);
	}

}
