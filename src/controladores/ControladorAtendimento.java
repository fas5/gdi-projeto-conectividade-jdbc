package controladores;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import negocio.AtendimentoNegocio;
import negocio.EscreventeNegocio;
import negocio.PessoaFisicaNegocio;
import negocio.PrestaNegocio;
import negocio.ServicoEscreventeClienteNegocio;
import negocio.TabeliaoNegocio;
import comunicacao.OracleConnection;
import entidades.Atendimento;
import entidades.Cliente;
import entidades.Escrevente;
import entidades.PessoaFisica;
import entidades.PessoaJuridica;
import entidades.Presta;
import entidades.Servico;
import entidades.ServicoEscreventeCliente;
import entidades.Tabeliao;

public class ControladorAtendimento {
	OracleConnection oracle;
	PessoaFisicaNegocio pessoaFisicaDados;
	AtendimentoNegocio atendimentoDados;
	EscreventeNegocio escreventeDados;
	TabeliaoNegocio tabeliaoDados;
	ServicoEscreventeClienteNegocio secDados;
	PrestaNegocio prestaDados;
	
	public ControladorAtendimento() throws SQLException {
			oracle = new OracleConnection();
			atendimentoDados = new AtendimentoNegocio(oracle);
			prestaDados = new PrestaNegocio(oracle);
			tabeliaoDados = new TabeliaoNegocio(oracle);
			secDados = new ServicoEscreventeClienteNegocio(oracle);
	}

	public void inserirAtendimentoPessoaFisicaJuridica(PessoaFisica pf, PessoaJuridica pj, Escrevente escrevente, Tabeliao tabeliao, Servico servico, double preco, int quantidade, int desconto) throws SQLException{

		Atendimento atendimento = new Atendimento(0, quantidade, desconto, servico);
		atendimentoDados.inserir(atendimento);
		int idAtendiemnto = atendimentoDados.maximoId();
		
		atendimento.setId(idAtendiemnto);
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm"); 
		Date date = new Date(); 
		String dataHora = dateFormat.format(date);
		
		Cliente cliente;
		if(pf != null){
			cliente = pf;
		}else{
			cliente = pj;
		}
		
		Presta presta = new Presta(escrevente, atendimento, cliente, dataHora);
		
		prestaDados.inserir(presta);
		
		if (tabeliao != null){
			ServicoEscreventeCliente sec = new ServicoEscreventeCliente(escrevente, atendimento, cliente, date);
			secDados.inserir(sec);
			tabeliaoDados.inserirNestedTable(tabeliao, atendimento.getId());
		}

	}

}
