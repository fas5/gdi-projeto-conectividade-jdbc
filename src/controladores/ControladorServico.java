package controladores;

import java.sql.SQLException;
import java.util.List;

import negocio.ServicoNegocio;
import comunicacao.OracleConnection;
import entidades.Servico;

public class ControladorServico {
	
	private OracleConnection oracle;
	private ServicoNegocio servicoDados;
	
	public ControladorServico() throws SQLException {
		this.oracle = new OracleConnection();
		this.servicoDados = new ServicoNegocio(oracle);
	}
	
	public void cadastrar(String nome, double preco, int autorizado) throws SQLException {
		Servico servico = new Servico (0, nome, preco, autorizado);
		servicoDados.inserir(servico);	
	}
	
	public void atualizar(int codigo, String nome, double preco, int autorizado) throws SQLException {
		Servico servico = new Servico (codigo, nome, preco, autorizado);
		servicoDados.atualizar(servico);	
	}
	
	public List<Object> buscarTodos() throws SQLException {
		return servicoDados.buscarTodos();	
	}
	
	public void remover(int codigo) throws SQLException {
		servicoDados.remover(codigo);
	}

}
