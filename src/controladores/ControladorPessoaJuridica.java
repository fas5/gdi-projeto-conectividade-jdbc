/**
 * 
 */
package controladores;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import negocio.PessoaFisicaNegocio;
import negocio.PessoaJuridicaNegocio;
import comunicacao.OracleConnection;
import entidades.Endereco;
import entidades.PessoaJuridica;
import entidades.Telefone;

/**
 * @author Felipe
 *
 */
public class ControladorPessoaJuridica {

	OracleConnection oracle;
	PessoaJuridicaNegocio pessoaJuridicaDados;
	
	public ControladorPessoaJuridica() throws SQLException {
		this.oracle = new OracleConnection();
		this.pessoaJuridicaDados = new PessoaJuridicaNegocio(oracle);
	}
	
	public void atualizar(String razaoSocial, String dataFundacao, String cnpj, String areaAtuacao,
			Endereco endereco,
			String telefone1, String telefone2, String telefone3) throws SQLException {
		
		List<Telefone> telefonesList = new ArrayList<Telefone>();
		
		if(telefone1.length() != 0){
			preencherTelefones(telefonesList, telefone1);
		}

		if(telefone2.length() != 0){
			preencherTelefones(telefonesList, telefone2);
		}
		
		if(telefone3.length() != 0){
			preencherTelefones(telefonesList, telefone3);
		}
		
		Telefone [] telefones = new Telefone[telefonesList.size()];
		telefonesList.toArray(telefones);	
		
		PessoaJuridica pj = new PessoaJuridica(0, razaoSocial, dataFundacao, endereco, telefones, cnpj, areaAtuacao);

		pessoaJuridicaDados.atualizar(pj);
	}
	
	public void cadastrar(String razaoSocial, String dataFundacao, String cnpj, String areaAtuacao,
			Endereco endereco,
			String telefone1, String telefone2, String telefone3) throws SQLException {
		
		List<Telefone> telefonesList = new ArrayList<Telefone>();
		
		if(telefone1.length() != 0){
			preencherTelefones(telefonesList, telefone1);
		}

		if(telefone2.length() != 0){
			preencherTelefones(telefonesList, telefone2);
		}
		
		if(telefone3.length() != 0){
			preencherTelefones(telefonesList, telefone3);
		}
		
		Telefone [] telefones = new Telefone[telefonesList.size()];
		telefonesList.toArray(telefones);	
		
		PessoaJuridica pj = new PessoaJuridica(0, razaoSocial, dataFundacao, endereco, telefones, cnpj, areaAtuacao);

		pessoaJuridicaDados.inserir(pj);
	}
	
	public List<Object> pesquisarPorNome(String nome) {
		List<Object> listaPessoas = null;
		
		try {
			listaPessoas = pessoaJuridicaDados.buscarPorNome((String) nome);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listaPessoas;
	}
	
	public Object pesquisarPorCnpj(String cnpj) {
		Object pf = null;
		
		try {
			pf = pessoaJuridicaDados.buscar(cnpj);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pf;
	}	
	
	public void deletar(String cnpj) throws SQLException {
		pessoaJuridicaDados.remover(cnpj);
	}
	
	/** UTIL **/
	public void preencherTelefones(List<Telefone> telefones, String telefone) {
		String ddd = telefone.substring(0, 2);
		String numero = telefone.substring(2, telefone.length());
		Telefone telefoneCompleto = new Telefone(ddd, numero);
		telefones.add(telefoneCompleto);
	}
}
