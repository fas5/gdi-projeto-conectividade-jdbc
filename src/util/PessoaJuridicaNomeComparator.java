/**
 * 
 */
package util;

import java.util.Comparator;

import entidades.PessoaFisica;
import entidades.PessoaJuridica;

/**
 * @author Felipe
 *
 */
public class PessoaJuridicaNomeComparator implements Comparator<Object> {
	
	@Override
	public int compare(Object pf1, Object pf2) {
		return ((PessoaJuridica)pf1).getNome().compareTo(((PessoaJuridica)pf2).getNome());
	}
	
}
