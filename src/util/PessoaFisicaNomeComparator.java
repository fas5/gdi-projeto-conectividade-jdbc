/**
 * 
 */
package util;

import java.util.Comparator;

import entidades.PessoaFisica;

/**
 * @author Felipe
 *
 */
public class PessoaFisicaNomeComparator implements Comparator<Object> {
	
	@Override
	public int compare(Object pf1, Object pf2) {
		return ((PessoaFisica)pf1).getNome().compareTo(((PessoaFisica)pf2).getNome());
	}
	
}
