package entidades;

import java.util.List;

public class Tabeliao extends Funcionario{
	private int matricula;
	private Tabeliao supervisor;
	private double salario;
	private String dataAdmissao;
	private String funcao;
	private List<ServicoEscreventeCliente> listaSec;
	
	public Tabeliao(int id, String nome, String dataNascimento, Endereco endereco, Telefone[] telefones, int matricula, Tabeliao supervisor, double salario, String dataAdmissao, String funcao, List<ServicoEscreventeCliente> listaSec, byte[] foto){
		super(id, nome, dataNascimento, endereco, telefones, foto);
		this.matricula = matricula;
		this.supervisor = supervisor;
		this.salario = salario;
		this.dataAdmissao = dataAdmissao;
		this.funcao = funcao;
		this.listaSec = listaSec;
		this.foto = foto;
	}

	/**
	 * @return the matricula
	 */
	
	public int getMatricula() {
		return matricula;
	}

	/**
	 * @return the funcao
	 */
	public String getFuncao() {
		return funcao;
	}
	
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	/**
	 * @param funcao the funcao to set
	 */
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	/**
	 * @return the listaSec
	 */
	public List<ServicoEscreventeCliente> getListaSec() {
		return listaSec;
	}

	/**
	 * @param listaSec the listaSec to set
	 */
	public void setListaSec(List<ServicoEscreventeCliente> listaSec) {
		this.listaSec = listaSec;
	}

	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	/**
	 * @return the supervisor
	 */
	public Tabeliao getSupervisor() {
		return supervisor;
	}

	/**
	 * @param supervisor the supervisor to set
	 */
	public void setSupervisor(Tabeliao supervisor) {
		this.supervisor = supervisor;
	}

	/**
	 * @return the salario
	 */
	public double getSalario() {
		return salario;
	}

	/**
	 * @param salario the salario to set
	 */
	public void setSalario(double salario) {
		this.salario = salario;
	}

	/**
	 * @return the dataAdmissao
	 */
	public String getDataAdmissao() {
		return dataAdmissao;
	}

	/**
	 * @param dataAdmissao the dataAdmissao to set
	 */
	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	@Override
	public int getId() {
		return this.id;
	}

	public String getNome() {
		return this.nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String getDataNascimento() {
		return this.dataNascimento;
	}

	@Override
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return this.endereco;
	}

	@Override
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	@Override
	public Telefone[] getTelefones() {
		return this.telefones;
	}

	@Override
	public void setTelefones(Telefone[] telefones) {
		this.telefones = telefones;
	}
}
