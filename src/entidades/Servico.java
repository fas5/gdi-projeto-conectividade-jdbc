package entidades;

public final class Servico {
	
	private int codigo;
	private String descricao;
	private double preco;
	private int autorizado;
	
	public Servico(int codigo, String descricao, double preco, int autorizado){
		this.codigo = codigo;
		this.descricao = descricao;
		this.preco = preco;
		this.autorizado = autorizado;
	}

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the preco
	 */
	public double getPreco() {
		return preco;
	}

	/**
	 * @param preco the preco to set
	 */
	public void setPreco(double preco) {
		this.preco = preco;
	}

	/**
	 * @return the autorizado
	 */
	public int getAutorizado() {
		return autorizado;
	}

	/**
	 * @param autorizado the autorizado to set
	 */
	public void setAutorizado(int autorizado) {
		this.autorizado = autorizado;
	}
	
	

}
