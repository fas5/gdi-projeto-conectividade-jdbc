package entidades;

public final class Escrevente extends Funcionario {
	
	private int matricula;
	private Escrevente supervisor;
	private double salario;
	private String dataAdmissao;
	
	public Escrevente(int id, String nome, String dataNascimento, Endereco endereco, Telefone[] telefones, int matricula, Escrevente supervisor, double salario, String dataAdmissao, byte[] foto){
		super(id, nome, dataNascimento, endereco, telefones, foto);
		this.matricula = matricula;
		this.supervisor = supervisor;
		this.salario = salario;
		this.dataAdmissao = dataAdmissao;
	}
	
	/**
	 * @return the matricula
	 */
	public int getMatricula() {
		return matricula;
	}

	/**
	 * @return the supervisor
	 */
	public Escrevente getSupervisor() {
		return supervisor;
	}

	/**
	 * @param supervisor the supervisor to set
	 */
	public void setSupervisor(Escrevente supervisor) {
		this.supervisor = supervisor;
	}

	/**
	 * @return the salario
	 */
	public double getSalario() {
		return salario;
	}

	/**
	 * @param salario the salario to set
	 */
	public void setSalario(double salario) {
		this.salario = salario;
	}

	/**
	 * @return the dataAdmissao
	 */
	public String getDataAdmissao() {
		return dataAdmissao;
	}

	/**
	 * @param dataAdmissao the dataAdmissao to set
	 */
	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	@Override
	public int getId() {
		return this.id;
	}

	public String getNome() {
		return this.nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String getDataNascimento() {
		return this.dataNascimento;
	}

	@Override
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return this.endereco;
	}

	@Override
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	@Override
	public Telefone[] getTelefones() {
		return this.telefones;
	}

	@Override
	public void setTelefones(Telefone[] telefones) {
		this.telefones = telefones;
	}
	
	@Override
	public byte[] getFoto() {
		return this.foto;
	}
	
	@Override
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

}
