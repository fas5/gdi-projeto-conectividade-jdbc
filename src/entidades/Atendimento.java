package entidades;

public class Atendimento {
	
	private int id;
	private int quantidade;
	private int desconto;
	private Servico servico;
	
	
	public Atendimento(int id, int quantidade, int desconto, Servico servico){
		this.id = id;
		this.desconto = desconto;
		this.quantidade = quantidade;
		this.servico = servico;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the desconto
	 */
	public int getDesconto() {
		return desconto;
	}


	/**
	 * @param desconto the desconto to set
	 */
	public void setDesconto(int desconto) {
		this.desconto = desconto;
	}


	/**
	 * @return the quantidade
	 */
	public int getQuantidade() {
		return quantidade;
	}


	/**
	 * @param quantidade the quantidade to set
	 */
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}


	/**
	 * @return the servico
	 */
	public Servico getServico() {
		return servico;
	}


	/**
	 * @param servico the servico to set
	 */
	public void setServico(Servico servico) {
		this.servico = servico;
	}
	
	

}
