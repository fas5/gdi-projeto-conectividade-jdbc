package entidades;

/**
 * Classe que representa a entidade PESSOA
 *
 * @author fas5
 * @since 18/06/2015
 */
public abstract class Pessoa {

	protected int id;
	protected String nome;
	protected String dataNascimento;
	protected Endereco endereco;
	protected Telefone[] telefones;

	/**
* 
*/
	public Pessoa(int id, String nome, String dataNascimento, Endereco endereco,
			Telefone[] telefones) {
		this.id = id;
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.endereco = endereco;
		this.telefones = telefones;
	}

	/**
	 * Retorna o valor do campo <CODE>id</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>id</CODE> deste objeto.
	 */
	public abstract int getId();

	/**
	 * Retorna o valor do campo <CODE>nome</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>nome</CODE> deste objeto.
	 */
	public abstract String getNome();

	/**
	 * Seta o valor do campo nome deste objeto.
	 *
	 * @param nome
	 *            valor do campo nome
	 */
	public abstract void setNome(String nome);

	/**
	 * Retorna o valor do campo <CODE>dataNascimento</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>dataNascimento</CODE> deste objeto.
	 */
	public abstract String getDataNascimento();

	/**
	 * Seta o valor do campo dataNascimento deste objeto.
	 *
	 * @param dataNascimento
	 *            valor do campo dataNascimento
	 */
	public abstract void setDataNascimento(String dataNascimento);

	/**
	 * Retorna o valor do campo <CODE>endereco</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>endereco</CODE> deste objeto.
	 */
	public abstract Endereco getEndereco();

	/**
	 * Seta o valor do campo endereco deste objeto.
	 *
	 * @param endereco
	 *            valor do campo endereco
	 */
	public abstract void setEndereco(Endereco endereco);

	/**
	 * Retorna o valor do campo <CODE>telefones</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>telefones</CODE> deste objeto.
	 */
	public abstract Telefone[] getTelefones();

	/**
	 * Seta o valor do campo telefones deste objeto.
	 *
	 * @param telefones
	 *            valor do campo telefones
	 */
	public abstract void setTelefones(Telefone[] telefones);
}