
package entidades;


/**
 * Classe que representa a entidade PESSOAJURIDICA
 *
 * @author fas5
 * @since  18/06/2015
 */
public class PessoaJuridica extends Cliente {

	private String cnpj;
	private String areaAtuacao;
	
	/**
	 * Construtor
	 * 
	 * @param id
	 * @param nome
	 * @param dataNascimento
	 * @param endereco
	 * @param telefones
	 */
	public PessoaJuridica(int id, String nome, String dataNascimento, Endereco endereco, Telefone[] telefones,
		String cnpj, String areaAtuacao) {
		super(id, nome, dataNascimento, endereco, telefones);
		
		this.cnpj = cnpj;
		this.areaAtuacao = areaAtuacao;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#getId()
	 */
	@Override
	public int getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#getNome()
	 */
	@Override
	public String getNome() {
		return this.nome;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#getDataNascimento()
	 */
	@Override
	public String getDataNascimento() {
		return this.dataNascimento;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#setDataNascimento(java.sql.Date)
	 */
	@Override
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#getEndereco()
	 */
	@Override
	public Endereco getEndereco() {
		return this.endereco;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#setEndereco(entidades.Endereco)
	 */
	@Override
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#getTelefones()
	 */
	@Override
	public Telefone[] getTelefones() {
		return this.telefones;
	}

	/* (non-Javadoc)
	 * @see entidades.Cliente#setTelefones(entidades.Telefone[])
	 */
	@Override
	public void setTelefones(Telefone[] telefones) {
		this.telefones = telefones;
	}
	
	/**
	 * Retorna o valor do campo <CODE>cnpj</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>cnpj</CODE> deste objeto.
	 */
	public String getCnpj() {
		return this.cnpj;
	}
	
	/**
	 * Seta o valor do campo cnpj deste objeto.
	 *
	 * @param cnpj valor do campo cnpj
	 */
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	/**
	 * Retorna o valor do campo <CODE>areaAtuacao</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>areaAtuacao</CODE> deste objeto.
	 */
	public String getAreaAtuacao() {
		return this.areaAtuacao;
	}

	/**
	 * Seta o valor do campo areaAtuacao deste objeto.
	 *
	 * @param areaAtuacao valor do campo areaAtuacao
	 */
	public void setAreaAtuacao(String areaAtuacao) {
		this.areaAtuacao = areaAtuacao;
	}
}