package entidades;

/**
 * Classe que representa a entidade PESSOAFISICA
 *
 * @author fas5
 * @since 18/06/2015
 */
public final class PessoaFisica extends Cliente {

	private String cpf;
	private char sexo;
	private double renda;
	private String profissao;

	/**
	 * Construtor
	 * 
	 * @param id
	 * @param nome
	 * @param dataNascimento
	 * @param endereco
	 * @param telefones
	 */
	public PessoaFisica(int id, String nome, String dataNascimento, Endereco endereco,
			Telefone[] telefones, String cpf, char sexo, double renda,
			String profissao) {
		super(id, nome, dataNascimento, endereco, telefones);
		this.cpf = cpf;
		this.sexo = sexo;
		this.renda = renda;
		this.telefones = telefones;
		this.profissao = profissao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#getId()
	 */
	@Override
	public int getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#getNome()
	 */
	@Override
	public String getNome() {
		return this.nome;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#getDataNascimento()
	 */
	@Override
	public String getDataNascimento() {
		return this.dataNascimento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#setDataNascimento(java.sql.Date)
	 */
	@Override
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#getEndereco()
	 */
	@Override
	public Endereco getEndereco() {
		return this.endereco;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#setEndereco(entidades.Endereco)
	 */
	@Override
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#getTelefones()
	 */
	@Override
	public Telefone[] getTelefones() {
		return this.telefones;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see entidades.Cliente#setTelefones(entidades.Telefone[])
	 */
	@Override
	public void setTelefones(Telefone[] telefones) {
		this.telefones = telefones;
	}

	/**
	 * Retorna o valor do campo <CODE>cpf</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>cpf</CODE> deste objeto.
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Seta o valor do campo cpf deste objeto.
	 *
	 * @param cpf
	 *            valor do campo cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Retorna o valor do campo <CODE>sexo</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>sexo</CODE> deste objeto.
	 */
	public char getSexo() {
		return sexo;
	}

	/**
	 * Seta o valor do campo sexo deste objeto.
	 *
	 * @param sexo
	 *            valor do campo sexo
	 */
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	/**
	 * Retorna o valor do campo <CODE>renda</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>renda</CODE> deste objeto.
	 */
	public double getRenda() {
		return renda;
	}

	/**
	 * Seta o valor do campo renda deste objeto.
	 *
	 * @param renda
	 *            valor do campo renda
	 */
	public void setRenda(double renda) {
		this.renda = renda;
	}

	/**
	 * Retorna o valor do campo <CODE>profissao</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>profissao</CODE> deste objeto.
	 */
	public String getProfissao() {
		return profissao;
	}

	/**
	 * Seta o valor do campo profissao deste objeto.
	 *
	 * @param profissao
	 *            valor do campo profissao
	 */
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
}
