package entidades;
/**
 * Classe que representa a entidade ENDERECO
 *
 * @author fas5
 * @since  18/06/2015
 */
public class Endereco {

	private int id;
	private String logradouro;
	private String numero;
	private String bairro;
	private String cidade;
	private String uf;
	private String cep;
	/**
	 * Construtor
	 */
	public Endereco(String logradouro, String numero,
		String bairro, String cidade, String uf, String cep ) {
		this.logradouro = logradouro;
		this.numero = numero;
		this.bairro = bairro;
		this.cidade = cidade;
		this.uf = uf;
		this.cep = cep;
	}
	
	/**
	 * Retorna o valor do campo <CODE>id</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>id</CODE> deste objeto.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Seta o valor do campo id deste objeto.
	 *
	 * @param id valor do campo id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Retorna o valor do campo <CODE>logradouro</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>logradouro</CODE> deste objeto.
	 */
	public String getLogradouro() {
		return logradouro;
	}
	
	/**
	 * Seta o valor do campo logradouro deste objeto.
	 *
	 * @param logradouro valor do campo logradouro
	 */
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	/**
	 * Retorna o valor do campo <CODE>numero</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>numero</CODE> deste objeto.
	 */
	public String getNumero() {
		return numero;
	}
	
	/**
	 * Seta o valor do campo numero deste objeto.
	 *
	 * @param numero valor do campo numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	/**
	 * Retorna o valor do campo <CODE>bairro</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>bairro</CODE> deste objeto.
	 */
	public String getBairro() {
		return bairro;
	}
	
	/**
	 * Seta o valor do campo bairro deste objeto.
	 *
	 * @param bairro valor do campo bairro
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}


	/**
	 * Retorna o valor do campo <CODE>cidade</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>cidade</CODE> deste objeto.
	 */
	public String getCidade() {
		return cidade;
	}


	/**
	 * Seta o valor do campo cidade deste objeto.
	 *
	 * @param cidade valor do campo cidade
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}


	/**
	 * Retorna o valor do campo <CODE>uf</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>uf</CODE> deste objeto.
	 */
	public String getUf() {
		return uf;
	}

	/**
	 * Seta o valor do campo uf deste objeto.
	 *
	 * @param uf valor do campo uf
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}
	
	/**
	 * Retorna o valor do campo <CODE>cep</CODE> deste objeto.
	 *
	 * @return O valor do campo <CODE>cep</CODE> deste objeto.
	 */
	public String getCep() {
		return cep;
	}
	
	/**
	 * Seta o valor do campo cep deste objeto.
	 *
	 * @param cep valor do campo cep
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}	
}