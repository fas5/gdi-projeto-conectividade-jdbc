package entidades;

public abstract class Funcionario extends Pessoa{

	protected byte[] foto;
	
	public Funcionario(int id, String nome, String dataNascimento, Endereco endereco,
			Telefone[] telefones, byte[] foto){
		super(id, nome, dataNascimento, endereco, telefones);
		this.foto = foto;
	}
	
	public abstract byte[] getFoto();
	
	public abstract void setFoto(byte[] foto);
}
