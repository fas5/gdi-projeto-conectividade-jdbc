package entidades;

import java.util.Date;

public class ServicoEscreventeCliente {
	
	private Escrevente escrevente;
	private Atendimento atendimento;
	private Cliente cliente;
	private Date dataHora;
	
	public ServicoEscreventeCliente(Escrevente escrevente, Atendimento atendimento, Cliente cliente, Date dataHora){
		this.escrevente = escrevente;
		this.atendimento = atendimento;
		this.cliente = cliente;
		this.dataHora = dataHora;
	}

	/**
	 * @return the escrevente
	 */
	public Escrevente getEscrevente() {
		return escrevente;
	}

	/**
	 * @param escrevente the escrevente to set
	 */
	public void setEscrevente(Escrevente escrevente) {
		this.escrevente = escrevente;
	}

	/**
	 * @return the atendimento
	 */
	public Atendimento getAtendimento() {
		return atendimento;
	}

	/**
	 * @param atendimento the atendimento to set
	 */
	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the dataHora
	 */
	public Date getDataHora() {
		return dataHora;
	}

	/**
	 * @param dataHora the dataHora to set
	 */
	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
}
