package entidades;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RelatorioAtendimento {
	private String pfNome;
	private String eNome;
	private int eMatricula;
	private String sDescricao;
	private double sPreco;
	private int sCodigo;
	private int aQuantidade;
	private int aDesconto;
	private Date prDataHora;
	
	public RelatorioAtendimento(String pfNome, String eNome,
			int eMatricula, String sDescricao, double sPreco,
			int sCodigo, int aQuantidade, int aDesconto, Date prDataHora) {
		this.pfNome = pfNome;
		this.eNome = eNome;
		this.eMatricula = eMatricula;
		this.sDescricao = sDescricao;
		this.sPreco = sPreco;
		this.sCodigo = sCodigo;
		this.aQuantidade = aQuantidade;
		this.aDesconto = aDesconto;
		this.prDataHora = prDataHora;
	}
	
	public RelatorioAtendimento(String pfNome, String eNome,
			String eMatricula, String sDescricao, String sPreco,
			String sCodigo, String aQuantidade, String aDesconto, String prDataHora) throws ParseException {
		this.pfNome = pfNome;
		this.eNome = eNome;
		this.eMatricula = Integer.parseInt(eMatricula);
		this.sDescricao = sDescricao;
		this.sPreco = Double.parseDouble(sPreco);
		this.sCodigo = Integer.parseInt(sCodigo);
		this.aQuantidade = Integer.parseInt(aQuantidade);
		this.aDesconto = Integer.parseInt(aDesconto);
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		this.prDataHora = format.parse(prDataHora);
	}

	@Override
	public String toString() {
		return "RelatorioAtendimento [pfNome=" + pfNome 
				+ ", eNome=" + eNome + ", eMatricula=" + eMatricula
				+ ", sDescricao=" + sDescricao + ", sPreco=" + sPreco
				+ ", sCodigo=" + sCodigo + ", aQuantidade=" + aQuantidade
				+ ", aDesconto=" + aDesconto + ", prDataHora=" + prDataHora
				+ "]";
	}

	public String getPfNome() {
		return pfNome;
	}

	public void setPfNome(String pfNome) {
		this.pfNome = pfNome;
	}

	public String geteNome() {
		return eNome;
	}

	public void seteNome(String eNome) {
		this.eNome = eNome;
	}

	public int geteMatricula() {
		return eMatricula;
	}

	public void seteMatricula(int eMatricula) {
		this.eMatricula = eMatricula;
	}

	public String getsDescricao() {
		return sDescricao;
	}

	public void setsDescricao(String sDescricao) {
		this.sDescricao = sDescricao;
	}

	public double getsPreco() {
		return sPreco;
	}

	public void setsPreco(double sPreco) {
		this.sPreco = sPreco;
	}

	public int getsCodigo() {
		return sCodigo;
	}

	public void setsCodigo(int sCodigo) {
		this.sCodigo = sCodigo;
	}

	public int getaQuantidade() {
		return aQuantidade;
	}

	public void setaQuantidade(int aQuantidade) {
		this.aQuantidade = aQuantidade;
	}

	public int getaDesconto() {
		return aDesconto;
	}

	public void setaDesconto(int aDesconto) {
		this.aDesconto = aDesconto;
	}

	public Date getPrDataHora() {
		return prDataHora;
	}

	public void setPrDataHora(Date prDataHora) {
		this.prDataHora = prDataHora;
	}
	
	
}
