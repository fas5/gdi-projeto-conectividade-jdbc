package enums;

public enum Telas {
	TELA_INICIAL("telaInicial"),
	TELA_ATENDIMENTO("atendimento"),
	CADASTRAR_PESSOA_FISICA("cadastrarPessoaFisica"),
	CADASTRAR_PESSOA_JURIDICA("cadastrarPessoaJuridica"),
	CADASTRAR_ESCREVENTE("cadastrarEscrevente"),
	CADASTRAR_TABELIAO("cadastrarTabeliao"),
	CADASTRAR_SERVICO("cadastrarServico"),
	PESQUISAR_CLIENTE("pesquisarCliente"),
	PESQUISAR_FUNCIONARIO("pesquisarFuncionario"),
	PESQUISAR_SERVICO("pesquisarServico"),
	RELATORIO_POR_DATA("relatorioPorData"),
	RELATORIO_POR_SERVICO("relatorioPorServico"),
	RELATORIO_POR_FUNCIONARIO("relatorioPorFuncionario"),
	RELATORIO_POR_CLIENTE("relatorioPorCliente"),
	EDITAR_ESCREVENTE("editarEscrevente");
	
	private String nomeTela;
	
	Telas(String nomeTela) {
		this.nomeTela = nomeTela;
	}
	
	public String toString() {
		return nomeTela;
	}
}
