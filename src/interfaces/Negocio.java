/**
 * 
 */
package interfaces;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Felipe
 *
 */
public interface Negocio {

	void inserir(Object obj) throws SQLException;
	
	Object buscar(Object obj) throws SQLException;
	
	void atualizar(Object obj) throws SQLException;
	
	void remover(Object obj) throws SQLException;

}