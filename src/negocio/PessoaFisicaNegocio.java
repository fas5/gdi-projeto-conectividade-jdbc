package negocio;
import oracle.sql.STRUCT;
import interfaces.Negocio;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import comunicacao.OracleConnection;
import entidades.Endereco;
import entidades.Escrevente;
import entidades.PessoaFisica;
import entidades.Telefone;

public class PessoaFisicaNegocio implements Negocio{
	
	private OracleConnection oracle;
	private PreparedStatement comando;
	
	public PessoaFisicaNegocio(OracleConnection oracle){
		this.oracle = oracle;
	}
	
	public String preencherTelefones(int qtdTelefones) {		
		String resultado = "tp_telefones(null)";
		
		if (qtdTelefones != 0) {
			resultado = "tp_telefones(";
			String fone = "tp_telefone(?, ?)";
			
			for(int i = 0; i < qtdTelefones; i++){
				resultado += fone;
				
				if(i < qtdTelefones-1) {
					resultado += ", ";
				}
			}
			
			resultado += ")";
		}
		
		return resultado;
	}
	
public void inserir(Object pessoaFisicaObject) throws SQLException {
		
		PessoaFisica pessoaFisica = (PessoaFisica)pessoaFisicaObject;
		
		oracle.conectar();
		
		try {
			
			int indice;
			int tamanho = 0;
			
			tamanho = pessoaFisica.getTelefones().length;
			
			this.comando = oracle.getConexao().prepareStatement(
					"INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL,"
					+ " ?, "
					+ "TO_DATE(?,'DD/MM/YYYY'), "
					+ "tp_endereco(?, ?, ?, ?, ?, ?), "
					+ preencherTelefones(tamanho) // telefone
					+ ",?, "
					+ "?, "
					+ "?, "
					+ "?))");

			comando.setString(1, pessoaFisica.getNome());
			comando.setString(2, pessoaFisica.getDataNascimento());
			comando.setString(3, pessoaFisica.getEndereco().getLogradouro());
			comando.setString(4, pessoaFisica.getEndereco().getNumero());
			comando.setString(5, pessoaFisica.getEndereco().getBairro());
			comando.setString(6, pessoaFisica.getEndereco().getCidade());
			comando.setString(7, pessoaFisica.getEndereco().getUf());
			comando.setString(8, pessoaFisica.getEndereco().getCep());

			indice = 9;
			for(int i = 0; i < pessoaFisica.getTelefones().length; i++) {
				comando.setString(indice, pessoaFisica.getTelefones()[i].getDdd());
				comando.setString(indice+1, pessoaFisica.getTelefones()[i].getNumero());
				indice = indice + 2;
			}
			comando.setString(indice, pessoaFisica.getCpf());
			comando.setString(indice+1, String.valueOf(pessoaFisica.getSexo()));
			comando.setDouble(indice+2, pessoaFisica.getRenda());
			comando.setString(indice+3, pessoaFisica.getProfissao());
			
			comando.executeQuery();
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO AO INSERIR pessoa fisica!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();  
        }
	}
	
	@Override
	public void atualizar(Object pessoaFisicaObject) throws SQLException {

		PessoaFisica pessoaFisica = (PessoaFisica)pessoaFisicaObject;
		
		oracle.conectar();
				
		try {
			
			int indice;
			int tamanho = pessoaFisica.getTelefones().length;
			this.comando = oracle.getConexao().prepareStatement(
						"UPDATE tb_pessoafisica SET NOME = ?,"
						+ "DATANASCIMENTO = TO_DATE(?,'DD/MM/YYYY'),"
						+ "Endereco = tp_endereco(?, ?, ?, ?, ?, ?),"		
						+ "Telefones = "+ preencherTelefones(tamanho) // telefone
						+ ",SEXO = ?, "
						+ "RENDA = ?, "
						+ "PROFISSAO = ? WHERE CPF = ?");

			comando.setString(1, pessoaFisica.getNome());
			comando.setString(2, pessoaFisica.getDataNascimento());
			comando.setString(3, pessoaFisica.getEndereco().getLogradouro());
			comando.setString(4, pessoaFisica.getEndereco().getNumero());
			comando.setString(5, pessoaFisica.getEndereco().getBairro());
			comando.setString(6, pessoaFisica.getEndereco().getCidade());
			comando.setString(7, pessoaFisica.getEndereco().getUf());
			comando.setString(8, pessoaFisica.getEndereco().getCep());
			
			indice = 9;
			for(int i = 0; i < pessoaFisica.getTelefones().length; i++) {
				comando.setString(indice, pessoaFisica.getTelefones()[i].getDdd());
				comando.setString(indice+1, pessoaFisica.getTelefones()[i].getNumero());
				indice = indice + 2;
			}
			comando.setString(indice, String.valueOf(pessoaFisica.getSexo()));
			comando.setDouble(indice+1, pessoaFisica.getRenda());
			comando.setString(indice+2, pessoaFisica.getProfissao());
			comando.setString(indice+3, pessoaFisica.getCpf());
			
			
			comando.executeQuery();
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO AO ATUALIZAR pessoa fisica!");
		}
		finally{ 
			this.comando.close();
			oracle.desconectar();  
        }
	}
	
public PessoaFisica buscar(Object cpf) throws SQLException{
		
		oracle.conectar();
		ResultSet resultado;
		PessoaFisica pessoaFisica = null;
		
		try {
			
			this.comando = oracle.getConexao().prepareStatement(
						"SELECT * FROM TB_PESSOAFISICA PF WHERE PF.CPF = ?");
			
			this.comando.setString(1, (String)cpf);
			
			resultado = this.comando.executeQuery();
			
			String nome, dataNascimento, logradouro, numero, bairro, cidade, uf, cep, cpfPF, profissao;
			int id;
			Endereco endereco;
			Array array;
			Object[] objeto;
			Telefone [] telefones;
			Telefone tel;
			Object objetoEndereco;
			
			if(resultado.next()) {
				System.out.println("pessoa fisica ENCONTRADA!");
				
				do {
					id = ((BigDecimal)resultado.getBigDecimal("ID")).intValue();
					nome = (String) resultado.getObject("NOME");
					dataNascimento = new SimpleDateFormat("MM/dd/yyyy").format(resultado.getObject("DATANASCIMENTO"));

					objetoEndereco = (Object) resultado.getObject("ENDERECO");
					
					logradouro = (String)((STRUCT)objetoEndereco).getAttributes()[0];
					numero = (String)((STRUCT)objetoEndereco).getAttributes()[1];
					bairro = (String)((STRUCT)objetoEndereco).getAttributes()[2];
					cidade = (String)((STRUCT)objetoEndereco).getAttributes()[3];
					uf = (String)((STRUCT)objetoEndereco).getAttributes()[4];
					cep = (String)((STRUCT)objetoEndereco).getAttributes()[5];
					
					endereco = new Endereco(logradouro, numero, bairro, cidade, uf, cep);

					array = resultado.getArray("TELEFONES");
					objeto = (Object[])array.getArray();
					
					telefones = new Telefone[3];
					
					for (int i=0; i<objeto.length; i++) 
					{
						Object telefone_Saida = (Object) objeto[i];
						String ddd = (String) ((STRUCT) telefone_Saida).getAttributes()[0];
						String numeroTel = (String) ((STRUCT) telefone_Saida).getAttributes()[1];
					    tel = new Telefone(ddd, numeroTel);
					    telefones[i] = tel;
					}
					
					cpfPF = (String) resultado.getObject("CPF");
					char sexo = ((String) resultado.getObject("SEXO")).charAt(0);
					double renda = resultado.getDouble("RENDA");
					profissao = (String) resultado.getObject("PROFISSAO");
					
					pessoaFisica = new PessoaFisica(id, nome, dataNascimento, endereco, telefones, cpfPF, sexo, renda, profissao);
					
				} while(resultado.next());
			} else {
				System.out.println("pessoa fisica NAO ENCONTRADA!");
			}
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao buscar pessoa fisica!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();
        }
		return pessoaFisica;
	}
	
	public List<Object> buscarPorNome(Object nomePesquisado) throws SQLException{
		
		oracle.conectar();
		ResultSet resultado;
		PessoaFisica pessoaFisica = null;
		
		List<Object> pessoasFisicas = new ArrayList<Object>();
		
		try {
			
			this.comando = oracle.getConexao().prepareStatement(
						"SELECT * FROM TB_PESSOAFISICA PF WHERE LOWER(PF.NOME) LIKE ?");
			
			this.comando.setString(1, '%' + (String)nomePesquisado + '%');
			
			resultado = this.comando.executeQuery();
			
			String nome, dataNascimento, logradouro, numero, bairro, cidade, uf, cep, cpfPF, profissao;
			int id;
			Endereco endereco;
			Array array;
			Object[] objeto;
			Telefone [] telefones;
			Telefone tel;
			Object objetoEndereco;

			if(resultado.next()) {
				System.out.println("pessoa fisica ENCONTRADA!");
				
				do {
					id = ((BigDecimal)resultado.getBigDecimal("ID")).intValue();
					nome = (String) resultado.getObject("NOME");
					dataNascimento = new SimpleDateFormat("MM/dd/yyyy").format(resultado.getObject("DATANASCIMENTO"));

					objetoEndereco = (Object) resultado.getObject("ENDERECO");
					
					logradouro = (String)((STRUCT)objetoEndereco).getAttributes()[0];
					numero = (String)((STRUCT)objetoEndereco).getAttributes()[1];
					bairro = (String)((STRUCT)objetoEndereco).getAttributes()[2];
					cidade = (String)((STRUCT)objetoEndereco).getAttributes()[3];
					uf = (String)((STRUCT)objetoEndereco).getAttributes()[4];
					cep = (String)((STRUCT)objetoEndereco).getAttributes()[5];
					
					endereco = new Endereco(logradouro, numero, bairro, cidade, uf, cep);

					array = resultado.getArray("TELEFONES");
					objeto = (Object[])array.getArray();
					
					telefones = new Telefone[3];
					
					for (int i=0; i<objeto.length; i++) 
					{
						Object telefone_Saida = (Object) objeto[i];
						String ddd = (String) ((STRUCT) telefone_Saida).getAttributes()[0];
						String numeroTel = (String) ((STRUCT) telefone_Saida).getAttributes()[1];
					    tel = new Telefone(ddd, numeroTel);
					    telefones[i] = tel;
					}
					
					cpfPF = (String) resultado.getObject("CPF");
					char sexo = ((String) resultado.getObject("SEXO")).charAt(0);
					double renda = resultado.getDouble("RENDA");
					profissao = (String) resultado.getObject("PROFISSAO");
					
					pessoaFisica = new PessoaFisica(id, nome, dataNascimento, endereco, telefones, cpfPF, sexo, renda, profissao);
					
					pessoasFisicas.add(pessoaFisica);
				} while(resultado.next());
			} else {
				System.out.println("pessoa fisica NAO ENCONTRADA!");
			}
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao buscar pessoa fisica!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();
        }
		return pessoasFisicas;
	}
	
	public void remover(Object cpfObject) throws SQLException{
		
		String cpf = (String) cpfObject;
		
		oracle.conectar();
		
		try {
			
			this.comando = oracle.getConexao().prepareStatement(
						"DELETE FROM TB_PESSOAFISICA PF WHERE PF.CPF = ?");
			
			comando.setString(1, cpf);

			comando.executeQuery();
			System.out.println("pessoa fisica DELETADA!");
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao deletar pessoa fisica!");
		}
		finally{ 
			this.comando.close();
			oracle.desconectar();  
        }
	}
}
