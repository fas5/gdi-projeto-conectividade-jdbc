package negocio;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import oracle.sql.STRUCT;
import interfaces.Negocio;
import comunicacao.OracleConnection;
import entidades.Endereco;
import entidades.PessoaFisica;
import entidades.PessoaJuridica;
import entidades.Telefone;

public class PessoaJuridicaNegocio implements Negocio {

	private PessoaJuridica pj;
	private OracleConnection oracle;
	private PreparedStatement comando;

	public PessoaJuridicaNegocio(OracleConnection oracle) throws SQLException {
		this.oracle = oracle;
	}

	public String preencherTelefones(int qtdTelefones) {
		String resultado = "tp_telefones(null)";

		if (qtdTelefones != 0) {
			resultado = "tp_telefones(";
			String fone = "tp_telefone(?, ?)";

			for (int i = 0; i < qtdTelefones; i++) {
				resultado += fone;

				if (i < qtdTelefones - 1) {
					resultado += ", ";
				}
			}

			resultado += ")";
		}

		return resultado;
	}

	@Override
	public void inserir(Object pessoaJuridica) throws SQLException {
		this.oracle.conectar();

		this.pj = (PessoaJuridica) pessoaJuridica;

		try {
			int indice;
			int tamanho = this.pj.getTelefones().length;

			this.comando = this.oracle.getConexao().prepareStatement(
					"INSERT INTO tb_pessoajuridica VALUES (tp_pessoajuridica("
							+ "tb_pessoa_seq.NEXTVAL, " + "?, "
							+ "TO_DATE(?,'DD/MM/YYYY'), "
							+ "tp_endereco(?, ?, ?, ?, ?, ?), "
							+ preencherTelefones(tamanho) + " , ?, " + "?))");

			this.comando.setString(1, this.pj.getNome());
			this.comando.setString(2, this.pj.getDataNascimento());
			this.comando.setString(3, this.pj.getEndereco().getLogradouro());
			this.comando.setString(4, this.pj.getEndereco().getNumero());
			this.comando.setString(5, this.pj.getEndereco().getBairro());
			this.comando.setString(6, this.pj.getEndereco().getCidade());
			this.comando.setString(7, this.pj.getEndereco().getUf());
			this.comando.setString(8, this.pj.getEndereco().getCep());

			indice = 9;
			for (int i = 0; i < this.pj.getTelefones().length; i++) {
					this.comando.setString(indice,
							this.pj.getTelefones()[i].getDdd());
					this.comando.setString(indice + 1,
							this.pj.getTelefones()[i].getNumero());
					indice = indice + 2;
			}
			this.comando.setString(indice, this.pj.getCnpj());
			this.comando.setString(indice + 1, this.pj.getAreaAtuacao());

			this.comando.executeQuery();
		} catch (SQLException e) {
			System.out.println("ERRO ao inserir PESSOAJURIDICA!");
			e.printStackTrace();
		} finally {
			this.comando.close();
			this.oracle.desconectar();
		}
	}

	@Override
	public Object buscar(Object cnpj) throws SQLException {
		this.oracle.conectar();

		ResultSet resultado;

		PessoaJuridica pessoaJuridica = null;

		try {
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT * FROM tb_pessoajuridica PJ WHERE PJ.cnpj = ?");

			this.comando.setString(1, (String) cnpj);

			resultado = this.comando.executeQuery();

			String nome, dataNascimento, logradouro, numero, bairro, cidade, uf, cep;
			int id;
			Object objetoEndereco;
			Endereco endereco;
			Array array;
			Object[] objeto;
			Telefone[] telefones;
			Telefone tel;
			String cnpjPJ, areaAtuacao;

			while (resultado.next()) {
				id = ((BigDecimal)resultado.getBigDecimal("ID")).intValue();
				nome = (String) resultado.getObject("NOME");
				dataNascimento = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATANASCIMENTO"));

				objetoEndereco = (Object) resultado.getObject("ENDERECO");

				logradouro = (String) ((STRUCT) objetoEndereco).getAttributes()[0];
				numero = (String) ((STRUCT) objetoEndereco).getAttributes()[1];
				bairro = (String) ((STRUCT) objetoEndereco).getAttributes()[2];
				cidade = (String) ((STRUCT) objetoEndereco).getAttributes()[3];
				uf = (String) ((STRUCT) objetoEndereco).getAttributes()[4];
				cep = (String) ((STRUCT) objetoEndereco).getAttributes()[5];

				endereco = new Endereco(logradouro, numero, bairro, cidade, uf,
						cep);

				array = resultado.getArray("TELEFONES");
				objeto = (Object[]) array.getArray();

				telefones = new Telefone[3];
				for (int i = 0; i < objeto.length; i++) {
					Object telefone_Saida = (Object) objeto[i];
					String ddd = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[0];
					String numeroTel = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[1];

					tel = new Telefone(ddd, numeroTel);
					telefones[i] = tel;
				}

				cnpjPJ = (String) resultado.getObject("CNPJ");
				areaAtuacao = (String) resultado.getObject("AREAATUACAO");

				pessoaJuridica = new PessoaJuridica(id, nome, dataNascimento,
						endereco, telefones, cnpjPJ, areaAtuacao);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("ERRO ao buscar pessoa JURIDICA!");
		} finally {
			this.comando.close();
			this.oracle.desconectar();
		}

		return pessoaJuridica;
	}
	
	public List<Object> buscarPorNome(Object nomePesquisado) throws SQLException {
		this.oracle.conectar();

		ResultSet resultado;
		PessoaJuridica pessoaJuridica = null;
		
		List<Object> pessoasJuridicas = new ArrayList<Object>();

		try {
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT * FROM tb_pessoajuridica PJ WHERE LOWER(PJ.NOME) LIKE ?");

			this.comando.setString(1, '%' + (String) nomePesquisado + '%');

			resultado = this.comando.executeQuery();

			String nome, dataNascimento, logradouro, numero, bairro, cidade, uf, cep;
			int id;
			Object objetoEndereco;
			Endereco endereco;
			Array array;
			Object[] objeto;
			Telefone[] telefones;
			Telefone tel;
			String cnpjPJ, areaAtuacao;

			while (resultado.next()) {
				id = ((BigDecimal)resultado.getBigDecimal("ID")).intValue();
				nome = (String) resultado.getObject("NOME");
				dataNascimento = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATANASCIMENTO"));

				objetoEndereco = (Object) resultado.getObject("ENDERECO");

				logradouro = (String) ((STRUCT) objetoEndereco).getAttributes()[0];
				numero = (String) ((STRUCT) objetoEndereco).getAttributes()[1];
				bairro = (String) ((STRUCT) objetoEndereco).getAttributes()[2];
				cidade = (String) ((STRUCT) objetoEndereco).getAttributes()[3];
				uf = (String) ((STRUCT) objetoEndereco).getAttributes()[4];
				cep = (String) ((STRUCT) objetoEndereco).getAttributes()[5];

				endereco = new Endereco(logradouro, numero, bairro, cidade, uf,
						cep);

				array = resultado.getArray("TELEFONES");
				objeto = (Object[]) array.getArray();

				telefones = new Telefone[3];
				for (int i = 0; i < objeto.length; i++) {
					Object telefone_Saida = (Object) objeto[i];
					String ddd = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[0];
					String numeroTel = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[1];

					tel = new Telefone(ddd, numeroTel);
					telefones[i] = tel;
				}

				cnpjPJ = (String) resultado.getObject("CNPJ");
				areaAtuacao = (String) resultado.getObject("AREAATUACAO");

				pessoaJuridica = new PessoaJuridica(id, nome, dataNascimento,
						endereco, telefones, cnpjPJ, areaAtuacao);
				
				pessoasJuridicas.add(pessoaJuridica);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("ERRO ao buscar pessoa JURIDICA!");
		} finally {
			this.comando.close();
			this.oracle.desconectar();
		}

		return pessoasJuridicas;
	}

	@Override
	public void atualizar(Object pessoaJuridicaObject) throws SQLException {
		PessoaJuridica pessoaJuridica = (PessoaJuridica) pessoaJuridicaObject;

		this.oracle.conectar();

		try {
			int indice;
			int tamanho = pessoaJuridica.getTelefones().length;
			this.comando = oracle.getConexao().prepareStatement(
					"UPDATE tb_pessoajuridica SET NOME = ?, "
							+ "DATANASCIMENTO = TO_DATE(?,'DD/MM/YYYY'), "
							+ "Endereco = tp_endereco(?, ?, ?, ?, ?, ?), "
							+ "Telefones = " + preencherTelefones(tamanho)
							+ ", "// telefone
							+ "AREAATUACAO = ?" + "WHERE CNPJ = ?");

			this.comando.setString(1, pessoaJuridica.getNome());
			this.comando.setString(2, pessoaJuridica.getDataNascimento());
			this.comando.setString(3, pessoaJuridica.getEndereco()
					.getLogradouro());
			this.comando.setString(4, pessoaJuridica.getEndereco().getNumero());
			this.comando.setString(5, pessoaJuridica.getEndereco().getBairro());
			this.comando.setString(6, pessoaJuridica.getEndereco().getCidade());
			this.comando.setString(7, pessoaJuridica.getEndereco().getUf());
			this.comando.setString(8, pessoaJuridica.getEndereco().getCep());

			indice = 9;
			for (int i = 0; i < pessoaJuridica.getTelefones().length; i++) {
				this.comando.setString(indice,
						pessoaJuridica.getTelefones()[i].getDdd());
				this.comando.setString(indice + 1,
						pessoaJuridica.getTelefones()[i].getNumero());
				indice = indice + 2;
			}

			this.comando.setString(indice, pessoaJuridica.getAreaAtuacao());
			this.comando.setString(indice + 1, pessoaJuridica.getCnpj());

			comando.executeQuery();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("ERRO ao ATUALIZAR pessoa JURIDICA!");
		} finally {
			this.comando.close();
			this.oracle.desconectar();
		}
	}

	@Override
	public void remover(Object cnpj) throws SQLException {
		this.oracle.conectar();

		try {
			this.comando = this.oracle.getConexao().prepareStatement(
					"DELETE FROM tb_pessoajuridica PJ WHERE PJ.cnpj = ?");

			this.comando.setString(1, (String) cnpj);

			this.comando.executeQuery();
	
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("ERRO ao REMOVER pessoa JURIDICA!");
		} finally {
			this.comando.close();
			this.oracle.desconectar();
		}
	}
}