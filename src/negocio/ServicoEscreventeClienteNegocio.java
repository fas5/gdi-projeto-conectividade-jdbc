package negocio;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import comunicacao.OracleConnection;
import entidades.Atendimento;
import entidades.Cliente;
import entidades.Endereco;
import entidades.Escrevente;
import entidades.PessoaFisica;
import entidades.PessoaJuridica;
import entidades.Servico;
import entidades.ServicoEscreventeCliente;
import entidades.Telefone;
import interfaces.Negocio;

public class ServicoEscreventeClienteNegocio implements Negocio {
	
	private OracleConnection oracle;
	private PreparedStatement comando;
	
	public ServicoEscreventeClienteNegocio(OracleConnection oracle){
		this.oracle = oracle;
	}

	@Override
	public void inserir(Object secObj) throws SQLException {
		
		ServicoEscreventeCliente sec = (ServicoEscreventeCliente)secObj;
		
		oracle.conectar();
		
		try {
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String data = df.format(sec.getDataHora());

			this.comando = oracle.getConexao().prepareStatement(
					"INSERT INTO tb_servico_escrevente_cliente VALUES (tp_servico_escrevente_cliente("
					+ "(SELECT REF(G) FROM tb_escrevente G WHERE Id = ?),"
					+ "(SELECT REF(H) FROM tb_atendimento H WHERE Id= ?),"
					+ "((SELECT REF(X) FROM tb_pessoafisica X WHERE Id= ?)UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id= ?)),"
					+ "TO_DATE(?,'DD/MM/YYYY HH24:MI')))");

			comando.setInt(1, sec.getEscrevente().getId());
			comando.setInt(2, sec.getAtendimento().getId());
			comando.setInt(3, sec.getCliente().getId());
			comando.setInt(4, sec.getCliente().getId());
			comando.setString(5, data);

			comando.executeQuery();
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO AO INSERIR sec!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();  
        }
	}

	@Override
	public Object buscar(Object obj) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void atualizar(Object obj) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remover(Object obj) throws SQLException {
		// TODO Auto-generated method stub
		
	}
}
