package negocio;

import interfaces.Negocio;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import comunicacao.OracleConnection;
import entidades.Atendimento;
import entidades.Cliente;
import entidades.Endereco;
import entidades.Escrevente;
import entidades.PessoaFisica;
import entidades.PessoaJuridica;
import entidades.Presta;
import entidades.Servico;
import entidades.Telefone;

public class PrestaNegocio implements Negocio {
	
	private OracleConnection oracle;
	private PreparedStatement comando;
	
	public PrestaNegocio(OracleConnection oracle){
		this.oracle = oracle;
	}

	@Override
	public void inserir(Object prestaObj) throws SQLException {
		
		Presta presta = (Presta)prestaObj;
		
		oracle.conectar();
		
		try {

			this.comando = oracle.getConexao().prepareStatement(
					"INSERT INTO tb_presta VALUES (tp_presta("
					+ "(SELECT REF(G) FROM tb_escrevente G WHERE Id = ?),"
					+ "(SELECT REF(H) FROM tb_atendimento H WHERE Id= ?),"
					+ "((SELECT REF(X) FROM tb_pessoafisica X WHERE Id= ?)UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id= ?)),"
					+ "TO_DATE(?,'DD/MM/YYYY HH24:MI')))");

			comando.setInt(1, presta.getEscrevente().getId());
			comando.setInt(2, presta.getAtendimento().getId());
			comando.setInt(3, presta.getCliente().getId());
			comando.setInt(4, presta.getCliente().getId());
			comando.setString(5, presta.getDataHora());

			comando.executeQuery();
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO AO INSERIR presta!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();  
        }
		
	}

	@Override
	public Object buscar(Object obj) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void atualizar(Object obj) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remover(Object obj) throws SQLException {
		
	}
}
