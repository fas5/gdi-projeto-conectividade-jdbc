package negocio;

import interfaces.Negocio;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import oracle.sql.REF;
import oracle.sql.STRUCT;
import comunicacao.OracleConnection;
import entidades.Endereco;
import entidades.Escrevente;
import entidades.PessoaFisica;
import entidades.Telefone;

public class EscreventeNegocio implements Negocio {

	private OracleConnection oracle;
	private PreparedStatement comando;

	public EscreventeNegocio(OracleConnection oracle) {
		this.oracle = oracle;
	}

	public String preencherTelefones(int qtdTelefones) {
		String resultado = "tp_telefones(null)";

		if (qtdTelefones != 0) {
			resultado = "tp_telefones(";
			String fone = "tp_telefone(?, ?)";

			for (int i = 0; i < qtdTelefones; i++) {
				resultado += fone;

				if (i < qtdTelefones - 1) {
					resultado += ", ";
				}
			}

			resultado += ")";
		}

		return resultado;
	}

	public void remover(Object matriculaObject) throws SQLException {

		int matricula = (int) matriculaObject;

		oracle.conectar();

		try {

			this.comando = oracle.getConexao().prepareStatement(
					"DELETE FROM TB_ESCREVENTE E WHERE E.MATRICULA = ?");

			comando.setInt(1, matricula);

			comando.executeQuery();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
	}

	@Override
	public void inserir(Object escreventeObj) throws SQLException {

		Escrevente escrevente = (Escrevente) escreventeObj;

		oracle.conectar();

		try {

			int indice;
			int tamanho = 0;

			tamanho = escrevente.getTelefones().length;

			this.comando = oracle
					.getConexao()
					.prepareStatement(
							"INSERT INTO tb_escrevente VALUES (tp_escrevente(tb_pessoa_seq.NEXTVAL,"
									+ " ?, "
									+ "TO_DATE(?,'DD/MM/YYYY'), "
									+ "tp_endereco(?, ?, ?, ?, ?, ?), "
									+ preencherTelefones(tamanho) // telefone
									+ ",?, "
									+ "(select ref(e) from tb_escrevente e where (e.matricula = ?)), "
									+ "?, " 
									+ "?,"
									+ "?))");

			comando.setString(1, escrevente.getNome());
			comando.setString(2, escrevente.getDataNascimento());
			comando.setString(3, escrevente.getEndereco().getLogradouro());
			comando.setString(4, escrevente.getEndereco().getNumero());
			comando.setString(5, escrevente.getEndereco().getBairro());
			comando.setString(6, escrevente.getEndereco().getCidade());
			comando.setString(7, escrevente.getEndereco().getUf());
			comando.setString(8, escrevente.getEndereco().getCep());

			indice = 9;
			for (int i = 0; i < escrevente.getTelefones().length; i++) {
				comando.setString(indice, escrevente.getTelefones()[i].getDdd());
				comando.setString(indice + 1,
						escrevente.getTelefones()[i].getNumero());
				indice = indice + 2;
			}
			comando.setInt(indice, escrevente.getMatricula());
			if (escrevente.getSupervisor() != null) {
				comando.setInt(indice + 1, escrevente.getSupervisor()
						.getMatricula());
			} else {
				comando.setInt(indice + 1, 0);
			}
			comando.setDouble(indice + 2, escrevente.getSalario());
			comando.setString(indice + 3, escrevente.getDataAdmissao());
			comando.setBytes(indice + 4, escrevente.getFoto());

			comando.executeQuery();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}

	}

	@Override
	public Object buscar(Object matricula) throws SQLException {

		oracle.conectar();
		ResultSet resultado;
		Escrevente escrevente = null;

		try {

			this.comando = oracle.getConexao().prepareStatement(
					"SELECT * FROM TB_ESCREVENTE E WHERE E.MATRICULA = ?");

			this.comando.setInt(1, (int) matricula);

			resultado = this.comando.executeQuery();

			String nome, dataNascimento, dataAdmissaoEscrevente, logradouro, numero, bairro, cidade, uf, cep, nomeSupervisor, dataNascimentoSupervisor, dataAdmissaoSupervisor, logradouroSupervisor, numeroSupervisor, bairroSupervisor, cidadeSupervisor, ufSupervisor, cepSupervisor;
			int matriculaEscrevente, matriculaSupervisor, idEscrevente, idSupervisor;
			double salarioEscrevente, salarioSupervisor;
			Endereco enderecoEscrevente, enderecoSupervisor;
			Array array, arraySupervisor;
			Object[] objetoTelefone, objetoTelefoneSupervisor;
			Escrevente escreventeSupervisor = null;
			Telefone[] telefones, telefonesSupervisor;
			Object objetoEndereco, objetoEnderecoSupervisor;
			byte[] foto = null;

			while (resultado.next()) {
				idEscrevente = ((BigDecimal)resultado.getBigDecimal("ID")).intValue();
				nome = (String) resultado.getObject("NOME");
				dataNascimento = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATANASCIMENTO"));

				objetoEndereco = (Object) resultado.getObject("ENDERECO");
				logradouro = (String) ((STRUCT) objetoEndereco).getAttributes()[0];
				numero = (String) ((STRUCT) objetoEndereco).getAttributes()[1];
				bairro = (String) ((STRUCT) objetoEndereco).getAttributes()[2];
				cidade = (String) ((STRUCT) objetoEndereco).getAttributes()[3];
				uf = (String) ((STRUCT) objetoEndereco).getAttributes()[4];
				cep = (String) ((STRUCT) objetoEndereco).getAttributes()[5];

				enderecoEscrevente = new Endereco(logradouro, numero, bairro,
						cidade, uf, cep);

				array = resultado.getArray("TELEFONES");
				objetoTelefone = (Object[]) array.getArray();

				telefones = new Telefone[3];

				for (int i = 0; i < objetoTelefone.length; i++) {
					Object telefone_Saida = (Object) objetoTelefone[i];
					String ddd = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[0];
					String numeroTel = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[1];
					Telefone tel = new Telefone(ddd, numeroTel);
					telefones[i] = tel;
				}

				matriculaEscrevente = resultado.getBigDecimal("MATRICULA")
						.intValue();
				salarioEscrevente = resultado.getDouble("SALARIO");
				dataAdmissaoEscrevente = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATAADMISSAO"));
				foto = resultado.getBytes("FOTO");

				REF supervisor = (REF) resultado.getRef("SUPERVISOR");

				if (supervisor != null) {

					STRUCT supervisorStruct = (STRUCT) supervisor.getSTRUCT();

					idSupervisor = ((BigDecimal) supervisorStruct.getAttributes()[0]).intValue();
					nomeSupervisor = (String) supervisorStruct.getAttributes()[1];
					dataNascimentoSupervisor = new SimpleDateFormat(
							"MM/dd/yyyy").format(supervisorStruct
							.getAttributes()[2]);

					objetoEnderecoSupervisor = (Object) supervisorStruct
							.getAttributes()[3];

					logradouroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[0];
					numeroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[1];
					bairroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[2];
					cidadeSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[3];
					ufSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[4];
					cepSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[5];

					enderecoSupervisor = new Endereco(logradouroSupervisor,
							numeroSupervisor, bairroSupervisor,
							cidadeSupervisor, ufSupervisor, cepSupervisor);

					arraySupervisor = (Array) supervisorStruct.getAttributes()[4];
					objetoTelefoneSupervisor = (Object[]) arraySupervisor
							.getArray();

					telefonesSupervisor = new Telefone[3];

					for (int i = 0; i < objetoTelefoneSupervisor.length; i++) {
						Object telefone_Saida = (Object) objetoTelefoneSupervisor[i];
						String ddd = (String) ((STRUCT) telefone_Saida)
								.getAttributes()[0];
						String numeroTel = (String) ((STRUCT) telefone_Saida)
								.getAttributes()[1];
						Telefone tel = new Telefone(ddd, numeroTel);
						telefonesSupervisor[i] = tel;
					}

					matriculaSupervisor = ((BigDecimal) supervisorStruct
							.getAttributes()[5]).intValue();
					salarioSupervisor = ((BigDecimal) supervisorStruct
							.getAttributes()[7]).doubleValue();
					dataAdmissaoSupervisor = new SimpleDateFormat("MM/dd/yyyy")
							.format(supervisorStruct.getAttributes()[8]);

					escreventeSupervisor = new Escrevente(idSupervisor, nomeSupervisor,
							dataNascimentoSupervisor, enderecoSupervisor,
							telefonesSupervisor, matriculaSupervisor, null,
							salarioSupervisor, dataAdmissaoSupervisor, null);

				}

				escrevente = new Escrevente(idEscrevente, nome, dataNascimento,
						enderecoEscrevente, telefones, matriculaEscrevente,
						escreventeSupervisor, salarioEscrevente,
						dataAdmissaoEscrevente, foto);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		return escrevente;
	}
	
	public List<Escrevente> buscarTodasMatriculas() throws SQLException {

		oracle.conectar();
		ResultSet resultado;
		Escrevente escrevente = null;
		List<Escrevente> escreventes = new ArrayList<Escrevente>();
		
		try{
			
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT E.NOME, E.MATRICULA FROM TB_ESCREVENTE E");

			resultado = this.comando.executeQuery();
			
			int matricula;
			String nome;
			
			while(resultado.next()){
				matricula = resultado.getInt("MATRICULA");
				nome = resultado.getString("NOME");
				
				escrevente = new Escrevente(0, nome, null, null, null, matricula, null, 0, null, null);
				
				escreventes.add(escrevente);
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		
		return escreventes;
	}
	
	public List<Object> buscarPorNome(Object nomePesquisado) throws SQLException {

		oracle.conectar();
		ResultSet resultado;
		Escrevente escrevente = null;
		List<Object> escreventes = new ArrayList<Object>();

		try {

			this.comando = oracle.getConexao().prepareStatement(
					"SELECT * FROM TB_ESCREVENTE E WHERE LOWER(E.NOME) LIKE ?");

			this.comando.setString(1, '%' + (String) nomePesquisado+ '%');

			resultado = this.comando.executeQuery();

			String nome, dataNascimento, dataAdmissaoEscrevente, logradouro, numero, bairro, cidade, uf, cep, nomeSupervisor, dataNascimentoSupervisor, dataAdmissaoSupervisor, logradouroSupervisor, numeroSupervisor, bairroSupervisor, cidadeSupervisor, ufSupervisor, cepSupervisor;
			int matriculaEscrevente, matriculaSupervisor, idEscrevente, idSupervisor;
			double salarioEscrevente, salarioSupervisor;
			Endereco enderecoEscrevente, enderecoSupervisor;
			Array array, arraySupervisor;
			Object[] objetoTelefone, objetoTelefoneSupervisor;
			Escrevente escreventeSupervisor = null;
			Telefone[] telefones, telefonesSupervisor;
			Object objetoEndereco, objetoEnderecoSupervisor;
			byte[] foto = null;
			
			while (resultado.next()) {
				idEscrevente = ((BigDecimal)resultado.getBigDecimal("ID")).intValue();
				nome = (String) resultado.getObject("NOME");
				dataNascimento = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATANASCIMENTO"));

				objetoEndereco = (Object) resultado.getObject("ENDERECO");
				logradouro = (String) ((STRUCT) objetoEndereco).getAttributes()[0];
				numero = (String) ((STRUCT) objetoEndereco).getAttributes()[1];
				bairro = (String) ((STRUCT) objetoEndereco).getAttributes()[2];
				cidade = (String) ((STRUCT) objetoEndereco).getAttributes()[3];
				uf = (String) ((STRUCT) objetoEndereco).getAttributes()[4];
				cep = (String) ((STRUCT) objetoEndereco).getAttributes()[5];

				enderecoEscrevente = new Endereco(logradouro, numero, bairro,
						cidade, uf, cep);

				array = resultado.getArray("TELEFONES");
				objetoTelefone = (Object[]) array.getArray();

				telefones = new Telefone[3];

				for (int i = 0; i < objetoTelefone.length; i++) {
					Object telefone_Saida = (Object) objetoTelefone[i];
					String ddd = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[0];
					String numeroTel = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[1];
					Telefone tel = new Telefone(ddd, numeroTel);
					telefones[i] = tel;
				}

				matriculaEscrevente = resultado.getBigDecimal("MATRICULA")
						.intValue();
				salarioEscrevente = resultado.getDouble("SALARIO");
				dataAdmissaoEscrevente = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATAADMISSAO"));
				foto = resultado.getBytes("FOTO");

				REF supervisor = (REF) resultado.getRef("SUPERVISOR");

				if (supervisor != null) {

					STRUCT supervisorStruct = (STRUCT) supervisor.getSTRUCT();

					idSupervisor = ((BigDecimal) supervisorStruct.getAttributes()[0]).intValue();
					nomeSupervisor = (String) supervisorStruct.getAttributes()[1];
					dataNascimentoSupervisor = new SimpleDateFormat(
							"MM/dd/yyyy").format(supervisorStruct
							.getAttributes()[2]);

					objetoEnderecoSupervisor = (Object) supervisorStruct
							.getAttributes()[3];

					logradouroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[0];
					numeroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[1];
					bairroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[2];
					cidadeSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[3];
					ufSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[4];
					cepSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[5];

					enderecoSupervisor = new Endereco(logradouroSupervisor,
							numeroSupervisor, bairroSupervisor,
							cidadeSupervisor, ufSupervisor, cepSupervisor);

					arraySupervisor = (Array) supervisorStruct.getAttributes()[4];
					objetoTelefoneSupervisor = (Object[]) arraySupervisor
							.getArray();

					telefonesSupervisor = new Telefone[3];

					for (int i = 0; i < objetoTelefoneSupervisor.length; i++) {
						Object telefone_Saida = (Object) objetoTelefoneSupervisor[i];
						String ddd = (String) ((STRUCT) telefone_Saida)
								.getAttributes()[0];
						String numeroTel = (String) ((STRUCT) telefone_Saida)
								.getAttributes()[1];
						Telefone tel = new Telefone(ddd, numeroTel);
						telefonesSupervisor[i] = tel;
					}

					matriculaSupervisor = ((BigDecimal) supervisorStruct
							.getAttributes()[5]).intValue();
					salarioSupervisor = ((BigDecimal) supervisorStruct
							.getAttributes()[7]).doubleValue();
					dataAdmissaoSupervisor = new SimpleDateFormat("MM/dd/yyyy")
							.format(supervisorStruct.getAttributes()[8]);

					escreventeSupervisor = new Escrevente(idSupervisor, nomeSupervisor,
							dataNascimentoSupervisor, enderecoSupervisor,
							telefonesSupervisor, matriculaSupervisor, null,
							salarioSupervisor, dataAdmissaoSupervisor, null);

				}

				escrevente = new Escrevente(idEscrevente, nome, dataNascimento,
						enderecoEscrevente, telefones, matriculaEscrevente,
						escreventeSupervisor, salarioEscrevente,
						dataAdmissaoEscrevente, foto);
				
				escreventes.add(escrevente);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		return escreventes;
	}

	@Override
	public void atualizar(Object escreventeObj) throws SQLException {

		Escrevente escrevente = (Escrevente) escreventeObj;

		oracle.conectar();

		try {

			int indice;
			int tamanho = escrevente.getTelefones().length;
			this.comando = oracle.getConexao().prepareStatement(
					"UPDATE tb_escrevente SET NOME = ?,"
							+ "DATANASCIMENTO = TO_DATE(?,'DD/MM/YYYY'),"
							+ "Endereco = tp_endereco(?, ?, ?, ?, ?, ?),"
							+ "Telefones = "+ preencherTelefones(tamanho) // telefone
							+ ",Supervisor = (select ref(e) from tb_escrevente e where (e.matricula = ?)),"
							+ "SALARIO = ?, "
							+ "DATAADMISSAO = TO_DATE(?,'DD/MM/YYYY'),"
							+ "FOTO = ? WHERE MATRICULA = ?");

			comando.setString(1, escrevente.getNome());
			comando.setString(2, escrevente.getDataNascimento());
			comando.setString(3, escrevente.getEndereco().getLogradouro());
			comando.setString(4, escrevente.getEndereco().getNumero());
			comando.setString(5, escrevente.getEndereco().getBairro());
			comando.setString(6, escrevente.getEndereco().getCidade());
			comando.setString(7, escrevente.getEndereco().getUf());
			comando.setString(8, escrevente.getEndereco().getCep());

			indice = 9;
			for (int i = 0; i < escrevente.getTelefones().length; i++) {
				comando.setString(indice,
						escrevente.getTelefones()[i].getDdd());
				comando.setString(indice + 1,
						escrevente.getTelefones()[i].getNumero());
				indice = indice + 2;
			}
			
			if (escrevente.getSupervisor() != null) {
				comando.setInt(indice, escrevente.getSupervisor()
						.getMatricula());
			} else {
				comando.setInt(indice, 0);
			}
			comando.setDouble(indice + 1, escrevente.getSalario());
			comando.setString(indice + 2, escrevente.getDataAdmissao());
			comando.setBytes(indice + 3, escrevente.getFoto());
			comando.setInt(indice + 4, escrevente.getMatricula());

			comando.executeQuery();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
	}
}
