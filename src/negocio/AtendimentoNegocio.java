package negocio;

import interfaces.Negocio;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracle.sql.REF;
import oracle.sql.STRUCT;
import comunicacao.OracleConnection;
import entidades.Atendimento;
import entidades.Cliente;
import entidades.Escrevente;
import entidades.PessoaFisica;
import entidades.PessoaJuridica;
import entidades.RelatorioAtendimento;
import entidades.Servico;
import entidades.ServicoEscreventeCliente;

public class AtendimentoNegocio implements Negocio {
	
	private OracleConnection oracle;
	private PreparedStatement comando;
	
	public AtendimentoNegocio(OracleConnection oracle){
		this.oracle = oracle;
	}
	
	public int maximoId() throws SQLException{
		oracle.conectar();
		int retorno = 0;
		
		try{
			
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT MAX(ID) FROM TB_ATENDIMENTO");
			
			ResultSet resultado = comando.executeQuery();
			
			while(resultado.next()){
				retorno = resultado.getInt(1);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			this.comando.close();
			oracle.desconectar();  
		}
		return retorno;
		
	}

	@Override
	public void inserir(Object atendimentoObj) throws SQLException {
		
		Atendimento atendimento = (Atendimento)atendimentoObj;
		
		oracle.conectar();
		
		try {

			this.comando = oracle.getConexao().prepareStatement(
					"INSERT INTO tb_atendimento VALUES ( "
					+ "tp_atendimento(tb_atendimento_seq.NEXTVAL, "
					+ "?, ?, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = ?))))");

			comando.setInt(1, atendimento.getQuantidade());
			comando.setInt(2, atendimento.getDesconto());
			comando.setInt(3, atendimento.getServico().getCodigo());

			comando.executeQuery();
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO AO INSERIR atendimento!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();  
        }
	}

	@Override
	public Object buscar(Object idObj) throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		Atendimento atendimento = null;
		
		try {
			
			this.comando = oracle.getConexao().prepareStatement(
						"SELECT * FROM TB_ATENDIMENTO A WHERE A.ID = ?");
			
			this.comando.setInt(1, (int)idObj);
			
			resultado = this.comando.executeQuery();
			
			int id, desconto, quantidade, codigo, autorizado;
			Servico servico;
			double preco;
			String descricao;

			while(resultado.next()) {
				id = resultado.getInt("ID");
				desconto = resultado.getInt("DESCONTO");
				quantidade = resultado.getInt("QUANTIDADE");
				REF refServico = (REF) resultado.getRef("SERVICO");
				
				STRUCT servicoStruct = (STRUCT) refServico.getSTRUCT();
				
				codigo = ((BigDecimal) servicoStruct.getAttributes()[0]).intValue();
				descricao = (String) servicoStruct.getAttributes()[1];
				preco = ((BigDecimal) servicoStruct.getAttributes()[2]).intValue();
				autorizado = ((BigDecimal) servicoStruct.getAttributes()[3]).intValue();
				
				servico = new Servico(codigo, descricao, preco, autorizado);
				
				atendimento = new Atendimento(id, desconto, quantidade, servico);
			}
	
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao buscar atendimento!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();
        }
		return atendimento;
	}
	
	public List<RelatorioAtendimento> buscarPorCodigo(int codigo) throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		List<RelatorioAtendimento> relatoriosAtendimento = new ArrayList<RelatorioAtendimento>();
		
		try {
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT PF.NOME, E.NOME, E.MATRICULA, S.DESCRICAO, S.PRECO, S.CODIGO, A.QUANTIDADE, A.DESCONTO, PR.DATAHORA "
					+ "FROM TB_PESSOAFISICA PF "
					+ "INNER JOIN TB_PRESTA PR ON (PR.CLIENTE.ID = PF.ID) "
					+ "INNER JOIN TB_ESCREVENTE E ON (E.MATRICULA = PR.ESCREVENTE.MATRICULA) "
					+ "INNER JOIN TB_ATENDIMENTO A ON (A.ID = PR.ATENDIMENTO.ID) "
					+ "INNER JOIN TB_SERVICO S ON (S.CODIGO = A.SERVICO.CODIGO) "
					+ "WHERE S.CODIGO = ? "
					+ "UNION ALL "
					+ "SELECT PJ.NOME, E.NOME, E.MATRICULA, S.DESCRICAO, S.PRECO, S.CODIGO, A.QUANTIDADE, A.DESCONTO, PR.DATAHORA "
					+ "FROM TB_PESSOAJURIDICA PJ "
					+ "INNER JOIN TB_PRESTA PR ON (PR.CLIENTE.ID = PJ.ID) "
					+ "INNER JOIN TB_ESCREVENTE E ON (E.MATRICULA = PR.ESCREVENTE.MATRICULA) "
					+ "INNER JOIN TB_ATENDIMENTO A ON (A.ID = PR.ATENDIMENTO.ID) "
					+ "INNER JOIN TB_SERVICO S ON (S.CODIGO = A.SERVICO.CODIGO) "
					+ "WHERE S.CODIGO = ?");

			this.comando.setInt(1, codigo);
			this.comando.setInt(2, codigo);
			
			resultado = this.comando.executeQuery();
			
			while(resultado.next()){
				RelatorioAtendimento ra = new RelatorioAtendimento(resultado.getString(1), resultado.getString(2),
						resultado.getString(3), resultado.getString(4), resultado.getString(5), resultado.getString(6),
						resultado.getString(7), resultado.getString(8), resultado.getString(9));
				relatoriosAtendimento.add(ra);
			}
	
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao buscar por escrevente!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();
        }
		return relatoriosAtendimento;

	}
	
public List<RelatorioAtendimento> buscarPorData(String dataInicial, String dataFinal) throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		List<RelatorioAtendimento> relatoriosAtendimento = new ArrayList<RelatorioAtendimento>();
		
		try {
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT PF.NOME, E.NOME, E.MATRICULA, S.DESCRICAO, S.PRECO, S.CODIGO, A.QUANTIDADE, A.DESCONTO, PR.DATAHORA "
					+ "FROM TB_PESSOAFISICA PF "
					+ "INNER JOIN TB_PRESTA PR ON (PR.CLIENTE.ID = PF.ID) "
					+ "INNER JOIN TB_ESCREVENTE E ON (E.MATRICULA = PR.ESCREVENTE.MATRICULA) "
					+ "INNER JOIN TB_ATENDIMENTO A ON (A.ID = PR.ATENDIMENTO.ID) "
					+ "INNER JOIN TB_SERVICO S ON (S.CODIGO = A.SERVICO.CODIGO) "
					+ "WHERE PR.DATAHORA BETWEEN TO_DATE(?,'DD/MM/YYYY HH24:MI') AND TO_DATE(?,'DD/MM/YYYY HH24:MI') "
					+ "UNION ALL "
					+ "SELECT PJ.NOME, E.NOME, E.MATRICULA, S.DESCRICAO, S.PRECO, S.CODIGO, A.QUANTIDADE, A.DESCONTO, PR.DATAHORA "
					+ "FROM TB_PESSOAJURIDICA PJ "
					+ "INNER JOIN TB_PRESTA PR ON (PR.CLIENTE.ID = PJ.ID) "
					+ "INNER JOIN TB_ESCREVENTE E ON (E.MATRICULA = PR.ESCREVENTE.MATRICULA) "
					+ "INNER JOIN TB_ATENDIMENTO A ON (A.ID = PR.ATENDIMENTO.ID) "
					+ "INNER JOIN TB_SERVICO S ON (S.CODIGO = A.SERVICO.CODIGO) "
					+ "WHERE PR.DATAHORA BETWEEN TO_DATE(?,'DD/MM/YYYY HH24:MI') AND TO_DATE(?,'DD/MM/YYYY HH24:MI') ");

			this.comando.setString(1, dataInicial);
			this.comando.setString(2, dataFinal);
			this.comando.setString(3, dataInicial);
			this.comando.setString(4, dataFinal);
			
			resultado = this.comando.executeQuery();
			while(resultado.next()){
				RelatorioAtendimento ra = new RelatorioAtendimento(resultado.getString(1), resultado.getString(2),
						resultado.getString(3), resultado.getString(4), resultado.getString(5), resultado.getString(6),
						resultado.getString(7), resultado.getString(8), resultado.getString(9));
				relatoriosAtendimento.add(ra);
			}
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao buscar por atendimento!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();
        }
		return relatoriosAtendimento;
	}
	
	public List<RelatorioAtendimento> buscarPorPessoaFisica(String cpf) throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		List<RelatorioAtendimento> relatoriosAtendimento = new ArrayList<RelatorioAtendimento>();
		
		try {
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT PF.NOME, E.NOME, E.MATRICULA, S.DESCRICAO, S.PRECO, S.CODIGO, A.QUANTIDADE, A.DESCONTO, PR.DATAHORA "
					+ "FROM TB_PESSOAFISICA PF "
					+ "INNER JOIN TB_PRESTA PR ON (PR.CLIENTE.ID = PF.ID) "
					+ "INNER JOIN TB_ESCREVENTE E ON (E.MATRICULA = PR.ESCREVENTE.MATRICULA) "
					+ "INNER JOIN TB_ATENDIMENTO A ON (A.ID = PR.ATENDIMENTO.ID) "
					+ "INNER JOIN TB_SERVICO S ON (S.CODIGO = A.SERVICO.CODIGO) "
					+ "WHERE PF.CPF = ? ");

			this.comando.setString(1, cpf);;
			
			resultado = this.comando.executeQuery();
			
			while(resultado.next()){
				RelatorioAtendimento ra = new RelatorioAtendimento(resultado.getString(1), resultado.getString(2),
						resultado.getString(3), resultado.getString(4), resultado.getString(5), resultado.getString(6),
						resultado.getString(7), resultado.getString(8), resultado.getString(9));
				relatoriosAtendimento.add(ra);
			}
	
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao buscar por pessoa fisica!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();
        }
		return relatoriosAtendimento;

	}
	
	public List<RelatorioAtendimento> buscarPorPessoaJuridica(String cnpj) throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		List<RelatorioAtendimento> relatoriosAtendimento = new ArrayList<RelatorioAtendimento>();
		
		try {
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT PJ.NOME, E.NOME, E.MATRICULA, S.DESCRICAO, S.PRECO, S.CODIGO, A.QUANTIDADE, A.DESCONTO, PR.DATAHORA "
					+ "FROM TB_PESSOAJURIDICA PJ "
					+ "INNER JOIN TB_PRESTA PR ON (PR.CLIENTE.ID = PJ.ID) "
					+ "INNER JOIN TB_ESCREVENTE E ON (E.MATRICULA = PR.ESCREVENTE.MATRICULA) "
					+ "INNER JOIN TB_ATENDIMENTO A ON (A.ID = PR.ATENDIMENTO.ID) "
					+ "INNER JOIN TB_SERVICO S ON (S.CODIGO = A.SERVICO.CODIGO) "
					+ "WHERE PJ.CNPJ = ? ");

			this.comando.setString(1, cnpj);
			
			resultado = this.comando.executeQuery();
			
			while(resultado.next()){
				RelatorioAtendimento ra = new RelatorioAtendimento(resultado.getString(1), resultado.getString(2),
						resultado.getString(3), resultado.getString(4), resultado.getString(5), resultado.getString(6),
						resultado.getString(7), resultado.getString(8), resultado.getString(9));
				relatoriosAtendimento.add(ra);
			}
	
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao buscar por pessoa juridica!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();
        }
		return relatoriosAtendimento;
		
	}
	
	public List<RelatorioAtendimento> buscarPorEscrevente(int matricula) throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		List<RelatorioAtendimento> relatoriosAtendimento = new ArrayList<RelatorioAtendimento>();
		
		try {
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT PF.NOME, E.NOME, E.MATRICULA, S.DESCRICAO, S.PRECO, S.CODIGO, A.QUANTIDADE, A.DESCONTO, PR.DATAHORA "
					+ "FROM TB_PESSOAFISICA PF "
					+ "INNER JOIN TB_PRESTA PR ON (PR.CLIENTE.ID = PF.ID) "
					+ "INNER JOIN TB_ESCREVENTE E ON (E.MATRICULA = PR.ESCREVENTE.MATRICULA) "
					+ "INNER JOIN TB_ATENDIMENTO A ON (A.ID = PR.ATENDIMENTO.ID) "
					+ "INNER JOIN TB_SERVICO S ON (S.CODIGO = A.SERVICO.CODIGO) "
					+ "WHERE E.MATRICULA = ? "
					+ "UNION ALL "
					+ "SELECT PJ.NOME, E.NOME, E.MATRICULA, S.DESCRICAO, S.PRECO, S.CODIGO, A.QUANTIDADE, A.DESCONTO, PR.DATAHORA "
					+ "FROM TB_PESSOAJURIDICA PJ "
					+ "INNER JOIN TB_PRESTA PR ON (PR.CLIENTE.ID = PJ.ID) "
					+ "INNER JOIN TB_ESCREVENTE E ON (E.MATRICULA = PR.ESCREVENTE.MATRICULA) "
					+ "INNER JOIN TB_ATENDIMENTO A ON (A.ID = PR.ATENDIMENTO.ID) "
					+ "INNER JOIN TB_SERVICO S ON (S.CODIGO = A.SERVICO.CODIGO) "
					+ "WHERE E.MATRICULA = ?");

			this.comando.setInt(1, matricula);
			this.comando.setInt(2, matricula);
			
			resultado = this.comando.executeQuery();
			
			while(resultado.next()){
				RelatorioAtendimento ra = new RelatorioAtendimento(resultado.getString(1), resultado.getString(2),
						resultado.getString(3), resultado.getString(4), resultado.getString(5), resultado.getString(6),
						resultado.getString(7), resultado.getString(8), resultado.getString(9));
				relatoriosAtendimento.add(ra);
			}
	
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao buscar por escrevente!");
		}
		finally{  
			this.comando.close();
			oracle.desconectar();
        }
		return relatoriosAtendimento;
	}
	
	public List<RelatorioAtendimento> buscarPorTabeliao(int matricula) throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		List<RelatorioAtendimento> relatoriosAtendimento = new ArrayList<RelatorioAtendimento>();
		
		try {
			Object[]  objetoSecLista;
			java.sql.Array arraySec;
			List<ServicoEscreventeCliente> listaSec = new ArrayList<ServicoEscreventeCliente>();
			
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT T.SERVICOESCREVENTECLIENTE,T.NOME FROM TB_TABELIAO T WHERE T.MATRICULA = ?");
			
			this.comando.setInt(1, (int) matricula);
			resultado = this.comando.executeQuery();
			String nome = null;
		
			while(resultado.next()){
				
				arraySec = resultado.getArray(1);
				nome = resultado.getString(2);
			
				objetoSecLista = (Object[]) arraySec.getArray();
				for (int i = 0; i < objetoSecLista.length; i++) {
                   
					Object structSEC = ((Ref) objetoSecLista[i]).getObject();
					
					Servico servicoSec = null;
					Atendimento atendimentoSec = null;
					Cliente clienteSec = null;
					Escrevente escreventeSec = null;
					
					
					REF escreventeRef = (REF) ((STRUCT) structSEC).getAttributes()[0];
					REF atendimentoRef = (REF) ((STRUCT) structSEC).getAttributes()[1];
					REF clienteRef =  (REF) ((STRUCT) structSEC).getAttributes()[2];
					Date dataRef = (Date) ((STRUCT) structSEC).getAttributes()[3];
					
					if (escreventeRef != null) {
						STRUCT escreventeStruct = (STRUCT) escreventeRef.getObject();
						String nomeEscreventeSec = (String) escreventeStruct.getAttributes()[1];
						int matriculaEscreventeSec = ((BigDecimal) escreventeStruct.getAttributes()[5]).intValue();
						
						escreventeSec = new Escrevente(0, nomeEscreventeSec, null, null, null, matriculaEscreventeSec, null, 0,  null, null);
					}
					
					if (atendimentoRef != null) {
						STRUCT atendimentoStruct = (STRUCT) atendimentoRef.getSTRUCT();
						int idAtendimentoSec = ((BigDecimal) atendimentoStruct.getAttributes()[0]).intValue();
						int descontoSec = ((BigDecimal) atendimentoStruct.getAttributes()[1]).intValue();
						int quantidadeSec = ((BigDecimal) atendimentoStruct.getAttributes()[2]).intValue();
						
						REF servicoRef = (REF) atendimentoStruct.getAttributes()[3];
						
						if (servicoRef != null) {
							STRUCT servicoStruct = (STRUCT) servicoRef.getSTRUCT();
							int codigoServico = ((BigDecimal) servicoStruct.getAttributes()[0]).intValue();
							String descricaoServico = ((String) servicoStruct.getAttributes()[1]);
							double precoServico = ((BigDecimal) servicoStruct.getAttributes()[0]).doubleValue();
							
							servicoSec = new Servico(codigoServico, descricaoServico, precoServico, 0);
						}
						
						atendimentoSec = new Atendimento(idAtendimentoSec, quantidadeSec, descontoSec, servicoSec);
					}
					
					if (clienteRef != null) {
						STRUCT clienteStruct = (STRUCT) clienteRef.getSTRUCT();
						String nomeCLiente = ((String) clienteStruct.getAttributes()[1]);
						
						PessoaJuridica pessoaJuridica;
						
						PessoaFisica fisica;
						
						pessoaJuridica = new PessoaJuridica(0, nomeCLiente, null, null, null , null, null);
						clienteSec = pessoaJuridica;
					}
					
					ServicoEscreventeCliente secTabeliao = new ServicoEscreventeCliente(escreventeSec, atendimentoSec, clienteSec, dataRef);
					listaSec.add(secTabeliao);
				}
			}
			
			ServicoEscreventeCliente sec;
			for(int i = 0; i < listaSec.size(); i++){
				sec = listaSec.get(i);
				RelatorioAtendimento ra = new RelatorioAtendimento(sec.getCliente().getNome(), sec.getEscrevente().getNome(),
						sec.getEscrevente().getMatricula(), sec.getAtendimento().getServico().getDescricao(),
						sec.getAtendimento().getServico().getPreco(), sec.getAtendimento().getServico().getCodigo(),
						sec.getAtendimento().getQuantidade(), sec.getAtendimento().getDesconto(), sec.getDataHora());
				relatoriosAtendimento.add(ra);
			}
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("ERRO AO buscar por tabeliao!");
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		return relatoriosAtendimento;
	}

	@Override
	public void atualizar(Object atendimentoObj) throws SQLException {
		
	}

	@Override
	public void remover(Object idObj) throws SQLException {
		
		int id = (int) idObj;
		
		oracle.conectar();
		
		try {
			
			this.comando = oracle.getConexao().prepareStatement(
						"DELETE FROM TB_PRESTA P WHERE P.ATENDIMENTO.ID = ?");
			
			comando.setInt(1, id);
			
			comando.executeQuery();
			
			this.comando = oracle.getConexao().prepareStatement(
					"DELETE FROM TB_SERVICO_ESCREVENTE_CLIENTE S WHERE S.ATENDIMENTO.ID = ?");
			
			comando.setInt(1, id);
			
			comando.executeQuery();
			
			this.comando = oracle.getConexao().prepareStatement(
					"DELETE FROM TB_ATENDIMENTO A WHERE A.ID = ?");
			
			comando.setInt(1, id);

			comando.executeQuery();
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO ao deletar atendimento!");
		}
		finally{ 
			this.comando.close();
			oracle.desconectar();  
        }
		
	}
}
