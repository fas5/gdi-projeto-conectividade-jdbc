package negocio;

import interfaces.Negocio;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import oracle.sql.REF;
import oracle.sql.STRUCT;
import comunicacao.OracleConnection;
import entidades.Atendimento;
import entidades.Cliente;
import entidades.Endereco;
import entidades.Escrevente;
import entidades.Servico;
import entidades.ServicoEscreventeCliente;
import entidades.Tabeliao;
import entidades.Telefone;

public class TabeliaoNegocio implements Negocio {
	
	private OracleConnection oracle;
	private PreparedStatement comando;

	public TabeliaoNegocio(OracleConnection oracle) {
		this.oracle = oracle;
	}
	
	public String preencherTelefones(int qtdTelefones) {
		String resultado = "tp_telefones(null)";

		if (qtdTelefones != 0) {
			resultado = "tp_telefones(";
			String fone = "tp_telefone(?, ?)";

			for (int i = 0; i < qtdTelefones; i++) {
				resultado += fone;

				if (i < qtdTelefones - 1) {
					resultado += ", ";
				}
			}

			resultado += ")";
		}

		return resultado;
	}
	
	public String preencherServicoEscreventeCliente(int qtdServicoEscreventeCliente) {
		String resultado = "tp_nt_sec(null)";

		if (qtdServicoEscreventeCliente != 0) {
			resultado = "tp_nt_sec(";
			String sec = "(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = ?))";

			for (int i = 0; i < qtdServicoEscreventeCliente; i++) {
				resultado += sec;

				if (i < qtdServicoEscreventeCliente - 1) {
					resultado += ", ";
				}
			}

			resultado += ")";
		}

		return resultado;
	}

	@Override
	public void inserir(Object tabeliaoObj) throws SQLException {
		
		Tabeliao tabeliao = (Tabeliao) tabeliaoObj;

		oracle.conectar();

		try {

			int indice;
			int tamanho = 0;

			tamanho = tabeliao.getTelefones().length;

			this.comando = oracle
					.getConexao()
					.prepareStatement(
							"INSERT INTO tb_tabeliao VALUES (tp_tabeliao(tb_pessoa_seq.NEXTVAL,"
									+ " ?, "
									+ "TO_DATE(?,'DD/MM/YYYY'), "
									+ "tp_endereco(?, ?, ?, ?, ?, ?), "
									+ preencherTelefones(tamanho) // telefone
									+ ",?, "
									+ "(select ref(e) from tb_tabeliao e where (e.matricula = ?)), "
									+ "?, " 
									+ "?, "
									+ "?,"
									+ "?,"
									+ "tp_nt_sec(null)"
									+"))");

			comando.setString(1, tabeliao.getNome());
			comando.setString(2, tabeliao.getDataNascimento());
			comando.setString(3, tabeliao.getEndereco().getLogradouro());
			comando.setString(4, tabeliao.getEndereco().getNumero());
			comando.setString(5, tabeliao.getEndereco().getBairro());
			comando.setString(6, tabeliao.getEndereco().getCidade());
			comando.setString(7, tabeliao.getEndereco().getUf());
			comando.setString(8, tabeliao.getEndereco().getCep());

			indice = 9;
			for (int i = 0; i < tabeliao.getTelefones().length; i++) {
				comando.setString(indice, tabeliao.getTelefones()[i].getDdd());
				comando.setString(indice + 1,
						tabeliao.getTelefones()[i].getNumero());
				indice = indice + 2;
			}
			comando.setInt(indice, tabeliao.getMatricula());
			if (tabeliao.getSupervisor() != null) {
				comando.setInt(indice + 1, tabeliao.getSupervisor()
						.getMatricula());
			} else {
				comando.setInt(indice + 1, 0);
			}
			comando.setDouble(indice + 2, tabeliao.getSalario());
			comando.setString(indice + 3, tabeliao.getDataAdmissao());
			comando.setBytes(indice + 4, tabeliao.getFoto());
			comando.setString(indice + 5, tabeliao.getFuncao());


			comando.executeQuery();

		} catch (Exception ex) {

			ex.printStackTrace();
			System.out.println("ERRO AO INSERIR tabeliao!");
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
	}
	
	public void inserirNestedTable(Object tabeliaoObj, int idAtendimento) throws SQLException{
		
		oracle.conectar();
		Tabeliao tabeliao = (Tabeliao) tabeliaoObj;
		
		try {
			
			this.comando = oracle
					.getConexao()
					.prepareStatement(
							"INSERT INTO TABLE(SELECT SERVICOESCREVENTECLIENTE FROM tb_tabeliao WHERE matricula = ?) VALUES ((SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = ?)))");
			
			comando.setInt(1, tabeliao.getMatricula());
			comando.setInt(2, idAtendimento);

			comando.executeQuery();

		} catch (Exception ex) {

			ex.printStackTrace();
			System.out.println("ERRO AO INSERIR lista!");
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		
	}
	
	public List<Object> buscarPorNome(Object nomePesquisado) throws SQLException {

		oracle.conectar();
		ResultSet resultado;
		Tabeliao tabeliao = null;
		List<Object> tabeliaes = new ArrayList<Object>();

		try {

			this.comando = oracle.getConexao().prepareStatement(
					"SELECT * FROM TB_TABELIAO T WHERE LOWER(T.NOME) LIKE ?");

			this.comando.setString(1, '%' + (String) nomePesquisado+ '%');

			resultado = this.comando.executeQuery();

			String nome, dataNascimento, funcao, dataAdmissaoTabeliao, logradouro, numero, bairro, cidade, uf, cep, nomeSupervisor, dataNascimentoSupervisor, funcaoSupervisor, dataAdmissaoSupervisor, logradouroSupervisor, numeroSupervisor, bairroSupervisor, cidadeSupervisor, ufSupervisor, cepSupervisor;
			int matriculaTabeliao, matriculaSupervisor, idTabeliao, idSupervisor;
			double salarioTabeliao, salarioSupervisor;
			Endereco enderecoTabeliao, enderecoSupervisor;
			Array array, arraySupervisor;
			Object[] objetoTelefone, objetoTelefoneSupervisor;
			Tabeliao tabeliaoSupervisor = null;
			Telefone[] telefones, telefonesSupervisor;
			Object objetoEndereco, objetoEnderecoSupervisor;
			byte[] foto = null;

			while (resultado.next()) {
				idTabeliao = ((BigDecimal)resultado.getBigDecimal("ID")).intValue();
				nome = (String) resultado.getObject("NOME");
				dataNascimento = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATANASCIMENTO"));

				objetoEndereco = (Object) resultado.getObject("ENDERECO");
				logradouro = (String) ((STRUCT) objetoEndereco).getAttributes()[0];
				numero = (String) ((STRUCT) objetoEndereco).getAttributes()[1];
				bairro = (String) ((STRUCT) objetoEndereco).getAttributes()[2];
				cidade = (String) ((STRUCT) objetoEndereco).getAttributes()[3];
				uf = (String) ((STRUCT) objetoEndereco).getAttributes()[4];
				cep = (String) ((STRUCT) objetoEndereco).getAttributes()[5];

				enderecoTabeliao = new Endereco(logradouro, numero, bairro,
						cidade, uf, cep);

				array = resultado.getArray("TELEFONES");
				objetoTelefone = (Object[]) array.getArray();

				telefones = new Telefone[3];

				for (int i = 0; i < objetoTelefone.length; i++) {
					Object telefone_Saida = (Object) objetoTelefone[i];
					String ddd = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[0];
					String numeroTel = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[1];
					Telefone tel = new Telefone(ddd, numeroTel);
					telefones[i] = tel;
				}

				matriculaTabeliao = resultado.getBigDecimal("MATRICULA")
						.intValue();
				salarioTabeliao = resultado.getDouble("SALARIO");
				dataAdmissaoTabeliao = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATAADMISSAO"));
				foto = resultado.getBytes("FOTO");
				
				funcao = resultado.getString("FUNCAO");

				REF supervisor = (REF) resultado.getRef("SUPERVISOR");

				if (supervisor != null) {

					STRUCT supervisorStruct = (STRUCT) supervisor.getSTRUCT();

					idSupervisor = ((BigDecimal) supervisorStruct.getAttributes()[0]).intValue();
					nomeSupervisor = (String) supervisorStruct.getAttributes()[1];
					dataNascimentoSupervisor = new SimpleDateFormat(
							"MM/dd/yyyy").format(supervisorStruct
							.getAttributes()[2]);

					objetoEnderecoSupervisor = (Object) supervisorStruct
							.getAttributes()[3];

					logradouroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[0];
					numeroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[1];
					bairroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[2];
					cidadeSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[3];
					ufSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[4];
					cepSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[5];

					enderecoSupervisor = new Endereco(logradouroSupervisor,
							numeroSupervisor, bairroSupervisor,
							cidadeSupervisor, ufSupervisor, cepSupervisor);

					arraySupervisor = (Array) supervisorStruct.getAttributes()[4];
					objetoTelefoneSupervisor = (Object[]) arraySupervisor
							.getArray();

					telefonesSupervisor = new Telefone[3];

					for (int i = 0; i < objetoTelefoneSupervisor.length; i++) {
						Object telefone_Saida = (Object) objetoTelefoneSupervisor[i];
						String ddd = (String) ((STRUCT) telefone_Saida)
								.getAttributes()[0];
						String numeroTel = (String) ((STRUCT) telefone_Saida)
								.getAttributes()[1];
						Telefone tel = new Telefone(ddd, numeroTel);
						telefonesSupervisor[i] = tel;
					}

					matriculaSupervisor = ((BigDecimal) supervisorStruct
							.getAttributes()[5]).intValue();
					salarioSupervisor = ((BigDecimal) supervisorStruct
							.getAttributes()[7]).doubleValue();
					dataAdmissaoSupervisor = new SimpleDateFormat("MM/dd/yyyy")
							.format(supervisorStruct.getAttributes()[8]);
					funcaoSupervisor = ((String) supervisorStruct
							.getAttributes()[10]);

					tabeliaoSupervisor = new Tabeliao(idSupervisor, nomeSupervisor,
							dataNascimentoSupervisor, enderecoSupervisor,
							telefonesSupervisor, matriculaSupervisor, null,
							salarioSupervisor, dataAdmissaoSupervisor, funcaoSupervisor, null, null);

				}

				tabeliao = new Tabeliao(idTabeliao, nome, dataNascimento,
						enderecoTabeliao, telefones, matriculaTabeliao,
						tabeliaoSupervisor, salarioTabeliao,
						dataAdmissaoTabeliao, funcao, null, foto);
				
				tabeliaes.add(tabeliao);
			}
		} catch (Exception ex) {

			ex.printStackTrace();
			System.out.println("ERRO ao buscar tabeliao!");
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		return tabeliaes;
	}

	@Override
	public Object buscar(Object matricula) throws SQLException {

		oracle.conectar();
		ResultSet resultado;
		Tabeliao tabeliao = null;

		try {

			this.comando = oracle.getConexao().prepareStatement(
					"SELECT * FROM TB_TABELIAO T WHERE T.MATRICULA = ?");

			this.comando.setInt(1, (int) matricula);

			resultado = this.comando.executeQuery();

			String nome, dataNascimento, dataAdmissao, logradouro, numero, bairro, cidade, uf, cep, funcao, nomeSupervisor, dataNascimentoSupervisor, dataAdmissaoSupervisor, logradouroSupervisor, numeroSupervisor, bairroSupervisor, cidadeSupervisor, ufSupervisor, cepSupervisor, funcaoSupervisor;
			int matriculaTabeliao, matriculaSupervisor, idTabeliao, idSupervisor;
			double salario, salarioSupervisor;
			Endereco enderecoTabeliao, enderecoSupervisor;
			Array array, arraySupervisor;
			Object[] objetoTelefone, objetoTelefoneSupervisor;
			Tabeliao tabeliaoSupervisor = null;
			Telefone[] telefones, telefonesSupervisor;
			Object objetoEndereco, objetoEnderecoSupervisor;
			byte[] foto;

			while (resultado.next()) {
				idTabeliao = ((BigDecimal)resultado.getBigDecimal("ID")).intValue();
				nome = (String) resultado.getObject("NOME");
				dataNascimento = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATANASCIMENTO"));

				objetoEndereco = (Object) resultado.getObject("ENDERECO");
				logradouro = (String) ((STRUCT) objetoEndereco).getAttributes()[0];
				numero = (String) ((STRUCT) objetoEndereco).getAttributes()[1];
				bairro = (String) ((STRUCT) objetoEndereco).getAttributes()[2];
				cidade = (String) ((STRUCT) objetoEndereco).getAttributes()[3];
				uf = (String) ((STRUCT) objetoEndereco).getAttributes()[4];
				cep = (String) ((STRUCT) objetoEndereco).getAttributes()[5];

				enderecoTabeliao = new Endereco(logradouro, numero, bairro,
						cidade, uf, cep);

				array = resultado.getArray("TELEFONES");
				objetoTelefone = (Object[]) array.getArray();

				telefones = new Telefone[3];

				for (int i = 0; i < objetoTelefone.length; i++) {
					Object telefone_Saida = (Object) objetoTelefone[i];
					String ddd = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[0];
					String numeroTel = (String) ((STRUCT) telefone_Saida)
							.getAttributes()[1];
					Telefone tel = new Telefone(ddd, numeroTel);
					telefones[i] = tel;
				}

				matriculaTabeliao = resultado.getBigDecimal("MATRICULA")
						.intValue();
				salario = resultado.getDouble("SALARIO");
				dataAdmissao = new SimpleDateFormat("MM/dd/yyyy")
						.format(resultado.getObject("DATAADMISSAO"));
				funcao = resultado.getString("FUNCAO");
				foto = resultado.getBytes("FOTO");
				
				REF supervisor = (REF) resultado.getRef("SUPERVISOR");

				if (supervisor != null) {

					STRUCT supervisorStruct = (STRUCT) supervisor.getSTRUCT();

					idSupervisor = ((BigDecimal) supervisorStruct.getAttributes()[0]).intValue();
					nomeSupervisor = (String) supervisorStruct.getAttributes()[1];
					dataNascimentoSupervisor = new SimpleDateFormat(
							"MM/dd/yyyy").format(supervisorStruct
							.getAttributes()[2]);

					objetoEnderecoSupervisor = (Object) supervisorStruct
							.getAttributes()[3];

					logradouroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[0];
					numeroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[1];
					bairroSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[2];
					cidadeSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[3];
					ufSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[4];
					cepSupervisor = (String) ((STRUCT) objetoEnderecoSupervisor)
							.getAttributes()[5];

					enderecoSupervisor = new Endereco(logradouroSupervisor,
							numeroSupervisor, bairroSupervisor,
							cidadeSupervisor, ufSupervisor, cepSupervisor);

					arraySupervisor = (Array) supervisorStruct.getAttributes()[4];
					objetoTelefoneSupervisor = (Object[]) arraySupervisor
							.getArray();

					telefonesSupervisor = new Telefone[3];

					for (int i = 0; i < objetoTelefoneSupervisor.length; i++) {
						Object telefone_Saida = (Object) objetoTelefoneSupervisor[i];
						String ddd = (String) ((STRUCT) telefone_Saida)
								.getAttributes()[0];
						String numeroTel = (String) ((STRUCT) telefone_Saida)
								.getAttributes()[1];
						Telefone tel = new Telefone(ddd, numeroTel);
						telefonesSupervisor[i] = tel;
					}

					matriculaSupervisor = ((BigDecimal) supervisorStruct
							.getAttributes()[5]).intValue();
					salarioSupervisor = ((BigDecimal) supervisorStruct
							.getAttributes()[7]).doubleValue();
					dataAdmissaoSupervisor = new SimpleDateFormat("MM/dd/yyyy")
							.format(supervisorStruct.getAttributes()[8]);
					funcaoSupervisor = ((String) supervisorStruct
							.getAttributes()[9]);

					tabeliaoSupervisor = new Tabeliao(idSupervisor, nomeSupervisor,
							dataNascimentoSupervisor, enderecoSupervisor,
							telefonesSupervisor, matriculaSupervisor, null,
							salarioSupervisor, dataAdmissaoSupervisor, funcaoSupervisor, null, null);

				}

				tabeliao = new Tabeliao(idTabeliao, nome, dataNascimento, enderecoTabeliao, telefones, matriculaTabeliao, tabeliaoSupervisor, salario, dataAdmissao, funcao, null, foto);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		return tabeliao;
		
	}
	
	public List<Tabeliao> buscarTodasMatriculas() throws SQLException {

		oracle.conectar();
		ResultSet resultado;
		Tabeliao tabeliao = null;
		List<Tabeliao> tabeliaes = new ArrayList<Tabeliao>();
		
		try{
			
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT T.NOME, T.MATRICULA FROM TB_TABELIAO T");

			resultado = this.comando.executeQuery();
			
			int matricula;
			String nome;
			
			while(resultado.next()){
				matricula = resultado.getInt("MATRICULA");
				nome = resultado.getString("NOME");
				
				tabeliao = new Tabeliao(0, nome, null, null, null, matricula, null, 0, null, null, null, null);
				
				tabeliaes.add(tabeliao);
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		
		return tabeliaes;
	}

	@Override
	public void atualizar(Object tabeliaoObj) throws SQLException {
		
		Tabeliao tabeliao = (Tabeliao) tabeliaoObj;

		oracle.conectar();

		try {

			int indice;
			int tamanho = tabeliao.getTelefones().length;
			this.comando = oracle.getConexao().prepareStatement(
					"UPDATE tb_tabeliao SET NOME = ?,"
							+ "DATANASCIMENTO = TO_DATE(?,'DD/MM/YYYY'),"
							+ "Endereco = tp_endereco(?, ?, ?, ?, ?, ?),"
							+ "Telefones = "+ preencherTelefones(tamanho) // telefone
							+ ",Supervisor = (select ref(e) from tb_tabeliao e where (e.matricula = ?)),"
							+ "SALARIO = ?, "
							+ "DATAADMISSAO = TO_DATE(?,'DD/MM/YYYY'),"
							+ "FUNCAO = ?,"
							+ "FOTO = ? "
							+ "WHERE MATRICULA = ?");

			comando.setString(1, tabeliao.getNome());
			comando.setString(2, tabeliao.getDataNascimento());
			comando.setString(3, tabeliao.getEndereco().getLogradouro());
			comando.setString(4, tabeliao.getEndereco().getNumero());
			comando.setString(5, tabeliao.getEndereco().getBairro());
			comando.setString(6, tabeliao.getEndereco().getCidade());
			comando.setString(7, tabeliao.getEndereco().getUf());
			comando.setString(8, tabeliao.getEndereco().getCep());

			indice = 9;
			for (int i = 0; i < tabeliao.getTelefones().length; i++) {
				comando.setString(indice,
						tabeliao.getTelefones()[i].getDdd());
				comando.setString(indice + 1,
						tabeliao.getTelefones()[i].getNumero());
				indice = indice + 2;
			}
			
			if (tabeliao.getSupervisor() != null) {
				comando.setInt(indice, tabeliao.getSupervisor()
						.getMatricula());
			} else {
				comando.setInt(indice, 0);
			}
			comando.setDouble(indice + 1, tabeliao.getSalario());
			comando.setString(indice + 2, tabeliao.getDataAdmissao());
			comando.setString(indice + 3, tabeliao.getFuncao());
			comando.setBytes(indice + 4, tabeliao.getFoto());
			comando.setInt(indice + 5, tabeliao.getMatricula());

			comando.executeQuery();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
	}

	@Override
	public void remover(Object matriculaObject) throws SQLException {
		
		int matricula = (int) matriculaObject;

		oracle.conectar();

		try {

			this.comando = oracle.getConexao().prepareStatement(
					"DELETE FROM TB_TABELIAO T WHERE T.MATRICULA = ?");

			comando.setInt(1, matricula);
			comando.executeQuery();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		
	}
}
