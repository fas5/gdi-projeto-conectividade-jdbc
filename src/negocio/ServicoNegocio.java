package negocio;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import comunicacao.OracleConnection;
import entidades.Servico;
import interfaces.Negocio;

public class ServicoNegocio  implements Negocio{
	
	private OracleConnection oracle;
	private PreparedStatement comando;
	
	public ServicoNegocio(OracleConnection oracle){
		this.oracle = oracle;
	}

	@Override
	public void inserir(Object servicoObj) throws SQLException {

		Servico servico = (Servico)servicoObj;
		
		oracle.conectar();
		
		try {
			
			this.comando = oracle.getConexao().prepareStatement(
					"INSERT INTO tb_servico VALUES (tp_servico(tb_servico_seq.NEXTVAL,?, ?, ?))");
			
			comando.setString(1, servico.getDescricao());
			comando.setDouble(2, servico.getPreco());
			comando.setInt(3, servico.getAutorizado());
			
			comando.executeQuery();
			
		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println("ERRO ao inserir servi�o!");
		}finally{
			this.comando.close();
			oracle.desconectar(); 
		}
		
	}

	public List<Object> buscarTodos() throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		Servico servico = null;
		List<Object> servicos = new ArrayList<Object>();  
		int codigo, autorizado;
		double preco;
		String descricao;
		
		try{
			
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT * FROM TB_SERVICO");
			
			resultado = this.comando.executeQuery();
			
			while(resultado.next()){
				
				codigo = resultado.getInt("CODIGO");
				descricao = resultado.getString("DESCRICAO");
				autorizado = resultado.getInt("AUTORIZADO");
				preco = resultado.getDouble("PRECO");
				
				servico = new Servico(codigo, descricao, preco, autorizado);
				
				servicos.add(servico);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println("ERRO ao buscar servi�os!");
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		
		return servicos;
	}

	@Override
	public void atualizar(Object servicoObj) throws SQLException {
		Servico servico = (Servico)servicoObj;
		
		oracle.conectar();
		
		try {
			this.comando = oracle.getConexao().prepareStatement(
						"UPDATE tb_servico "
						+ "SET DESCRICAO = ?,"
						+ "PRECO = ?,"
						+ "AUTORIZADO = ? "
						+ "WHERE CODIGO = ?");

			comando.setString(1, servico.getDescricao());
			comando.setDouble(2, servico.getPreco());
			comando.setInt(3, servico.getAutorizado());
			comando.setInt(4, servico.getCodigo());
			
			comando.executeQuery();
			
			this.comando = oracle.getConexao().prepareStatement("commit");
			
			comando.executeQuery();
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			System.out.println("ERRO AO ATUALIZAR servico!");
		}
		finally{ 
			this.comando.close();
			oracle.desconectar();  
        }
		
	}

	@Override
	public void remover(Object codigoObj) throws SQLException {
		
		int codigo = (int) codigoObj;
		
		oracle.conectar();
		
		try {
			
			this.comando = oracle.getConexao().prepareStatement(
					"DELETE FROM tb_servico WHERE CODIGO = ?");
			
			comando.setInt(1, codigo);
			
			comando.executeQuery();
			
		}catch(Exception ex){
			
			ex.printStackTrace();
			System.out.println("ERRO ao REMOVER servi�o!");
			
		}finally{
			
			this.comando.close();
			oracle.desconectar(); 
		}
		
	}
	
	@Override
	public Object buscar(Object codigoObj) throws SQLException {
		
		oracle.conectar();
		ResultSet resultado;
		Object servico = null;
		int codigoServico = (int) codigoObj;  
		int codigo, autorizado;
		double preco;
		String descricao;
		
		try{
			
			this.comando = oracle.getConexao().prepareStatement(
					"SELECT * FROM TB_SERVICO WHERE CODIGO = ?");
			
			this.comando.setInt(1, codigoServico);
			
			resultado = this.comando.executeQuery();
			
			while(resultado.next()){
				
				codigo = resultado.getInt("CODIGO");
				descricao = resultado.getString("DESCRICAO");
				autorizado = resultado.getInt("AUTORIZADO");
				preco = resultado.getDouble("PRECO");
				
				servico = new Servico(codigo, descricao, preco, autorizado);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println("ERRO ao buscar servi�o!");
		} finally {
			this.comando.close();
			oracle.desconectar();
		}
		
		return servico;
	}
}
