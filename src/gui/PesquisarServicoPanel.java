package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JComboBox;

import controladores.ControladorServico;
import entidades.Servico;

public class PesquisarServicoPanel extends JPanel {

	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JLabel labelPesquisarServico;
	
	private JButton buttonPesquisar;
	private JComboBox<String> comboBox_servicos;
	
	ControladorServico controladorServico;
	Servico servicoSelecionado = null;
	List<Object> listaServico = new ArrayList<Object>();
	private JLabel lblServicos;
	private JButton btnPesquisar;
	private EditarServicoDialog editarServicoDialog;
	
	public void atualizarLista(){
		try {
			listaServico = controladorServico.buscarTodos();
			comboBox_servicos.removeAllItems();
			comboBox_servicos.addItem("Selecione um Servi�o...");
			for (int i = 0; i < listaServico.size(); i++) {
				String nome = (((Servico)listaServico.get(i)).getDescricao());
				comboBox_servicos.addItem(nome);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
	/**
	 * Create the panel.
	 * @throws SQLException 
	 */
	public PesquisarServicoPanel(CardLayout cardLayout, JPanel contentPanel) throws SQLException {
		setLayout(null);
		
		controladorServico = new ControladorServico();
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelPesquisarServico = new JLabel("Pesquisar Servi�o");
		labelPesquisarServico.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelPesquisarServico.setBounds(10, 33, 239, 31);
		add(labelPesquisarServico);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		lblServicos = new JLabel("Servi\u00E7os");
		lblServicos.setBounds(168, 116, 68, 14);
		add(lblServicos);
		
		comboBox_servicos = new JComboBox<String>();
		comboBox_servicos.setBounds(168, 135, 466, 20);
		add(comboBox_servicos);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int indiceServicoSelecionado = comboBox_servicos.getSelectedIndex();
				servicoSelecionado = (Servico)listaServico.get(indiceServicoSelecionado-1);
				try {
					controladorServico = new ControladorServico();
					if(indiceServicoSelecionado > 0) {
						editarServicoDialog = new EditarServicoDialog(servicoSelecionado);
						editarServicoDialog.abrir();
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnPesquisar.setBounds(346, 166, 111, 23);
		add(btnPesquisar);
	}
}
