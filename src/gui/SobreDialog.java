package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Window.Type;

public class SobreDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			SobreDialog dialog = new SobreDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public SobreDialog() {
		setModal(true);
		setType(Type.POPUP);
		setAlwaysOnTop(true);
		setResizable(false);
		setTitle("Sobre");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JButton okButton = new JButton("OK");
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			okButton.setBounds(184, 227, 66, 23);
			contentPanel.add(okButton);
			okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
		{
			JLabel lblEquipe = new JLabel("Equipe 5");
			lblEquipe.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblEquipe.setHorizontalAlignment(SwingConstants.CENTER);
			lblEquipe.setBounds(10, 16, 424, 14);
			contentPanel.add(lblEquipe);
		}
		{
			JLabel lblNewLabel = new JLabel("Andreza Fab\u00EDola Vieira de Abreu (afva)");
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel.setBounds(10, 46, 424, 14);
			contentPanel.add(lblNewLabel);
		}
		{
			JLabel lblAntnioCarlosPortela = new JLabel("Ant\u00F4nio Carlos Portela Rodrigues (acpr)");
			lblAntnioCarlosPortela.setHorizontalAlignment(SwingConstants.CENTER);
			lblAntnioCarlosPortela.setBounds(10, 71, 424, 14);
			contentPanel.add(lblAntnioCarlosPortela);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Caio Souza Fonseca (csf2)");
			lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_1.setBounds(10, 96, 424, 14);
			contentPanel.add(lblNewLabel_1);
		}
		{
			JLabel lblDanielCndidodcc = new JLabel("Daniel C\u00E2ndido (dcc4)");
			lblDanielCndidodcc.setHorizontalAlignment(SwingConstants.CENTER);
			lblDanielCndidodcc.setBounds(10, 121, 424, 14);
			contentPanel.add(lblDanielCndidodcc);
		}
		{
			JLabel lblFelipeDeAssis = new JLabel("Felipe de Assis Souza (fas5)");
			lblFelipeDeAssis.setHorizontalAlignment(SwingConstants.CENTER);
			lblFelipeDeAssis.setBounds(10, 145, 424, 14);
			contentPanel.add(lblFelipeDeAssis);
		}
		{
			JLabel lblFilipeMendesMariz = new JLabel("Filipe Mendes Mariz (fmm4)");
			lblFilipeMendesMariz.setHorizontalAlignment(SwingConstants.CENTER);
			lblFilipeMendesMariz.setBounds(10, 170, 424, 14);
			contentPanel.add(lblFilipeMendesMariz);
		}
		{
			JLabel lblJulianeSabrinaMagalhes = new JLabel("Juliane Sabrina Magalh\u00E3es do Nascimento (jsmn)");
			lblJulianeSabrinaMagalhes.setHorizontalAlignment(SwingConstants.CENTER);
			lblJulianeSabrinaMagalhes.setBounds(10, 195, 424, 14);
			contentPanel.add(lblJulianeSabrinaMagalhes);
		}
	}

}
