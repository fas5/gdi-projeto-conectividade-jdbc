package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import controladores.ControladorServico;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import controladores.ControladorEscrevente;
import controladores.ControladorPessoaFisica;
import controladores.ControladorPessoaJuridica;
import controladores.ControladorTabeliao;
import controladores.ControladorAtendimento;
import entidades.Escrevente;
import entidades.PessoaFisica;
import entidades.PessoaJuridica;
import entidades.Servico;
import entidades.Tabeliao;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class AtendimentoPanel extends JPanel implements PropertyChangeListener, ChangeListener {

	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JRadioButton radioPessoaFisica;
	private JRadioButton radioPessoaJuridica;
	private JFormattedTextField formattedTextField_cpf;
	private JComboBox<String> comboBox_MatriculaEscrevente;
	private JComboBox<String> comboBox_Servico;
	private JComboBox<String> comboBox_MatriculaTabeliao;
	private JFormattedTextField formattedTextField_Preco;
	private JSpinner spinner_Quantidade;
	private JFormattedTextField formattedTextField_Desconto;
	private JFormattedTextField formattedTextField_Total;
	
	private AlertDialog alerta;
	//Formats to format and parse numbers
    private NumberFormat precoDisplayFormat;
    private NumberFormat precoEditFormat;
    private NumberFormat descontoDisplayFormat;
    private NumberFormat descontoEditFormat;
    private NumberFormat totalFormat;
	
	private JLabel labelIniciarAtendimento;
	private JLabel cpf_cnpj;
	private JLabel lblServico;
	private JLabel lblMatriculaTabeliao;
	private JLabel lblPreco;
	private JLabel lblTotal;
	
	private int indiceServicoSelecionado;
	
	List<Object> listaServico;
	ControladorServico controladorServico;
	ControladorEscrevente controladorEscrevente;
	ControladorTabeliao controladorTabeliao;
	ControladorPessoaFisica controladorPf;
	ControladorPessoaJuridica controladorPj;
	ControladorAtendimento controladorAtendimento;
	
	Object servicoSelecionado;
	
	private JButton buttonFinalizar;
	
	private double quantia = 0.0;
	private double percentual = 0.0; //0%
	private int quantidade = 1;
	private double totalPagamento = calcularTotal(quantia, percentual, quantidade);
	
	public void atualizarListaServico(){
		try {
			listaServico = controladorServico.buscarTodos();
			comboBox_Servico.removeAllItems();
			comboBox_Servico.addItem("Selecione um Servi�o...");
			for (int i = 0; i < listaServico.size(); i++) {
				String nome = (((Servico)listaServico.get(i)).getDescricao());
				comboBox_Servico.addItem(nome);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void atualizarListaMatriculaEscrevente(){
		try {
			List<Escrevente> lista = controladorEscrevente.buscarMatriculas();
			comboBox_MatriculaEscrevente.removeAllItems();
			comboBox_MatriculaEscrevente.addItem("Selecione...");
			String matricula;
			for (int i = 0; i < lista.size(); i++) {
				matricula = Integer.toString(lista.get(i).getMatricula());
				comboBox_MatriculaEscrevente.addItem(matricula);
			}
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	public void atualizarListaMatriculaTabeliao(){
		try {
			List<Tabeliao> lista = controladorTabeliao.buscarMatriculas();
			comboBox_MatriculaTabeliao.removeAllItems();
			comboBox_MatriculaTabeliao.addItem("Selecione...");
			String matricula;
			for (int i = 0; i < lista.size(); i++) {
				matricula = Integer.toString(lista.get(i).getMatricula());
				comboBox_MatriculaTabeliao.addItem(matricula);
			}
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Create the panel.
	 * @throws SQLException 
	 */
	public AtendimentoPanel(CardLayout cardLayout, JPanel contentPanel) throws SQLException {
		listaServico = new ArrayList<Object>();
		controladorServico = new ControladorServico();
		controladorEscrevente = new ControladorEscrevente();
		controladorTabeliao = new ControladorTabeliao();
		controladorAtendimento = new ControladorAtendimento();
		servicoSelecionado = null;
		
		setLayout(null);
		setUpFormats();
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelIniciarAtendimento = new JLabel("Iniciar Atendimento");
		labelIniciarAtendimento.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelIniciarAtendimento.setBounds(10, 33, 239, 31);
		add(labelIniciarAtendimento);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		radioPessoaFisica = new JRadioButton("Pessoa F\u00EDsica");
		radioPessoaFisica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radioPessoaFisica.isSelected()) {
					radioPessoaFisica.setSelected(true);
					radioPessoaJuridica.setSelected(false);
					
					cpf_cnpj.setText("CPF Cliente");
					
					buttonFinalizar.setEnabled(true);
				}
			}
		});
		radioPessoaFisica.setBounds(10, 97, 109, 23);
		add(radioPessoaFisica);
		
		radioPessoaJuridica = new JRadioButton("Pessoa Jur\u00EDdica");
		radioPessoaJuridica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radioPessoaJuridica.isSelected()) {
					radioPessoaFisica.setSelected(false);
					radioPessoaJuridica.setSelected(true);

					cpf_cnpj.setText("CNPJ Cliente");
					
					buttonFinalizar.setEnabled(true);
				}
			}
		});
		radioPessoaJuridica.setBounds(121, 97, 142, 23);
		add(radioPessoaJuridica);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 137, 765, 15);
		add(separator_1);
		
		cpf_cnpj = new JLabel("CPF Cliente");
		cpf_cnpj.setBounds(153, 163, 132, 23);
		add(cpf_cnpj);
		
		formattedTextField_cpf = new JFormattedTextField();
		formattedTextField_cpf.setBounds(153, 186, 230, 20);
		add(formattedTextField_cpf);
			
		JLabel lblMatriculaEscrevente = new JLabel("Matr\u00EDcula Escrevente");
		lblMatriculaEscrevente.setBounds(393, 167, 220, 14);
		add(lblMatriculaEscrevente);
		
		comboBox_MatriculaEscrevente = new JComboBox<String>();

		comboBox_MatriculaEscrevente.setBounds(393, 186, 220, 20);
		add(comboBox_MatriculaEscrevente);
		
		lblMatriculaTabeliao = new JLabel("Matr\u00EDcula Tabeli\u00E3o");
		lblMatriculaTabeliao.setBounds(153, 267, 230, 14);
		add(lblMatriculaTabeliao);
		
		comboBox_MatriculaTabeliao = new JComboBox<String>();
		comboBox_MatriculaTabeliao.setEnabled(false);
	
		comboBox_MatriculaTabeliao.setBounds(153, 288, 230, 20);
		add(comboBox_MatriculaTabeliao);		
		
		lblServico = new JLabel("Servi\u00E7o");
		lblServico.setBounds(153, 217, 93, 14);
		add(lblServico);
		
		// <PRECO>
		lblPreco = new JLabel("Pre\u00E7o");
		lblPreco.setBounds(504, 267, 46, 14);
		add(lblPreco);
		
		formattedTextField_Preco = new JFormattedTextField(
				new DefaultFormatterFactory(
						new NumberFormatter(precoDisplayFormat),
						new NumberFormatter(precoDisplayFormat),
						new NumberFormatter(precoEditFormat)));
		//formattedTextField_Preco.setEditable(false);
		formattedTextField_Preco.setValue(new Double(quantia));
		formattedTextField_Preco.setBounds(504, 288, 109, 20);
		formattedTextField_Preco.addPropertyChangeListener("value", this);
		add(formattedTextField_Preco);
		// </PRECO>
		
		// <QUANTIDADE>
		JLabel lblQuantidade = new JLabel("Quantidade");
		lblQuantidade.setBounds(504, 319, 109, 14);
		add(lblQuantidade);
		
		SpinnerModel model = new SpinnerNumberModel(quantidade, 1, 15, 1);
		spinner_Quantidade = new JSpinner(model);
		((DefaultEditor) spinner_Quantidade.getEditor()).getTextField().setEditable(false);
		spinner_Quantidade.addChangeListener(this);
		spinner_Quantidade.setValue(new Integer(quantidade));
		spinner_Quantidade.setBounds(504, 340, 109, 20);
		add(spinner_Quantidade);
		// </QUANTIDADE>
		
		// <DESCONTO>
		NumberFormatter percentEditFormatter =
                new NumberFormatter(descontoEditFormat) {
            public String valueToString(Object o)
                  throws ParseException {
                Number number = (Number)o;
                if (number != null) {
                    double d = number.doubleValue() * 100.0;
                    number = new Double(d);
                }
                return super.valueToString(number);
            }
            public Object stringToValue(String s)
                   throws ParseException {
                Number number = (Number)super.stringToValue(s);
                if (number != null) {
                    double d = number.doubleValue() / 100.0;
                    number = new Double(d);
                }
                return number;
            }
        };
		
		formattedTextField_Desconto = new JFormattedTextField(
				new DefaultFormatterFactory(
                new NumberFormatter(descontoDisplayFormat),
                new NumberFormatter(descontoDisplayFormat),
                percentEditFormatter));
		formattedTextField_Desconto.setValue(new Double(percentual));
		formattedTextField_Desconto.setBounds(504, 392, 109, 20);
		formattedTextField_Desconto.addPropertyChangeListener("value", this);
		add(formattedTextField_Desconto);
		
		JLabel lblDesconto = new JLabel("Desconto");
		lblDesconto.setBounds(504, 371, 93, 14);
		add(lblDesconto);
		// </DESCONTO>
		
		// <TOTAL>
		lblTotal = new JLabel("TOTAL A PAGAR");
		lblTotal.setBounds(504, 423, 109, 14);
		add(lblTotal);
		
		formattedTextField_Total = new JFormattedTextField(totalFormat);
		formattedTextField_Total.setValue(totalPagamento);
		formattedTextField_Total.setEditable(false);
		formattedTextField_Total.setBounds(504, 442, 109, 20);
		add(formattedTextField_Total);
		// </TOTAL>
		
		try {
			listaServico = controladorServico.buscarTodos();
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		comboBox_Servico = new JComboBox<String>();
		comboBox_Servico.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				indiceServicoSelecionado = comboBox_Servico.getSelectedIndex();
				
				if(indiceServicoSelecionado != -1 && indiceServicoSelecionado != 0){
					servicoSelecionado = listaServico.get(indiceServicoSelecionado-1);
				}

				if(servicoSelecionado != null){
					formattedTextField_Preco.setValue(((Servico)servicoSelecionado).getPreco());
					if(((Servico)servicoSelecionado).getAutorizado() == 0){
						comboBox_MatriculaTabeliao.setEnabled(true);
					}
				}
				
			}
		});
		comboBox_Servico.setBounds(153, 236, 460, 20);
		add(comboBox_Servico);

		buttonFinalizar = new JButton("Finalizar");
		buttonFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					controladorPf = new ControladorPessoaFisica();
					controladorPj = new ControladorPessoaJuridica();
					
					String cpf = formattedTextField_cpf.getText().replaceAll("-", "").replaceAll("\\.", "");
					PessoaFisica pf = null;
					PessoaJuridica pj  = null;
					
					int matriculaEscrevente = Integer.parseInt((String) comboBox_MatriculaEscrevente.getSelectedItem());
					Escrevente escrevente = (Escrevente)controladorEscrevente.buscar(matriculaEscrevente);

					Servico servico = (Servico) servicoSelecionado;
					
					int autorizado = ((Servico)servicoSelecionado).getAutorizado();
					
					double preco = servico.getPreco();
					
					int quantidade = (int)spinner_Quantidade.getValue();
					double desconto1 = (double) formattedTextField_Desconto.getValue();
					
					int desconto = (int) desconto1;
					
					Tabeliao tabeliao = null;
					
					if(autorizado == 0){
						int matriculaTabeliao = Integer.parseInt((String) comboBox_MatriculaTabeliao.getSelectedItem());
						tabeliao = (Tabeliao)controladorTabeliao.buscar(matriculaTabeliao);
					}
					
					if(radioPessoaFisica.isSelected()){
						pf = (PessoaFisica) controladorPf.pesquisarPorCpf(cpf);
						
					}else{
						pj = (PessoaJuridica) controladorPj.pesquisarPorCnpj(cpf);
					}
					
					if(pf == null && pj == null){
						alerta = new AlertDialog("Cliente n\u00E3o encontrado!");
						alerta.setVisible(true);
					}else{
						controladorAtendimento.inserirAtendimentoPessoaFisicaJuridica(pf, pj, escrevente, tabeliao, servico, preco, quantidade, desconto);
						alerta = new AlertDialog("Atendimento finalizado com sucesso!");
						alerta.setVisible(true);
						resetCampos();
					}
					
				} catch (SQLException e) {
					alerta = new AlertDialog("Ocorreu um erro!");
					alerta.setVisible(true);
					e.printStackTrace();
				}
			}
		});
		buttonFinalizar.setEnabled(false);
		buttonFinalizar.setBounds(671, 526, 104, 23);
		add(buttonFinalizar);
	}

	// Caso o valor da Quantidade mude:
	@Override
	public void stateChanged(ChangeEvent arg0) {
		quantia = ((Number)formattedTextField_Preco.getValue()).doubleValue();
		percentual = ((Number)formattedTextField_Desconto.getValue()).doubleValue();
		quantidade = ((Number)spinner_Quantidade.getModel().getValue()).intValue();
	
	double totalPagamento = calcularTotal(quantia, percentual, quantidade);
	formattedTextField_Total.setValue(new Double(totalPagamento));
		
	}
	
	// Caso o valor do Pre�o e/ou do Desconto mude(m):
	@Override
	public void propertyChange(PropertyChangeEvent e) {		
		quantia = ((Number)formattedTextField_Preco.getValue()).doubleValue();
		percentual = ((Number)formattedTextField_Desconto.getValue()).doubleValue();
		quantidade = ((Number)spinner_Quantidade.getModel().getValue()).intValue();
		
		double totalPagamento = calcularTotal(quantia, percentual, quantidade);
		formattedTextField_Total.setValue(new Double(totalPagamento));
	}
	
	public double calcularTotal(double quantia, double percentual, int quantidade) {
		double total = quantia*quantidade;
		
		return total - (total*percentual);
	}
	
	//Create and set up number formats. These objects also
    //parse numbers input by user.
    private void setUpFormats() {
        precoDisplayFormat = NumberFormat.getCurrencyInstance();
        precoDisplayFormat.setMinimumFractionDigits(0);
        precoEditFormat = NumberFormat.getNumberInstance();

        descontoDisplayFormat = NumberFormat.getPercentInstance();
        descontoDisplayFormat.setMinimumFractionDigits(2);
        descontoEditFormat = NumberFormat.getNumberInstance();
        descontoEditFormat.setMinimumFractionDigits(2);

        totalFormat = NumberFormat.getCurrencyInstance();
    }
    
	public void resetCampos(){
		radioPessoaFisica.setSelected(false);
		radioPessoaJuridica.setSelected(false);
		formattedTextField_cpf.setText("");
		comboBox_MatriculaEscrevente.setSelectedIndex(0);
		comboBox_Servico.setSelectedIndex(0);
		comboBox_MatriculaTabeliao.setSelectedIndex(0);
		formattedTextField_Preco.setValue(0);
		spinner_Quantidade.setValue(0);
		formattedTextField_Desconto.setValue(0);
		formattedTextField_Total.setValue(0);
	}
	
}
