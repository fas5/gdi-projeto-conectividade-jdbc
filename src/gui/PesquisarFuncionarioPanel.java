package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import controladores.ControladorEscrevente;
import controladores.ControladorTabeliao;
import entidades.Escrevente;
import entidades.Tabeliao;

public class PesquisarFuncionarioPanel extends JPanel {

	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JRadioButton radioEscrevente;
	private JRadioButton radioTabeliao;
	private JRadioButton radioPorNome;
	private JRadioButton radioPorMatricula;
	private JTextField textField_nome;
	private JFormattedTextField formattedTextField_matricula;
	private DefaultListModel<String> listModel;
	private JList<String> list_Nomes;
	private List<Object> listaEscrevente;
	private List<Object> listaTabeliao;
	private int listIndex;
	
	private JLabel labelPesquisarFuncionario;
	
	private JButton buttonPesquisar;
	
	private EditarEscreventeDialog editarEscreventeDialog;
	private EditarTabeliaoDialog editarTabeliaoDialog;
	private AlertDialog alerta;
	
	ControladorEscrevente controladorEscrevente = new ControladorEscrevente();
	ControladorTabeliao controladorTabeliao = new ControladorTabeliao();
	Object tabeliaoSelecionado = null;
	Object escreventeSelecionado = null;
	
	public JPanel getContentPanel() {
		return this.contentPanel;
	}
	
	/**
	 * Create the panel.
	 */
	public PesquisarFuncionarioPanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelPesquisarFuncionario = new JLabel("Pesquisar Funcion�rio");
		labelPesquisarFuncionario.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelPesquisarFuncionario.setBounds(10, 33, 239, 31);
		add(labelPesquisarFuncionario);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		radioEscrevente = new JRadioButton("Escrevente");
		radioEscrevente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioEscrevente.setSelected(true);
				radioTabeliao.setSelected(false);
				
				buttonPesquisar.setEnabled(true);

				if(!radioPorNome.isEnabled() && !radioPorMatricula.isEnabled()) {
					radioPorNome.setEnabled(true);
					radioPorMatricula.setEnabled(true);
					textField_nome.setEnabled(true);
				}				
			}
		});
		radioEscrevente.setBounds(10, 97, 109, 23);
		add(radioEscrevente);
		
		radioTabeliao = new JRadioButton("Tabeli�o");
		radioTabeliao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioEscrevente.setSelected(false);
				radioTabeliao.setSelected(true);
				
				buttonPesquisar.setEnabled(true);
				
				if(!radioPorNome.isEnabled() && !radioPorMatricula.isEnabled()) {
					radioPorNome.setEnabled(true);
					radioPorMatricula.setEnabled(true);
					textField_nome.setEnabled(true);
				}
				
			}
		});
		radioTabeliao.setBounds(121, 97, 142, 23);
		add(radioTabeliao);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 137, 765, 15);
		add(separator_1);
		
		radioPorNome = new JRadioButton("Por Nome");
		radioPorNome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioPorNome.setSelected(true);
				radioPorMatricula.setSelected(false);
				
				textField_nome.setEnabled(true);
				formattedTextField_matricula.setEnabled(false);
			}
		});
		radioPorNome.setSelected(true);
		radioPorNome.setEnabled(false);
		radioPorNome.setBounds(10, 163, 149, 23);
		add(radioPorNome);
		
		textField_nome = new JTextField();
		textField_nome.setEnabled(false);
		textField_nome.setBounds(165, 163, 385, 22);
		add(textField_nome);
		textField_nome.setColumns(10);
		
		radioPorMatricula = new JRadioButton("Por Matr�cula");
		radioPorMatricula.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioPorNome.setSelected(false);
				radioPorMatricula.setSelected(true);
				
				textField_nome.setEnabled(false);
				formattedTextField_matricula.setEnabled(true);
			}
		});
		radioPorMatricula.setEnabled(false);
		radioPorMatricula.setBounds(10, 207, 149, 23);
		add(radioPorMatricula);
		
		formattedTextField_matricula = new JFormattedTextField();
		formattedTextField_matricula.setEnabled(false);
		formattedTextField_matricula.setBounds(165, 207, 230, 22);
		add(formattedTextField_matricula);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(10, 252, 765, 15);
		add(separator_2);
		
		listModel = new DefaultListModel<String>();
		list_Nomes = new JList<String>(listModel);
		list_Nomes.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent evt){
				if(radioEscrevente.isSelected()) {
					JList<?> list = (JList<?>) evt.getSource();
					if(evt.getClickCount() == 2){
						listIndex = list.locationToIndex(evt.getPoint());
						if (listIndex > -1){
							escreventeSelecionado = listaEscrevente.get(listIndex);
							try {
								editarEscreventeDialog = new EditarEscreventeDialog(escreventeSelecionado);
								editarEscreventeDialog.setVisible(true);
							} catch (SQLException e) {
								alerta = new AlertDialog("Ocorreu um erro!");
								alerta.setVisible(true);
								e.printStackTrace();
							}	
						}
					}
				} else {
					JList<?> list = (JList<?>) evt.getSource();
					if(evt.getClickCount() == 2){
						listIndex = list.locationToIndex(evt.getPoint());
						if (listIndex > -1){
							tabeliaoSelecionado = listaTabeliao.get(listIndex);
							try {
								editarTabeliaoDialog = new EditarTabeliaoDialog(tabeliaoSelecionado);
								editarTabeliaoDialog.setVisible(true);
							} catch (SQLException e) {
								alerta = new AlertDialog("Ocorreu um erro!");
								alerta.setVisible(true);
								e.printStackTrace();
							}
						}
					}
				}
			}
		});
		list_Nomes.setBounds(10, 273, 540, 267);
		add(list_Nomes);
		
		buttonPesquisar = new JButton("Pesquisar");
		buttonPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(radioEscrevente.isSelected()) {
					boolean radioMatricula = radioPorMatricula.isSelected();
					if(radioMatricula){
						int matricula = Integer.parseInt(formattedTextField_matricula.getText());
						try {
							escreventeSelecionado = controladorEscrevente.buscar(matricula);
							
							if(escreventeSelecionado != null){
								editarEscreventeDialog = new EditarEscreventeDialog(escreventeSelecionado);
								editarEscreventeDialog.setVisible(true);
							}else{
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}
						} catch (SQLException ex) {
							alerta = new AlertDialog("Ocorreu um erro!");
							alerta.setVisible(true);
							ex.printStackTrace();
						}
					}else{
						listModel.clear();
						String nome = textField_nome.getText();
						try {
							listaEscrevente = controladorEscrevente.buscarPorNome(nome);
							
							if(listaEscrevente.size() == 0){
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}else{
								for(int i = 0; i < listaEscrevente.size(); i++){
									listModel.addElement(((Escrevente)listaEscrevente.get(i)).getNome());
								}
							}
						} catch (SQLException e) {
							alerta = new AlertDialog("Ocorreu um erro!");
							alerta.setVisible(true);
							e.printStackTrace();
						}
					}
					
				}else{
					boolean radioMatricula = radioPorMatricula.isSelected();
					if(radioMatricula){
						int matricula = Integer.parseInt(formattedTextField_matricula.getText());
						try {
							tabeliaoSelecionado = controladorTabeliao.buscar(matricula);
							if(tabeliaoSelecionado != null){
								editarTabeliaoDialog = new EditarTabeliaoDialog(tabeliaoSelecionado);
								editarTabeliaoDialog.setVisible(true);
							}else{
								alerta.setLocationRelativeTo(getContentPanel());
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}
						} catch (SQLException ex) {
							alerta = new AlertDialog("Ocorreu um erro!");
							alerta.setVisible(true);
							ex.printStackTrace();
						}
					}else{
						String nome = textField_nome.getText();
						listModel.clear();
						try {
							listaTabeliao = controladorTabeliao.buscarPorNome(nome);
							
							if(listaTabeliao.size() == 0){
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}
							else{
								for(int i = 0; i < listaTabeliao.size(); i++){
									listModel.addElement(((Tabeliao)listaTabeliao.get(i)).getNome());
								}
							}
						} catch (SQLException e) {
							alerta = new AlertDialog("Ocorreu um erro!");
							alerta.setVisible(true);
							e.printStackTrace();
						}
					}
				}
			}
		});
		buttonPesquisar.setEnabled(false);
		buttonPesquisar.setBounds(671, 207, 104, 23);
		add(buttonPesquisar);
	}
}
