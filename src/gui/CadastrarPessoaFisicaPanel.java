package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.text.MaskFormatter;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import controladores.ControladorPessoaFisica;
import entidades.Telefone;

public class CadastrarPessoaFisicaPanel extends JPanel {
	
	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JTextField textField_Nome;
	private JDateChooser chooser_DataNascimento;
	private JFormattedTextField formattedTextField_Cpf;
	private JComboBox<String> comboBox_Sexo;
	private JFormattedTextField formattedTextField_Renda;
	private JTextField textField_Profissao;
	private JTextField textField_Logradouro;
	private JTextField textField_Numero;
	private JTextField textField_Bairro;
	private JTextField textField_Cidade;
	private JComboBox<String> comboBox_Uf;
	private JFormattedTextField formattedTextField_Cep;
	private JFormattedTextField formattedTextField_Residencial;
	private JFormattedTextField formattedTextField_Celular;
	private JFormattedTextField formattedTextField_Comercial;
	
	private AlertDialog alerta;
	
	private JLabel labelCadastrarPessoaFisica;
	private JLabel labelNome;
	private JLabel labelData;
	private JLabel labelCpf;
	private JLabel labelSexo;
	private JLabel labelRenda;
	private JLabel labelProfissao;
	private JLabel labelEndereco;
	private JLabel labelLogradouro;
	private JLabel labelNumero;
	private JLabel labelBairro;
	private JLabel labelCidade;
	private JLabel labelUf;
	private JLabel labelCep;
	private JLabel labelTelefones;
	private JLabel labelResidencial;
	private JLabel labelCelular;
	private JLabel labelComercial;
	
	private JButton buttonCadastrar;
	private JButton buttonCancelar;

	public CardLayout getCardLayout() {
		return this.cardLayout;
	}
	
	public JPanel getContentPane() {
		return this.contentPanel;
	}
	
	/**
	 * Create the panel.
	 */
	public CadastrarPessoaFisicaPanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelCadastrarPessoaFisica = new JLabel("Cadastrar Pessoa F\u00EDsica");
		labelCadastrarPessoaFisica.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelCadastrarPessoaFisica.setBounds(10, 33, 239, 31);
		add(labelCadastrarPessoaFisica);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		labelNome = new JLabel("Nome");
		labelNome.setBounds(10, 94, 143, 14);
		add(labelNome);
		
		textField_Nome = new JTextField();
		textField_Nome.setBounds(10, 114, 601, 20);
		add(textField_Nome);
		textField_Nome.setColumns(10);
		
		labelData = new JLabel("Data de Nascimento");
		labelData.setBounds(621, 95, 154, 14);
		add(labelData);
		
		chooser_DataNascimento = new JDateChooser();
		chooser_DataNascimento.setBounds(621, 114, 154, 20);
		chooser_DataNascimento.setMaxSelectableDate(new Date());
		JTextFieldDateEditor chooser_DataNascimentoEditor = (JTextFieldDateEditor) chooser_DataNascimento.getDateEditor();
		chooser_DataNascimentoEditor.setEditable(false);
		add(chooser_DataNascimento);
		
		labelCpf = new JLabel("CPF");
		labelCpf.setBounds(10, 148, 108, 14);
		add(labelCpf);
		
		formattedTextField_Cpf = new JFormattedTextField();
		formattedTextField_Cpf.setBounds(10, 166, 224, 20);
		add(formattedTextField_Cpf);
		try {
			MaskFormatter formatter = new MaskFormatter("###.###.###-##");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Cpf);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		labelEndereco = new JLabel("Endere\u00E7o");
		labelEndereco.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelEndereco.setBounds(10, 225, 186, 23);
		add(labelEndereco);
		
		labelLogradouro = new JLabel("Logradouro");
		labelLogradouro.setBounds(10, 256, 89, 14);
		add(labelLogradouro);
		
		textField_Logradouro = new JTextField();
		textField_Logradouro.setBounds(10, 276, 448, 20);
		add(textField_Logradouro);
		textField_Logradouro.setColumns(10);
		
		labelNumero = new JLabel("N\u00FAmero");
		labelNumero.setBounds(468, 256, 86, 14);
		add(labelNumero);
		
		textField_Numero = new JTextField();
		textField_Numero.setBounds(468, 276, 86, 20);
		add(textField_Numero);
		textField_Numero.setColumns(10);
		
		labelBairro = new JLabel("Bairro");
		labelBairro.setBounds(564, 256, 211, 14);
		add(labelBairro);
		
		textField_Bairro = new JTextField();
		textField_Bairro.setBounds(564, 276, 211, 20);
		add(textField_Bairro);
		textField_Bairro.setColumns(10);
		
		labelCidade = new JLabel("Cidade");
		labelCidade.setBounds(10, 307, 104, 14);
		add(labelCidade);
		
		textField_Cidade = new JTextField();
		textField_Cidade.setBounds(10, 326, 448, 20);
		add(textField_Cidade);
		textField_Cidade.setColumns(10);
		
		labelUf = new JLabel("UF");
		labelUf.setBounds(468, 307, 56, 14);
		add(labelUf);
		
		comboBox_Uf = new JComboBox<String>();
		comboBox_Uf.setBounds(468, 326, 89, 20);
		String[] UFs = {"", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", 
				"GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", 
				"RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
		for(int i = 0; i < UFs.length; i++) {
			comboBox_Uf.addItem(UFs[i]);
		}
		add(comboBox_Uf);
		
		labelCep = new JLabel("CEP");
		labelCep.setBounds(564, 307, 67, 14);
		add(labelCep);
		
		formattedTextField_Cep = new JFormattedTextField();
		formattedTextField_Cep.setBounds(564, 326, 211, 20);
		add(formattedTextField_Cep);
		formattedTextField_Cep.setColumns(10);
		try {
			MaskFormatter formatter = new MaskFormatter("##.###-###");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Cep);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		labelTelefones = new JLabel("Telefones");
		labelTelefones.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelTelefones.setBounds(10, 376, 186, 23);
		add(labelTelefones);
		
		labelResidencial = new JLabel("Residencial");
		labelResidencial.setBounds(10, 410, 86, 14);
		add(labelResidencial);
		
		formattedTextField_Residencial = new JFormattedTextField();
		formattedTextField_Residencial.setBounds(10, 430, 143, 20);
		add(formattedTextField_Residencial);
		
		labelCelular = new JLabel("Celular");
		labelCelular.setBounds(163, 410, 86, 14);
		add(labelCelular);
		
		formattedTextField_Celular = new JFormattedTextField();
		formattedTextField_Celular.setBounds(316, 430, 143, 20);
		add(formattedTextField_Celular);
		
		labelComercial = new JLabel("Comercial");
		labelComercial.setBounds(316, 410, 86, 14);
		add(labelComercial);
		
		formattedTextField_Comercial = new JFormattedTextField();
		formattedTextField_Comercial.setBounds(163, 430, 143, 20);
		add(formattedTextField_Comercial);
		
		try {
			MaskFormatter formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Residencial);
			formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Celular);
			formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Comercial);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		labelSexo = new JLabel("Sexo");
		labelSexo.setBounds(244, 148, 46, 14);
		add(labelSexo);
		
		comboBox_Sexo = new JComboBox<String>();
		comboBox_Sexo.setBounds(244, 166, 50, 20);
		comboBox_Sexo.addItem("");
		comboBox_Sexo.addItem("F");
		comboBox_Sexo.addItem("M");
		add(comboBox_Sexo);
		
		labelRenda = new JLabel("Renda (R$)");
		labelRenda.setBounds(304, 148, 67, 14);
		add(labelRenda);
		
		formattedTextField_Renda = new JFormattedTextField();
		formattedTextField_Renda.setBounds(304, 166, 141, 20);
		add(formattedTextField_Renda);
		
		try {
			MaskFormatter formatter = new MaskFormatter("#######");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Renda);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		labelProfissao = new JLabel("Profiss\u00E3o");
		labelProfissao.setBounds(455, 148, 149, 14);
		add(labelProfissao);
		
		textField_Profissao = new JTextField();
		textField_Profissao.setColumns(10);
		textField_Profissao.setBounds(455, 166, 321, 20);
		add(textField_Profissao);
		
		/** BOTOES */
		
		buttonCadastrar = new JButton("Cadastrar");
		buttonCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nome = textField_Nome.getText();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String dataNascimento = dateFormat.format(chooser_DataNascimento.getDate()).replaceAll("-", "/");
				String cpf = formattedTextField_Cpf.getText().replaceAll("-", "").replaceAll("\\.", "");
				String sexoS = (String) comboBox_Sexo.getSelectedItem();
				char sexo = sexoS.charAt(0);
				double renda =  Double.parseDouble(formattedTextField_Renda.getText());
				String profissao = textField_Profissao.getText();
				String logradouro = textField_Logradouro.getText();
				String numero = textField_Numero.getText();
				String bairro = textField_Bairro.getText();
				String cidade = textField_Cidade.getText();
				String uf = (String) comboBox_Uf.getSelectedItem();
				String cep = formattedTextField_Cep.getText().replaceAll("-", "").replaceAll("\\.", "");
				
				String telResidencial = formattedTextField_Residencial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");
				String telComercial = formattedTextField_Comercial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");
				String telCelular = formattedTextField_Celular.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");

				List<Telefone> telefonesList = new ArrayList<Telefone>();
				
				if(telResidencial.length() != 0){
					String dddResidencial = telResidencial.substring(0, 2);
					String numeroResidencial = telResidencial.substring(2, telResidencial.length());
					Telefone telefoneResidencial = new Telefone(dddResidencial, numeroResidencial);
					telefonesList.add(telefoneResidencial);
				}

				if(telCelular.length() != 0){
					String dddCelular = telCelular.substring(0, 2);
					String numeroCelular = telCelular.substring(2, telCelular.length());
					Telefone telefoneCelular = new Telefone(dddCelular, numeroCelular);
					telefonesList.add(telefoneCelular);
				}
				
				if(telComercial.length() != 0){
					String dddComercial = telComercial.substring(0, 2);
					String numeroComercial = telComercial.substring(2, telComercial.length());
					Telefone telefoneComercial = new Telefone(dddComercial, numeroComercial);
					telefonesList.add(telefoneComercial);
				}
				
				Telefone [] telefones = new Telefone[telefonesList.size()];
				telefonesList.toArray(telefones);
				
				try {
					ControladorPessoaFisica controlador = new ControladorPessoaFisica();
					controlador.cadastrar(nome, dataNascimento, cpf, sexo, renda, profissao, logradouro, numero, bairro, cidade, uf, cep, telefones);
					alerta = new AlertDialog("Cliente cadastrado com sucesso!");
					alerta.setVisible(true);
					resetCampos();
				} catch (SQLException e) {
					alerta = new AlertDialog("Erro ao cadastrar cliente!");
					alerta.setVisible(true);
					e.printStackTrace();
				}
				
			}
		});
		buttonCadastrar.setBounds(671, 526, 104, 23);
		add(buttonCadastrar);
		
		buttonCancelar = new JButton("Cancelar");
		buttonCancelar.setBounds(572, 526, 89, 23);
		add(buttonCancelar);
	}
	
	public void resetCampos(){
		textField_Nome.setText("");
		formattedTextField_Cpf.setText("");
		textField_Logradouro.setText("");
		textField_Numero.setText("");
		textField_Bairro.setText("");
		textField_Cidade.setText("");
		comboBox_Uf.setSelectedIndex(0);
		comboBox_Sexo.setSelectedIndex(0);
		formattedTextField_Cep.setText("");
		formattedTextField_Renda.setValue(0);
		chooser_DataNascimento.setCalendar(null);
		formattedTextField_Residencial.setText("");
		formattedTextField_Residencial.setText("");
		formattedTextField_Comercial.setText("");
		textField_Profissao.setText("");

	}
}
