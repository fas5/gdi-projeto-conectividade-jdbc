package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.CardLayout;
import java.awt.Font;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.text.MaskFormatter;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import controladores.ControladorPessoaJuridica;
import entidades.Endereco;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastrarPessoaJuridicaPanel extends JPanel {
	
	private AlertDialog alert;
	private ControladorPessoaJuridica controladorPJ;
	
	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JTextField textField_Nome;
	private JDateChooser chooser_DataFundacao;
	private JFormattedTextField formattedTextField_Cnpj;
	private JTextField textField_AreaAtuacao;
	private JTextField textField_Logradouro;
	private JTextField textField_Numero;
	private JTextField textField_Bairro;
	private JTextField textField_Cidade;
	private JComboBox<String> comboBox_Uf;
	private JFormattedTextField textField_Cep;
	private JFormattedTextField formattedTextField_Residencial;
	private JFormattedTextField formattedTextField_Celular;
	private JFormattedTextField formattedTextField_Comercial;	
	
	private JLabel labelCadastrarPessoaJuridica;
	private JLabel labelNome;
	private JLabel labelDataFundacao;
	private JLabel labelCnpj;
	private JLabel labelAreaAtuao;
	private JLabel labelEndereco;
	private JLabel labelLogradouro;
	private JLabel labelNumero;
	private JLabel labelBairro;
	private JLabel labelCidade;
	private JLabel labelUf;
	private JLabel labelCep;
	private JLabel labelTelefones;
	private JLabel labelResidencial;
	private JLabel labelCelular;
	private JLabel labelComercial;
	
	private JButton buttonCadastrar;
	private JButton buttonCancelar;

	public CardLayout getCardLayout() {
		return this.cardLayout;
	}
	
	public JPanel getContentPane() {
		return this.contentPanel;
	}
	
	/**
	 * Create the panel.
	 */
	public CadastrarPessoaJuridicaPanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelCadastrarPessoaJuridica = new JLabel("Cadastrar Pessoa Jur\u00EDdica");
		labelCadastrarPessoaJuridica.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelCadastrarPessoaJuridica.setBounds(10, 33, 239, 31);
		add(labelCadastrarPessoaJuridica);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		labelNome = new JLabel("Raz\u00E3o Social");
		labelNome.setBounds(10, 94, 143, 14);
		add(labelNome);
		
		textField_Nome = new JTextField();
		textField_Nome.setBounds(10, 114, 601, 20);
		add(textField_Nome);
		textField_Nome.setColumns(10);
		
		labelDataFundacao = new JLabel("Data de Funda\u00E7\u00E3o");
		labelDataFundacao.setBounds(621, 95, 154, 14);
		add(labelDataFundacao);
		
		chooser_DataFundacao = new JDateChooser();
		chooser_DataFundacao.setBounds(621, 114, 154, 20);
		chooser_DataFundacao.setMaxSelectableDate(new Date());
		JTextFieldDateEditor chooser_DataFundacaoEditor = (JTextFieldDateEditor) chooser_DataFundacao.getDateEditor();
		chooser_DataFundacaoEditor.setEditable(false);
		add(chooser_DataFundacao);
		
		labelCnpj = new JLabel("CNPJ");
		labelCnpj.setBounds(10, 148, 108, 14);
		add(labelCnpj);
		try {
			MaskFormatter formatter = new MaskFormatter("##.###.###/####-##");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Cnpj);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		formattedTextField_Cnpj = new JFormattedTextField();
		formattedTextField_Cnpj.setBounds(10, 166, 224, 20);
		add(formattedTextField_Cnpj);
		
		labelEndereco = new JLabel("Endere\u00E7o");
		labelEndereco.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelEndereco.setBounds(10, 225, 186, 23);
		add(labelEndereco);
		
		labelLogradouro = new JLabel("Logradouro");
		labelLogradouro.setBounds(10, 256, 89, 14);
		add(labelLogradouro);
		
		textField_Logradouro = new JTextField();
		textField_Logradouro.setBounds(10, 276, 448, 20);
		add(textField_Logradouro);
		textField_Logradouro.setColumns(10);
		
		labelNumero = new JLabel("N\u00FAmero");
		labelNumero.setBounds(468, 256, 86, 14);
		add(labelNumero);
		
		textField_Bairro = new JTextField();
		textField_Bairro.setBounds(564, 276, 211, 20);
		add(textField_Bairro);
		textField_Bairro.setColumns(10);
		
		labelBairro = new JLabel("Bairro");
		labelBairro.setBounds(564, 256, 211, 14);
		add(labelBairro);
		
		textField_Numero = new JTextField();
		textField_Numero.setBounds(468, 276, 86, 20);
		add(textField_Numero);
		textField_Numero.setColumns(10);
		
		labelCidade = new JLabel("Cidade");
		labelCidade.setBounds(10, 307, 104, 14);
		add(labelCidade);
		
		textField_Cidade = new JTextField();
		textField_Cidade.setBounds(10, 326, 448, 20);
		add(textField_Cidade);
		textField_Cidade.setColumns(10);
		
		labelUf = new JLabel("UF");
		labelUf.setBounds(468, 307, 56, 14);
		add(labelUf);
		
		comboBox_Uf = new JComboBox<String>();
		comboBox_Uf.setBounds(468, 326, 89, 20);
		
		String[] UFs = {"", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", 
				"GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", 
				"RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
		
		for(int i = 0; i < UFs.length; i++) {
			comboBox_Uf.addItem(UFs[i]);
		}	
		
		add(comboBox_Uf);
		
		labelCep = new JLabel("CEP");
		labelCep.setBounds(564, 307, 67, 14);
		add(labelCep);
		
		textField_Cep = new JFormattedTextField();
		textField_Cep.setBounds(564, 326, 211, 20);
		add(textField_Cep);
		textField_Cep.setColumns(10);
		try {
			MaskFormatter formatter = new MaskFormatter("##.###-###");
			formatter.setValidCharacters("0123456789");
			formatter.install(textField_Cep);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		labelTelefones = new JLabel("Telefones");
		labelTelefones.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelTelefones.setBounds(10, 376, 186, 23);
		add(labelTelefones);
		
		labelResidencial = new JLabel("Residencial");
		labelResidencial.setBounds(10, 410, 86, 14);
		add(labelResidencial);
		
		formattedTextField_Residencial = new JFormattedTextField();
		formattedTextField_Residencial.setBounds(10, 430, 143, 20);
		add(formattedTextField_Residencial);
		
		labelCelular = new JLabel("Celular");
		labelCelular.setBounds(163, 410, 86, 14);
		add(labelCelular);
		
		formattedTextField_Celular = new JFormattedTextField();
		formattedTextField_Celular.setBounds(316, 430, 143, 20);
		add(formattedTextField_Celular);
		
		labelComercial = new JLabel("Comercial");
		labelComercial.setBounds(316, 410, 86, 14);
		add(labelComercial);
		
		formattedTextField_Comercial = new JFormattedTextField();
		formattedTextField_Comercial.setBounds(163, 430, 143, 20);
		add(formattedTextField_Comercial);
		
		try {
			MaskFormatter formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Residencial);
			formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Celular);
			formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Comercial);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		labelAreaAtuao = new JLabel("\u00C1rea de Atua\u00E7\u00E3o");
		labelAreaAtuao.setBounds(244, 148, 149, 14);
		add(labelAreaAtuao);
		
		textField_AreaAtuacao = new JTextField();
		textField_AreaAtuacao.setColumns(10);
		textField_AreaAtuacao.setBounds(244, 166, 531, 20);
		add(textField_AreaAtuacao);
		
		/** BOTOES */
		
		buttonCadastrar = new JButton("Cadastrar");
		buttonCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					Endereco endereco = new Endereco(textField_Logradouro.getText(), textField_Numero.getText(), 
							textField_Bairro.getText(), textField_Cidade.getText(), (String)comboBox_Uf.getSelectedItem(), 
							textField_Cep.getText().replaceAll("\\.", "").replaceAll("-", ""));
					
					JTextFieldDateEditor chooser_DataFundacaoEditor = (JTextFieldDateEditor) chooser_DataFundacao.getDateEditor();
				try {
					controladorPJ = new ControladorPessoaJuridica();					
					controladorPJ.cadastrar(
							textField_Nome.getText(),
							chooser_DataFundacaoEditor.getText(),
							formattedTextField_Cnpj.getText().replaceAll("-", "").replaceAll("\\.", "").replaceAll("/", ""),
							textField_AreaAtuacao.getText(), 
							endereco,
							formattedTextField_Residencial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", ""),
							formattedTextField_Celular.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", ""),
							formattedTextField_Comercial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", ""));
					
					alert = new AlertDialog("Cliente cadastrado com sucesso!");
					alert.abrir();
				} catch (SQLException e) {
					alert = new AlertDialog("Erro ao cadastrar cliente!");
					alert.abrir();
					e.printStackTrace();
				}
			}
		});
		buttonCadastrar.setBounds(671, 526, 104, 23);
		add(buttonCadastrar);
		
		buttonCancelar = new JButton("Cancelar");
		buttonCancelar.setBounds(572, 526, 89, 23);
		add(buttonCancelar);
	}
	
	public void resetCampos(){
		textField_Nome.setText("");
		formattedTextField_Cnpj.setText("");
		textField_Logradouro.setText("");
		textField_Numero.setText("");
		textField_Bairro.setText("");
		textField_Cidade.setText("");
		comboBox_Uf.setSelectedIndex(0);
		textField_Cep.setText("");
		chooser_DataFundacao.setCalendar(null);
		formattedTextField_Residencial.setText("");
		formattedTextField_Residencial.setText("");
		formattedTextField_Comercial.setText("");
		textField_AreaAtuacao.setText("");
	}
}
