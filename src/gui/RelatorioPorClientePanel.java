package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import controladores.ControladorRelatorios;
import entidades.RelatorioAtendimento;

public class RelatorioPorClientePanel extends JPanel {
	
	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JRadioButton radioPessoaFisica;
	private JRadioButton radioPessoaJuridica;
	private JRadioButton radioPorCpf;
	private JTextField textField_Cpf;
	
	private JLabel labelRelatorioPorCliente;
	
	private JButton buttonGerar;
	private JTable table;
	private DefaultTableModel dtm;
	
	private AlertDialog alerta;
	
	/**
	 * Create the panel.
	 */
	public RelatorioPorClientePanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelRelatorioPorCliente = new JLabel("Relat�rio de Atendimentos (Por Cliente)");
		labelRelatorioPorCliente.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelRelatorioPorCliente.setBounds(10, 33, 765, 31);
		add(labelRelatorioPorCliente);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		radioPessoaFisica = new JRadioButton("Pessoa F\u00EDsica");
		radioPessoaFisica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radioPessoaFisica.isSelected()) {
					radioPessoaFisica.setSelected(true);
					radioPessoaJuridica.setSelected(false);
					
					radioPorCpf.setText("Por CPF");
					
					buttonGerar.setEnabled(true);

					if(!radioPorCpf.isEnabled()) {
						radioPorCpf.setEnabled(true);
						textField_Cpf.setEnabled(true);
					}
				}
			}
		});
		radioPessoaFisica.setBounds(10, 97, 109, 23);
		add(radioPessoaFisica);
		
		radioPessoaJuridica = new JRadioButton("Pessoa Jur\u00EDdica");
		radioPessoaJuridica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radioPessoaJuridica.isSelected()) {
					radioPessoaFisica.setSelected(false);
					radioPessoaJuridica.setSelected(true);
					
					radioPorCpf.setText("Por CNPJ");
					
					buttonGerar.setEnabled(true);
					
					if(!radioPorCpf.isEnabled()) {
						radioPorCpf.setEnabled(true);
						textField_Cpf.setEnabled(true);
					}
				}
			}
		});
		radioPessoaJuridica.setBounds(121, 97, 142, 23);
		add(radioPessoaJuridica);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 137, 765, 15);
		add(separator_1);
		
		radioPorCpf = new JRadioButton("Por CPF");
		radioPorCpf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioPorCpf.setSelected(true);
				
				textField_Cpf.setEnabled(true);
			}
		});
		radioPorCpf.setSelected(true);
		radioPorCpf.setEnabled(false);
		radioPorCpf.setBounds(10, 163, 149, 23);
		add(radioPorCpf);
		
		textField_Cpf = new JTextField();
		textField_Cpf.setEnabled(false);
		textField_Cpf.setBounds(165, 163, 385, 22);
		add(textField_Cpf);
		textField_Cpf.setColumns(10);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(10, 209, 765, 15);
		add(separator_2);
		
		table = new JTable();
		dtm = new DefaultTableModel(0, 0);
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("Pessoa Nome");
		columnNames.add("Escrevente Nome");
		columnNames.add("Escrevente Matricula");
		columnNames.add("Servi�o Descricao");
		columnNames.add("Servi�o Preco");
		columnNames.add("Servi�o Codigo");
		columnNames.add("Atendimento Quantidade");
		columnNames.add("Atendimento Desconto");
		columnNames.add("Presta Data e Hora");
		dtm.setColumnIdentifiers(columnNames);
		table.setModel(dtm);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(10, 235, 765, 314);
		table.setBounds(10, 250, 1000, 305);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		scrollPane.setViewportView(table);
		add(scrollPane);
		
		buttonGerar = new JButton("Gerar");
		buttonGerar.setEnabled(false);
		buttonGerar.setBounds(671, 163, 104, 23);
		add(buttonGerar);
		buttonGerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField_Cpf.getText().isEmpty()){
//					Validacao
				}else{
					String cpf = textField_Cpf.getText();
					ControladorRelatorios cr = new ControladorRelatorios();
					if(radioPessoaFisica.isSelected()){
						try {
							for(int i = dtm.getRowCount() - 1; i >=0; i--)
							{
								dtm.removeRow(i); 
							}
							List<RelatorioAtendimento> resultado = cr.buscarPorPessoaFisica(cpf);
							
							if(resultado.size() == 0){
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}else{
								for (int i = 0; i < resultado.size(); i++) {
							        dtm.addRow(new Object[] { resultado.get(i).getPfNome(), resultado.get(i).geteNome(),
							        		resultado.get(i).geteMatricula(), resultado.get(i).getsDescricao(),
							        		resultado.get(i).getsPreco(), resultado.get(i).getsCodigo(),
							        		resultado.get(i).getaQuantidade(), resultado.get(i).getaDesconto(),
							        		resultado.get(i).getPrDataHora()});
								}
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}else{
						try {
							for(int i = dtm.getRowCount() - 1; i >=0; i--)
							{
								dtm.removeRow(i); 
							}
							List<RelatorioAtendimento> resultado = cr.buscarPorPessoaJuridica(cpf);
							if(resultado.size() == 0){
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}else{
								for (int i = 0; i < resultado.size(); i++) {
							        dtm.addRow(new Object[] { resultado.get(i).getPfNome(), resultado.get(i).geteNome(),
							        		resultado.get(i).geteMatricula(), resultado.get(i).getsDescricao(),
							        		resultado.get(i).getsPreco(), resultado.get(i).getsCodigo(),
							        		resultado.get(i).getaQuantidade(), resultado.get(i).getaDesconto(),
							        		resultado.get(i).getPrDataHora()});
								}	
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
	}

}
