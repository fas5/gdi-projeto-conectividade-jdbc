package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import controladores.ControladorRelatorios;
import entidades.RelatorioAtendimento;

public class RelatorioPorFuncionarioPanel extends JPanel {

	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JRadioButton radioEscrevente;
	private JRadioButton radioTabelião;
	private JRadioButton radioPorMatricula;
	private JTextField textField_matricula;
	
	private JLabel labelRelatorioPorFuncionario;
	
	private JButton buttonGerar;
	private JTable table;
	private DefaultTableModel dtm;
	
	private AlertDialog alerta;
	
	/**
	 * Create the panel.
	 */
	public RelatorioPorFuncionarioPanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelRelatorioPorFuncionario = new JLabel("Relatório de Atendimentos (Por Funcionário)");
		labelRelatorioPorFuncionario.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelRelatorioPorFuncionario.setBounds(10, 33, 765, 31);
		add(labelRelatorioPorFuncionario);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		radioEscrevente = new JRadioButton("Escrevente");
		radioEscrevente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radioEscrevente.isSelected()) {
					radioEscrevente.setSelected(true);
					radioTabelião.setSelected(false);
					
					buttonGerar.setEnabled(true);

					if(!radioPorMatricula.isEnabled()) {
						radioPorMatricula.setEnabled(true);
						textField_matricula.setEnabled(true);
					}
				}
			}
		});
		radioEscrevente.setBounds(10, 97, 109, 23);
		add(radioEscrevente);
		
		radioTabelião = new JRadioButton("Tabelião");
		radioTabelião.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radioTabelião.isSelected()) {
					radioEscrevente.setSelected(false);
					radioTabelião.setSelected(true);
					
					buttonGerar.setEnabled(true);
					
					if(!radioPorMatricula.isEnabled()) {
						radioPorMatricula.setEnabled(true);
						textField_matricula.setEnabled(true);
					}
				}
			}
		});
		radioTabelião.setBounds(121, 97, 142, 23);
		add(radioTabelião);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 137, 765, 15);
		add(separator_1);
		
		radioPorMatricula = new JRadioButton("Por Matricula");
		radioPorMatricula.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioPorMatricula.setSelected(true);
				
				textField_matricula.setEnabled(true);
			}
		});
		radioPorMatricula.setSelected(true);
		radioPorMatricula.setEnabled(false);
		radioPorMatricula.setBounds(10, 163, 149, 23);
		add(radioPorMatricula);
		
		textField_matricula = new JTextField();
		textField_matricula.setEnabled(false);
		textField_matricula.setBounds(165, 163, 385, 22);
		add(textField_matricula);
		textField_matricula.setColumns(10);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(10, 209, 765, 15);
		add(separator_2);
		
		table = new JTable();
		dtm = new DefaultTableModel(0, 0);
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("Pessoa Nome");
		columnNames.add("Escrevente Nome");
		columnNames.add("Escrevente Matricula");
		columnNames.add("Serviço Descricao");
		columnNames.add("Serviço Preco");
		columnNames.add("Serviço Codigo");
		columnNames.add("Atendimento Quantidade");
		columnNames.add("Atendimento Desconto");
		columnNames.add("Presta Data e Hora");
		dtm.setColumnIdentifiers(columnNames);
		table.setModel(dtm);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(10, 235, 765, 314);
		table.setBounds(10, 250, 1000, 305);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		scrollPane.setViewportView(table);
		add(scrollPane);
		
		buttonGerar = new JButton("Gerar");
		buttonGerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField_matricula.getText().isEmpty()){
//					Validacao
				}else{
					int matricula = Integer.parseInt(textField_matricula.getText());
					ControladorRelatorios cr = new ControladorRelatorios();
					if(radioTabelião.isSelected()){
						try {
							for(int i = dtm.getRowCount() - 1; i >=0; i--)
							{
								dtm.removeRow(i); 
							}
							List<RelatorioAtendimento> resultado = cr.buscarPorTabeliao(matricula);
							if(resultado.size() == 0){
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}else{
								for (int i = 0; i < resultado.size(); i++) {
							        dtm.addRow(new Object[] { resultado.get(i).getPfNome(), resultado.get(i).geteNome(),
							        		resultado.get(i).geteMatricula(), resultado.get(i).getsDescricao(),
							        		resultado.get(i).getsPreco(), resultado.get(i).getsCodigo(),
							        		resultado.get(i).getaQuantidade(), resultado.get(i).getaDesconto(),
							        		resultado.get(i).getPrDataHora()});
								}
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}else{
						try {
							for(int i = dtm.getRowCount() - 1; i >=0; i--)
							{
								dtm.removeRow(i); 
							}
							List<RelatorioAtendimento> resultado = cr.buscarPorEscrevente(matricula);
							if(resultado.size() == 0){
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}else{
								for (int i = 0; i < resultado.size(); i++) {
							        dtm.addRow(new Object[] { resultado.get(i).getPfNome(), resultado.get(i).geteNome(),
							        		resultado.get(i).geteMatricula(), resultado.get(i).getsDescricao(),
							        		resultado.get(i).getsPreco(), resultado.get(i).getsCodigo(),
							        		resultado.get(i).getaQuantidade(), resultado.get(i).getaDesconto(),
							        		resultado.get(i).getPrDataHora()});
								}
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
		buttonGerar.setEnabled(false);
		buttonGerar.setBounds(671, 163, 104, 23);
		add(buttonGerar);
	}

}
