package gui;

import java.awt.CardLayout;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import enums.Telas;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class MainWindow {

	private JFrame frame;
	private JPanel contentPanel;
	private JPanel telaInicialPanel;
	private CadastrarPessoaFisicaPanel cadastrarPessoaFisicaPanel;
	private CadastrarPessoaJuridicaPanel cadastrarPessoaJuridicaPanel;
	private CadastrarEscreventePanel cadastrarEscreventePanel;
	private CadastrarTabeliaoPanel cadastrarTabeliaoPanel;
	private CadastrarServicoPanel cadastrarServicoPanel;
	private PesquisarClientePanel pesquisarClientePanel;
	private PesquisarFuncionarioPanel pesquisarFuncionarioPanel;
	private PesquisarServicoPanel pesquisarServicoPanel;
	private RelatorioPorDataPanel relatorioPorDataPanel;
	private RelatorioPorServicoPanel relatorioPorServicoPanel;
	private RelatorioPorFuncionarioPanel relatorioPorFuncionarioPanel;
	private RelatorioPorClientePanel relatorioPorClientePanel;
	private AtendimentoPanel atendimentoPanel;
	private CardLayout cardLayout;
	private JMenu mnAtualizar;
	private JMenuItem mntmServio;
	private JMenuItem mntmServio_1;
	private SobreDialog sobreDialog;
	private JMenuItem mntmEquipe;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		try {
			initialize();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		
		// Sobre dialog
		sobreDialog = new SobreDialog();
		
		// Layout:
		cardLayout = new CardLayout();
		
		// Frame
		frame = new JFrame();
		frame.setTitle("Equipe 5 - Cart�rio");
		frame.setResizable(false);
		frame.setBounds(100, 100, 790, 590);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Cards:
		contentPanel = new JPanel();
		
		telaInicialPanel = new TelaInicialPanel();
		
		atendimentoPanel = new AtendimentoPanel(cardLayout, contentPanel);
		
		cadastrarPessoaFisicaPanel = new CadastrarPessoaFisicaPanel(cardLayout, contentPanel);
		cadastrarPessoaJuridicaPanel = new CadastrarPessoaJuridicaPanel(cardLayout, contentPanel);
		cadastrarEscreventePanel = new CadastrarEscreventePanel(cardLayout, contentPanel);
		cadastrarTabeliaoPanel = new CadastrarTabeliaoPanel(cardLayout, contentPanel);
		cadastrarServicoPanel = new CadastrarServicoPanel(cardLayout, contentPanel);
		
		pesquisarClientePanel = new PesquisarClientePanel(cardLayout, contentPanel);
		pesquisarFuncionarioPanel = new PesquisarFuncionarioPanel(cardLayout, contentPanel);
		pesquisarServicoPanel = new PesquisarServicoPanel(cardLayout, contentPanel);
		
		relatorioPorDataPanel = new RelatorioPorDataPanel(cardLayout, contentPanel);
		relatorioPorServicoPanel = new RelatorioPorServicoPanel(cardLayout, contentPanel);
		relatorioPorFuncionarioPanel = new RelatorioPorFuncionarioPanel(cardLayout, contentPanel);
		relatorioPorClientePanel = new RelatorioPorClientePanel(cardLayout, contentPanel);
		
		// Setando layout:
		contentPanel.setLayout(cardLayout);
		
		contentPanel.add(telaInicialPanel, Telas.TELA_INICIAL.toString());
		telaInicialPanel.setLayout(null);
		
		// <MENU>
		JMenuBar menuBar = new JMenuBar();
		frame.getContentPane().add(menuBar);
		menuBar.setBounds(0, 0, 800, 21);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);
		
		JMenuItem mntmInicio = new JMenuItem("Inicio");
		mntmInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.TELA_INICIAL.toString());
			}
		});
		mnArquivo.add(mntmInicio);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mntmSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnArquivo.add(mntmSair);
		
		JMenu mnAtendimento_1 = new JMenu("Atendimento");
		menuBar.add(mnAtendimento_1);
		
		JMenuItem mntmIniciar = new JMenuItem("Iniciar");
		mntmIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.TELA_ATENDIMENTO.toString());
				atendimentoPanel.atualizarListaServico();
				atendimentoPanel.atualizarListaMatriculaTabeliao();
				atendimentoPanel.atualizarListaMatriculaEscrevente();
			}
		});
		mnAtendimento_1.add(mntmIniciar);
		
		JMenu mnCadastrar = new JMenu("Cadastrar");
		menuBar.add(mnCadastrar);
		
		JMenu mnCliente = new JMenu("Cliente");
		mnCadastrar.add(mnCliente);
		
		JMenuItem mntmPessoaFisica = new JMenuItem("Pessoa F\u00EDsica");
		mntmPessoaFisica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cardLayout.show(contentPanel, Telas.CADASTRAR_PESSOA_FISICA.toString());
			}
		});
		mnCliente.add(mntmPessoaFisica);
		
		JMenuItem mntmPessoaJuridica = new JMenuItem("Pessoa Jur\u00EDdica");
		mntmPessoaJuridica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.CADASTRAR_PESSOA_JURIDICA.toString());
			}
		});
		mnCliente.add(mntmPessoaJuridica);
		
		JMenu mnNewMenu = new JMenu("Funcion\u00E1rio");
		mnCadastrar.add(mnNewMenu);
		
		JMenuItem mntmEscrevente = new JMenuItem("Escrevente");
		mntmEscrevente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.CADASTRAR_ESCREVENTE.toString());
				cadastrarEscreventePanel.atualizarListaMatriculaSupervisor();
			}
		});
		mnNewMenu.add(mntmEscrevente);
		
		JMenuItem mntmTabeliao = new JMenuItem("Tabeli\u00E3o");
		mntmTabeliao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.CADASTRAR_TABELIAO.toString());
				cadastrarTabeliaoPanel.atualizarListaMatriculaSupervisor();
			}
		});
		mnNewMenu.add(mntmTabeliao);
		
		mntmServio = new JMenuItem("Servi\u00E7o");
		mntmServio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.CADASTRAR_SERVICO.toString());
			}
		});
		mnCadastrar.add(mntmServio);
		
		mnAtualizar = new JMenu("Pesquisar");
		menuBar.add(mnAtualizar);
		
		JMenuItem mntmCliente = new JMenuItem("Cliente");
		mntmCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.PESQUISAR_CLIENTE.toString());
			}
		});
		mnAtualizar.add(mntmCliente);
		
		JMenuItem mntmFuncionario = new JMenuItem("Funcion\u00E1rio");
		mntmFuncionario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.PESQUISAR_FUNCIONARIO.toString());
			}
		});
		mnAtualizar.add(mntmFuncionario);
		
		mntmServio_1 = new JMenuItem("Servi\u00E7o");
		mntmServio_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.PESQUISAR_SERVICO.toString());
				pesquisarServicoPanel.atualizarLista();
			}
		});
		mnAtualizar.add(mntmServio_1);
		
		JMenu mnRelatorios = new JMenu("Relatorios");
		menuBar.add(mnRelatorios);
		
		JMenu mnAtendimento = new JMenu("Atendimento");
		mnRelatorios.add(mnAtendimento);
		
		JMenuItem mntmPorData = new JMenuItem("Por data...");
		mntmPorData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.RELATORIO_POR_DATA.toString());
			}
		});
		mnAtendimento.add(mntmPorData);
		
		JMenuItem mntmPorServio = new JMenuItem("Por servi\u00E7o...");
		mntmPorServio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.RELATORIO_POR_SERVICO.toString());
			}
		});
		mnAtendimento.add(mntmPorServio);
		
		JMenuItem mntmPorFuncionrio = new JMenuItem("Por funcion\u00E1rio...");
		mntmPorFuncionrio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.RELATORIO_POR_FUNCIONARIO.toString());
			}
		});
		mnAtendimento.add(mntmPorFuncionrio);
		
		JMenuItem mntmPorCliente = new JMenuItem("Por cliente...");
		mntmPorCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(contentPanel, Telas.RELATORIO_POR_CLIENTE.toString());
			}
		});
		mnAtendimento.add(mntmPorCliente);
		
		JMenu mnSobre = new JMenu("Sobre");
		menuBar.add(mnSobre);
		
		mntmEquipe = new JMenuItem("Equipe");
		mntmEquipe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				sobreDialog.setLocationRelativeTo(frame);
				sobreDialog.setVisible(true);
			}
		});
		mnSobre.add(mntmEquipe);
		// </MENU>
		
		contentPanel.add(atendimentoPanel, Telas.TELA_ATENDIMENTO.toString());
		contentPanel.add(cadastrarPessoaFisicaPanel, Telas.CADASTRAR_PESSOA_FISICA.toString());
		contentPanel.add(cadastrarPessoaJuridicaPanel, Telas.CADASTRAR_PESSOA_JURIDICA.toString());
		contentPanel.add(cadastrarEscreventePanel, Telas.CADASTRAR_ESCREVENTE.toString());
		contentPanel.add(cadastrarTabeliaoPanel, Telas.CADASTRAR_TABELIAO.toString());
		contentPanel.add(cadastrarServicoPanel, Telas.CADASTRAR_SERVICO.toString());
		contentPanel.add(pesquisarClientePanel, Telas.PESQUISAR_CLIENTE.toString());
		contentPanel.add(pesquisarFuncionarioPanel, Telas.PESQUISAR_FUNCIONARIO.toString());
		contentPanel.add(pesquisarServicoPanel, Telas.PESQUISAR_SERVICO.toString());
		contentPanel.add(relatorioPorDataPanel, Telas.RELATORIO_POR_DATA.toString());
		contentPanel.add(relatorioPorServicoPanel, Telas.RELATORIO_POR_SERVICO.toString());
		contentPanel.add(relatorioPorFuncionarioPanel, Telas.RELATORIO_POR_FUNCIONARIO.toString());
		contentPanel.add(relatorioPorClientePanel, Telas.RELATORIO_POR_CLIENTE.toString());
		
		cardLayout.show(contentPanel, Telas.TELA_INICIAL.toString());
		
		frame.getContentPane().add(contentPanel);
		frame.setVisible(true);
	}
}
