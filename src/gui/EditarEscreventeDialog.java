package gui;

import java.awt.BorderLayout;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import com.toedter.calendar.JDateChooser;

import controladores.ControladorEscrevente;
import entidades.Escrevente;
import entidades.Tabeliao;
import entidades.Telefone;

import javax.swing.JSeparator;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditarEscreventeDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private static Object objetoSelecionado;
	private JTextField textField_Nome;
	private JTextField textField_Matricula;
	private JTextField textField_Logradouro;
	private JTextField textField_Cidade;
	private JTextField textField_Numero;
	private JTextField textField_Bairro;
	private JTextField textField_Cep;
	private JComboBox<String> comboBox_Uf;
	private JDateChooser dateChooser_DataAdmissao;
	private JDateChooser dateChooser_DataNascimento;
	private JComboBox<String> comboBox_MatriculaSupervisor;
	private JFormattedTextField formattedTextField_Salario;
	private JFormattedTextField formattedTextField_Residencial;
	private JFormattedTextField formattedTextField_Celular;
	private JFormattedTextField formattedTextField_Comercial;
	
	private JLabel label_Foto;
	private JLabel label_nome;
	private JLabel label_matricula;
	private JLabel label_matriculaSupervisor;
	private JLabel label_salario;
	private JLabel label_dataNascimento;
	private JLabel label_dataAdmissao;
	private JLabel label_endereco;
	private JLabel label_logradouro;
	private JLabel label_cidade;
	private JLabel label_numero;
	private JLabel label_bairro;
	private JLabel label_uf;
	private JLabel label_cep;
	private JLabel label_telefones;
	private JLabel label_residencial;
	private JLabel label_celular;
	private JLabel label_comercial;
	private JLabel lblEditarEscrevente;
	
	public JButton btnDeletar;
	private JButton btnAtualizar;
	private JButton btn_cancelar;
	
	private JSeparator separator;
	
	private String nomeFoto;
	private String caminho;
	
	private AlertDialog alerta;

	/**
	 * @return the objetoSelecionado
	 */
	public Object getObjetoSelecionado() {
		return objetoSelecionado;
	}

	/**
	 * @param objetoSelecionado the objetoSelecionado to set
	 */
	public void setObjetoSelecionado(Object objetoSelecionado) {
		this.objetoSelecionado = objetoSelecionado;
	}

	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	
	ControladorEscrevente controlador;
	
	public EditarEscreventeDialog(Object objetoSelecionado) throws SQLException {
		controlador = new ControladorEscrevente();
		
		caminho = "/anonimo.png";
		
		this.objetoSelecionado = objetoSelecionado;
		
		setModal(true);
		setType(Type.POPUP);
		setAlwaysOnTop(true);
		setResizable(false);
		setTitle("Dados do Escrevente");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		textField_Nome = new JTextField();
		textField_Nome.setColumns(10);
		textField_Nome.setBounds(239, 111, 349, 20);
		contentPanel.add(textField_Nome);
		
		label_nome = new JLabel("Nome");
		label_nome.setBounds(239, 91, 349, 14);
		contentPanel.add(label_nome);
		
		label_matricula = new JLabel("Matr\u00EDcula");
		label_matricula.setBounds(239, 142, 225, 14);
		contentPanel.add(label_matricula);
		
		textField_Matricula = new JTextField();
		textField_Matricula.setEditable(false);
		textField_Matricula.setColumns(10);
		textField_Matricula.setBounds(239, 163, 225, 20);
		contentPanel.add(textField_Matricula);
		
		label_matriculaSupervisor = new JLabel("Matr\u00EDcula do Supervisor");
		label_matriculaSupervisor.setBounds(239, 201, 217, 14);
		contentPanel.add(label_matriculaSupervisor);
		
		comboBox_MatriculaSupervisor = new JComboBox<String>();
		comboBox_MatriculaSupervisor.setBounds(239, 222, 225, 20);
		List<Escrevente> lista = controlador.buscarMatriculas();
		comboBox_MatriculaSupervisor.removeAllItems();
		comboBox_MatriculaSupervisor.addItem("Selecione...");
		for (int i = 0; i < lista.size(); i++) {
			String matricula = Integer.toString(lista.get(i).getMatricula());
			comboBox_MatriculaSupervisor.addItem(matricula);
		}
		contentPanel.add(comboBox_MatriculaSupervisor);
		
		label_salario = new JLabel("Sal\u00E1rio (R$)");
		label_salario.setBounds(482, 145, 67, 14);
		contentPanel.add(label_salario);
		
		formattedTextField_Salario = new JFormattedTextField();
		formattedTextField_Salario.setBounds(480, 163, 108, 20);
		contentPanel.add(formattedTextField_Salario);
		
		label_dataNascimento = new JLabel("Data de Nascimento");
		label_dataNascimento.setBounds(608, 92, 154, 14);
		contentPanel.add(label_dataNascimento);
		
		dateChooser_DataNascimento = new JDateChooser();
		dateChooser_DataNascimento.setBounds(608, 111, 154, 20);
		contentPanel.add(dateChooser_DataNascimento);
		
		label_dataAdmissao = new JLabel("Data de Admiss\u00E3o");
		label_dataAdmissao.setBounds(608, 145, 149, 14);
		contentPanel.add(label_dataAdmissao);
		
		dateChooser_DataAdmissao = new JDateChooser();
		dateChooser_DataAdmissao.setBounds(608, 163, 155, 20);
		contentPanel.add(dateChooser_DataAdmissao);
		
		label_endereco = new JLabel("Endere\u00E7o");
		label_endereco.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label_endereco.setBounds(24, 271, 186, 23);
		contentPanel.add(label_endereco);
		
		label_logradouro = new JLabel("Logradouro");
		label_logradouro.setBounds(24, 302, 89, 14);
		contentPanel.add(label_logradouro);
		
		textField_Logradouro = new JTextField();
		textField_Logradouro.setColumns(10);
		textField_Logradouro.setBounds(24, 322, 412, 20);
		contentPanel.add(textField_Logradouro);
		
		label_cidade = new JLabel("Cidade");
		label_cidade.setBounds(24, 353, 104, 14);
		contentPanel.add(label_cidade);
		
		textField_Cidade = new JTextField();
		textField_Cidade.setColumns(10);
		textField_Cidade.setBounds(24, 372, 412, 20);
		contentPanel.add(textField_Cidade);
		
		label_numero = new JLabel("N\u00FAmero");
		label_numero.setBounds(455, 302, 86, 14);
		contentPanel.add(label_numero);
		
		label_bairro = new JLabel("Bairro");
		label_bairro.setBounds(561, 302, 201, 14);
		contentPanel.add(label_bairro);
		
		textField_Numero = new JTextField();
		textField_Numero.setColumns(10);
		textField_Numero.setBounds(455, 322, 86, 20);
		contentPanel.add(textField_Numero);
		
		textField_Bairro = new JTextField();
		textField_Bairro.setColumns(10);
		textField_Bairro.setBounds(561, 322, 201, 20);
		contentPanel.add(textField_Bairro);
		
		label_uf = new JLabel("UF");
		label_uf.setBounds(455, 353, 56, 14);
		contentPanel.add(label_uf);
		
		comboBox_Uf = new JComboBox<String>();
		comboBox_Uf.setBounds(455, 372, 89, 20);
		
		String[] UFs = {"", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", 
			 	"GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", 
			 	"RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
			 	 
	 	for(int i = 0; i < UFs.length; i++) {
	 	comboBox_Uf.addItem(UFs[i]);
	 	} 
			 	
		contentPanel.add(comboBox_Uf);
		
		label_cep = new JLabel("CEP");
		label_cep.setBounds(561, 353, 67, 14);
		contentPanel.add(label_cep);
		
		textField_Cep = new JTextField();
		textField_Cep.setColumns(10);
		textField_Cep.setBounds(561, 372, 201, 20);
		contentPanel.add(textField_Cep);
		
		label_telefones = new JLabel("Telefones");
		label_telefones.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label_telefones.setBounds(24, 403, 186, 23);
		contentPanel.add(label_telefones);
		
		label_residencial = new JLabel("Residencial");
		label_residencial.setBounds(24, 437, 86, 14);
		contentPanel.add(label_residencial);
		
		formattedTextField_Residencial = new JFormattedTextField();
		formattedTextField_Residencial.setBounds(24, 457, 143, 20);
		contentPanel.add(formattedTextField_Residencial);
		
		label_celular = new JLabel("Celular");
		label_celular.setBounds(177, 437, 86, 14);
		contentPanel.add(label_celular);
		
		formattedTextField_Celular = new JFormattedTextField();
		formattedTextField_Celular.setBounds(177, 457, 143, 20);
		contentPanel.add(formattedTextField_Celular);
		
		label_comercial = new JLabel("Comercial");
		label_comercial.setBounds(330, 437, 86, 14);
		contentPanel.add(label_comercial);
		
		formattedTextField_Comercial = new JFormattedTextField();
		formattedTextField_Comercial.setBounds(330, 457, 143, 20);
		contentPanel.add(formattedTextField_Comercial);
		
		if(((Escrevente)getObjetoSelecionado()).getSupervisor() == null){
			comboBox_MatriculaSupervisor.setEnabled(false);
		}
		
		btnAtualizar = new JButton("Atualizar");
		btnAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String nome = textField_Nome.getText();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String dataNascimento = dateFormat.format(dateChooser_DataNascimento.getDate()).replaceAll("-", "/");
				String logradouro = textField_Logradouro.getText();
				String numero = textField_Numero.getText();
				String bairro = textField_Bairro.getText();
				String cidade = textField_Cidade.getText();
				String uf = (String) comboBox_Uf.getSelectedItem();
				String cep = textField_Cep.getText().replaceAll("-", "").replaceAll("\\.", "");
				int matricula = Integer.parseInt(textField_Matricula.getText());
				int matriculaSupervisor = 0;
				if(((Escrevente)getObjetoSelecionado()).getSupervisor() != null){
					matriculaSupervisor = Integer.parseInt((String) comboBox_MatriculaSupervisor.getSelectedItem());
				}
				String dataAdmissao = dateFormat.format(dateChooser_DataAdmissao.getDate()).replaceAll("-", "/");
				double salario =  Double.parseDouble(formattedTextField_Salario.getText());
				
				byte[] foto = null;
				
				if(nomeFoto != null){
					
					BufferedImage imagem = null;
					try {
						imagem = ImageIO.read(new File(nomeFoto));
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					
					ByteArrayOutputStream bytesImg = new ByteArrayOutputStream();  
					try {
						ImageIO.write((BufferedImage)imagem, "png", bytesImg);
						bytesImg.flush();  
			            foto = bytesImg.toByteArray(); 
			            bytesImg.close();
						
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				String telResidencial = formattedTextField_Residencial.getText().replaceAll("-", "").replaceAll("[()]", "");

				List<Telefone> telefonesList = new ArrayList<Telefone>();
				
				if(!telResidencial.contains(" ") && telResidencial.length() != 0){
					String dddResidencial = telResidencial.substring(0, 2);
					String numeroResidencial = telResidencial.substring(2, telResidencial.length());
					Telefone telefoneResidencial = new Telefone(dddResidencial, numeroResidencial);
					telefonesList.add(telefoneResidencial);
				}

				String telComercial = formattedTextField_Comercial.getText().replaceAll("-", "").replaceAll("[()]", "");
				
				if(!telComercial.contains(" ") && telComercial.length() != 0){
					String dddComercial = telComercial.substring(0, 2);
					String numeroComercial = telComercial.substring(2, telComercial.length());
					Telefone telefoneComercial = new Telefone(dddComercial, numeroComercial);
					telefonesList.add(telefoneComercial);
				}
				
				String telCelular = formattedTextField_Celular.getText().replaceAll("-", "").replaceAll("[()]", "");
				
				if(!telCelular.contains(" ") && telCelular.length() != 0){
					String dddCelular = telCelular.substring(0, 2);
					String numeroCelular = telCelular.substring(2, telCelular.length());
					Telefone telefoneCelular = new Telefone(dddCelular, numeroCelular);
					telefonesList.add(telefoneCelular);
				}
				
				Telefone [] telefones = new Telefone[telefonesList.size()];
				telefonesList.toArray(telefones);
				
				ControladorEscrevente controlador = new ControladorEscrevente();
				
				try {
					controlador.atualizar(nome, dataNascimento, logradouro, numero, bairro, cidade, uf, cep, telefones, matricula, matriculaSupervisor, salario, dataAdmissao, foto);
					alerta = new AlertDialog("Escrevente atualizado com sucesso!");
					alerta.setVisible(true);
					dispose();
				} catch (SQLException ex) {
					alerta = new AlertDialog("Ocorreu um erro ao atualizar!");
					alerta.setVisible(true);
					ex.printStackTrace();
				}
			}
		});
		btnAtualizar.setBounds(660, 504, 104, 23);
		contentPanel.add(btnAtualizar);
		
		btnDeletar = new JButton("Deletar");
		btnDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					controlador.remover(((Escrevente)getObjetoSelecionado()).getMatricula());
					alerta = new AlertDialog("Escrevente deletado com sucesso!");
					alerta.setVisible(true);
					dispose();
				} catch (SQLException e1) {
					alerta = new AlertDialog("Ocorreu um erro ao deletar escrevente!");
					alerta.setVisible(true);
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnDeletar.setBounds(561, 504, 89, 23);
		contentPanel.add(btnDeletar);
		
		label_Foto = new JLabel("");
		label_Foto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFileChooser fc = new JFileChooser();
				fc.showOpenDialog(contentPanel);
				File f = fc.getSelectedFile();
				Image imgNova = null;
				if(f != null){
					try {
						nomeFoto = f.getAbsolutePath();
						imgNova = ImageIO.read(new File(nomeFoto));
					} catch (IOException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
					label_Foto.setIcon(new ImageIcon(imgNova));
				}
			}
		});
		label_Foto.setBounds(24, 91, 192, 164);
		contentPanel.add(label_Foto);
		
		//Puxa do objeto e seta nos campos TextField
		
		String nome = (String)((Escrevente) getObjetoSelecionado()).getNome();
		textField_Nome.setText(nome);
		int matriculaEscrevente = (int)((Escrevente) getObjetoSelecionado()).getMatricula();
		int matriculaSupervisor = 0;
		if(((Escrevente) getObjetoSelecionado()).getSupervisor() != null){
			matriculaSupervisor = (int)((Escrevente) getObjetoSelecionado()).getSupervisor().getMatricula();
		}
		textField_Matricula.setText(Integer.toString(matriculaEscrevente));
		comboBox_MatriculaSupervisor.setSelectedItem(Integer.toString(matriculaSupervisor));
		String logradouro = (String)((Escrevente) getObjetoSelecionado()).getEndereco().getLogradouro();
		String numero = (String)((Escrevente) getObjetoSelecionado()).getEndereco().getNumero();
		String bairro = (String)((Escrevente) getObjetoSelecionado()).getEndereco().getBairro();
		String cep = (String)((Escrevente) getObjetoSelecionado()).getEndereco().getCep();
		String cidade = (String)((Escrevente) getObjetoSelecionado()).getEndereco().getCidade();
		String uf = (String)((Escrevente) getObjetoSelecionado()).getEndereco().getUf();
		textField_Logradouro.setText(logradouro);
		textField_Cidade.setText(cidade);
		textField_Numero.setText(numero);
		textField_Bairro.setText(bairro);
		textField_Cep.setText(cep);
		comboBox_Uf.setSelectedItem(uf);
		double salario =  (double)((Escrevente) getObjetoSelecionado()).getSalario();
		formattedTextField_Salario.setText(Double.toString(salario));
        
		BufferedImage img = null; 
		   
		if(((Escrevente) getObjetoSelecionado()).getFoto() != null){
			try {  
	            img = ImageIO.read(new ByteArrayInputStream(((Escrevente) getObjetoSelecionado()).getFoto()));  
	            label_Foto.setIcon(new ImageIcon(img));
	        } catch (IOException e) {  
	            // TODO Auto-generated catch block  
	            e.printStackTrace();  
	        }  
		}else{
			Image imgAnonima = new ImageIcon(this.getClass().getResource(caminho)).getImage();
			label_Foto.setIcon(new ImageIcon(imgAnonima));
		}
		
		String dataNascimento = (String)((Escrevente) getObjetoSelecionado()).getDataNascimento();
		String dataAdmissao = (String)((Escrevente) getObjetoSelecionado()).getDataAdmissao();

		try {
			dateChooser_DataAdmissao.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(dataAdmissao));
			dateChooser_DataNascimento.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(dataNascimento));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		String dddResidencial= "", numeroResidencial = "", dddCelular= "", numeroCelular= "", dddComercial= "", numeroComercial= "";
		
		if(((Escrevente) getObjetoSelecionado()).getTelefones()[0] != null){
			dddResidencial = ((Escrevente) getObjetoSelecionado()).getTelefones()[0].getDdd();
			numeroResidencial = ((Escrevente) getObjetoSelecionado()).getTelefones()[0].getNumero();
		}
		
		if(((Escrevente) getObjetoSelecionado()).getTelefones()[1] != null){
			dddCelular = ((Escrevente) getObjetoSelecionado()).getTelefones()[1].getDdd();
			numeroCelular = ((Escrevente) getObjetoSelecionado()).getTelefones()[1].getNumero();
		}
		
		if(((Escrevente) getObjetoSelecionado()).getTelefones()[2] != null){
			dddComercial = ((Escrevente) getObjetoSelecionado()).getTelefones()[2].getDdd();
			numeroComercial = ((Escrevente) getObjetoSelecionado()).getTelefones()[2].getNumero();
		}
		
		String telResidencial, telCelular, telComercial;
		
		telResidencial = dddResidencial+ numeroResidencial;
		telCelular = dddCelular+numeroCelular;		
		telComercial = dddComercial+numeroComercial;
		
		formattedTextField_Residencial.setText(telResidencial);
		formattedTextField_Celular.setText(telCelular);
		formattedTextField_Comercial.setText(telComercial);
		
		lblEditarEscrevente = new JLabel("Editar Escrevente ");
		lblEditarEscrevente.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblEditarEscrevente.setBounds(24, 24, 239, 31);
		contentPanel.add(lblEditarEscrevente);
		
		separator = new JSeparator();
		separator.setBounds(24, 65, 738, 15);
		contentPanel.add(separator);
		
		btn_cancelar = new JButton("Cancelar");
		btn_cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btn_cancelar.setBounds(460, 504, 89, 23);
		contentPanel.add(btn_cancelar);

	}
}
