package gui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import com.toedter.calendar.JDateChooser;

import controladores.ControladorEscrevente;
import controladores.ControladorServico;
import entidades.Escrevente;
import entidades.Servico;
import entidades.Telefone;

import javax.swing.JSeparator;
import javax.swing.JCheckBox;

public class EditarServicoDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Servico servico;
	private JTextField textField_Nome;
	private JFormattedTextField formattedTextField_Preco;
	private JCheckBox checkBox_Autorizado;
	
	private AlertDialog alerta;

	/**
	 * @return the objetoSelecionado
	 */
	public Object getObjetoSelecionado() {
		return servico;
	}

	ControladorServico controlador = new ControladorServico();
	
	public void abrir() {
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
	
	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	public EditarServicoDialog(Servico s) throws SQLException {
		
		this.servico = s;

		setModal(true);
		setType(Type.POPUP);
		setAlwaysOnTop(true);
		setResizable(false);
		setTitle("Dados do Servico");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		textField_Nome = new JTextField();
		textField_Nome.setColumns(10);
		textField_Nome.setBounds(24, 111, 612, 20);
		contentPanel.add(textField_Nome);
		
		JLabel label = new JLabel("Nome");
		label.setBounds(24, 91, 143, 14);
		contentPanel.add(label);
		
		JLabel lblPreor = new JLabel("Pre\u00E7o (R$)");
		lblPreor.setBounds(656, 93, 67, 14);
		contentPanel.add(lblPreor);
		
		formattedTextField_Preco = new JFormattedTextField();
		formattedTextField_Preco.setBounds(654, 111, 108, 20);
		contentPanel.add(formattedTextField_Preco);
		
		checkBox_Autorizado = new JCheckBox("Autorizado");
		if(s.getAutorizado() == 1) {
			checkBox_Autorizado.setSelected(true);
		}
		checkBox_Autorizado.setBounds(24, 147, 97, 23);
		contentPanel.add(checkBox_Autorizado);

		JLabel lblEditarServico = new JLabel("Editar Servi\u00E7o");
		lblEditarServico.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblEditarServico.setBounds(24, 24, 239, 31);
		contentPanel.add(lblEditarServico);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(24, 65, 738, 15);
		contentPanel.add(separator);
		
		JButton buttonDeletar = new JButton("Deletar");
		buttonDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					controlador.remover(((Servico)getObjetoSelecionado()).getCodigo());
					alerta = new AlertDialog("Servi\u00E7o deletado com sucesso!");
					alerta.setVisible(true);
					dispose();
				} catch (SQLException e1) {
					alerta = new AlertDialog("Ocorreu um erro ao deletar servi\u00E7o!");
					alerta.setVisible(true);
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		buttonDeletar.setBounds(547, 470, 89, 23);
		contentPanel.add(buttonDeletar);
		
		JButton button_1 = new JButton("Atualizar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int codigo = servico.getCodigo();
				String nome = textField_Nome.getText();
				double preco =  Double.parseDouble(formattedTextField_Preco.getText());
				boolean selecionado = checkBox_Autorizado.isSelected();
				int autorizado = 0;
				
				if(selecionado){
					autorizado = 1;
				}
				
				try {
					controlador.atualizar(codigo, nome, preco, autorizado);
					alerta = new AlertDialog("Servi\u00E7o atualizado com sucesso!");
					alerta.setVisible(true);
					dispose();
				} catch (SQLException e1) {
					alerta = new AlertDialog("Ocorreu um erro ao atualizar!");
					alerta.setVisible(true);
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_1.setBounds(646, 470, 104, 23);
		contentPanel.add(button_1);
		
		//Puxa do objeto e seta nos campos TextField
		
		String nome = (String)((Servico) getObjetoSelecionado()).getDescricao();
		textField_Nome.setText(nome);
		
		int autorizado = ((Servico) getObjetoSelecionado()).getAutorizado();
		
		if (autorizado == 1){
			checkBox_Autorizado.setSelected(true);
		}else{
			checkBox_Autorizado.setSelected(false);
		}

		double preco =  (double)((Servico) getObjetoSelecionado()).getPreco();
		formattedTextField_Preco.setText(Double.toString(preco));
		
		JButton button = new JButton("Cancelar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		button.setBounds(448, 470, 89, 23);
		contentPanel.add(button);
		
	}
}
