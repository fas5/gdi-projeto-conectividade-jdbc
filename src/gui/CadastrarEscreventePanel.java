package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import controladores.ControladorEscrevente;
import entidades.Escrevente;
import entidades.Tabeliao;
import entidades.Telefone;

public class CadastrarEscreventePanel extends JPanel {

	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JTextField textField_Nome;
	private JDateChooser chooser_DataNascimento;
	private JTextField textField_Matricula;
	private JComboBox<String> comboBox_Matricula_Supervisor;
	private JFormattedTextField formattedTextField_Salario;
	private JDateChooser chooser_DataAdmissao;
	private JTextField textField_Logradouro;
	private JTextField textField_Numero;
	private JTextField textField_Bairro;
	private JTextField textField_Cidade;
	private JComboBox<String> comboBox_Uf;
	private JFormattedTextField textField_Cep;
	private JFormattedTextField formattedTextField_Residencial;
	private JFormattedTextField formattedTextField_Celular;
	private JFormattedTextField formattedTextField_Comercial;
	private List<Escrevente> lista;
	private String nomeFoto;
	
	private String caminho;
	
	private AlertDialog alerta;
	
	private JLabel labelCadastrarEscrevente;
	private JLabel labelNome;
	private JLabel labelData;
	private JLabel labelMatricula;
	private JLabel labelMatriculaSupervisor;
	private JLabel labelSalario;
	private JLabel labelDataAdmissao;
	private JLabel labelEndereco;
	private JLabel labelLogradouro;
	private JLabel labelNumero;
	private JLabel labelBairro;
	private JLabel labelCidade;
	private JLabel labelUf;
	private JLabel labelCep;
	private JLabel labelTelefones;
	private JLabel labelResidencial;
	private JLabel labelCelular;
	private JLabel labelComercial;
	private JLabel label_Foto;
	
	private JButton buttonCadastrar;
	private JButton buttonCancelar;
	
	ControladorEscrevente controlador;

	public CardLayout getCardLayout() {
		return this.cardLayout;
	}
	
	public JPanel getContentPane() {
		return this.contentPanel;
	}
	
	public void atualizarListaMatriculaSupervisor(){
		try {
			lista = controlador.buscarMatriculas();
			comboBox_Matricula_Supervisor.removeAllItems();
			comboBox_Matricula_Supervisor.addItem("Selecione...");
			String matricula;
			for (int i = 0; i < lista.size(); i++) {
				matricula = Integer.toString(lista.get(i).getMatricula());
				comboBox_Matricula_Supervisor.addItem(matricula);
			}
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Create the panel.
	 */

	public CadastrarEscreventePanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		controlador = new ControladorEscrevente();
		caminho = "/anonimo.png";
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelCadastrarEscrevente = new JLabel("Cadastrar Escrevente");
		labelCadastrarEscrevente.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelCadastrarEscrevente.setBounds(10, 33, 239, 31);
		add(labelCadastrarEscrevente);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		label_Foto = new JLabel("");
		label_Foto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFileChooser fc = new JFileChooser();
				fc.showOpenDialog(null);
				File f = fc.getSelectedFile();
				Image imgNova = null;
				if(f != null){
					try {
						nomeFoto = f.getAbsolutePath();
						imgNova = ImageIO.read(new File(nomeFoto));
					} catch (IOException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
					label_Foto.setIcon(new ImageIcon(imgNova));
				}
			}
		});
		Image img = new ImageIcon(this.getClass().getResource(caminho)).getImage();
		label_Foto.setIcon(new ImageIcon(img));
		label_Foto.setBounds(20, 101, 192, 164);
		add(label_Foto);
		
		labelNome = new JLabel("Nome");
		labelNome.setBounds(229, 95, 143, 14);
		add(labelNome);
		
		textField_Nome = new JTextField();
		textField_Nome.setBounds(229, 114, 382, 20);
		add(textField_Nome);
		textField_Nome.setColumns(10);
		
		labelData = new JLabel("Data de Nascimento");
		labelData.setBounds(621, 95, 154, 14);
		add(labelData);
		
		chooser_DataNascimento = new JDateChooser();
		chooser_DataNascimento.setBounds(621, 114, 154, 20);
		chooser_DataNascimento.setMaxSelectableDate(new Date());
		JTextFieldDateEditor chooser_dataNascimentoEditor = (JTextFieldDateEditor) chooser_DataNascimento.getDateEditor();
		chooser_dataNascimentoEditor.setEditable(false);
		add(chooser_DataNascimento);
		
		labelMatricula = new JLabel("Matr\u00EDcula");
		labelMatricula.setBounds(229, 148, 108, 14);
		add(labelMatricula);
		
		textField_Matricula = new JTextField();
		textField_Matricula.setBounds(229, 166, 224, 20);
		add(textField_Matricula);
		textField_Matricula.setColumns(10);
		
		labelMatriculaSupervisor = new JLabel("Matr\u00EDcula do Supervisor");
		labelMatriculaSupervisor.setBounds(229, 197, 197, 14);
		add(labelMatriculaSupervisor);
		
		comboBox_Matricula_Supervisor = new JComboBox<String>();
	 	comboBox_Matricula_Supervisor.setBounds(229, 217, 224, 20);
	 	add(comboBox_Matricula_Supervisor);
		
	 	labelSalario = new JLabel("Sal\u00E1rio (R$)");
		labelSalario.setBounds(468, 148, 73, 14);
		add(labelSalario);
		
		formattedTextField_Salario = new JFormattedTextField();
		formattedTextField_Salario.setBounds(463, 166, 148, 20);
		add(formattedTextField_Salario);
		try {
			MaskFormatter formatter = new MaskFormatter("######");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Salario);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		labelDataAdmissao = new JLabel("Data de Admiss\u00E3o");
		labelDataAdmissao.setBounds(621, 148, 149, 14);
		add(labelDataAdmissao);
		
		chooser_DataAdmissao = new JDateChooser();
		chooser_DataAdmissao.setBounds(621, 166, 155, 20);
		chooser_DataAdmissao.setMaxSelectableDate(new Date());
		JTextFieldDateEditor chooser_dataAdmissaoEditor = (JTextFieldDateEditor) chooser_DataAdmissao.getDateEditor();
		chooser_dataAdmissaoEditor.setEditable(false);
		add(chooser_DataAdmissao);
		
		labelEndereco = new JLabel("Endere\u00E7o");
		labelEndereco.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelEndereco.setBounds(10, 287, 186, 23);
		add(labelEndereco);
		
		labelLogradouro = new JLabel("Logradouro");
		labelLogradouro.setBounds(10, 318, 89, 14);
		add(labelLogradouro);
		
		textField_Logradouro = new JTextField();
		textField_Logradouro.setBounds(10, 338, 448, 20);
		add(textField_Logradouro);
		textField_Logradouro.setColumns(10);
		
		labelNumero = new JLabel("N\u00FAmero");
		labelNumero.setBounds(468, 318, 86, 14);
		add(labelNumero);
		
		textField_Bairro = new JTextField();
		textField_Bairro.setBounds(564, 338, 211, 20);
		add(textField_Bairro);
		textField_Bairro.setColumns(10);
		
		labelBairro = new JLabel("Bairro");
		labelBairro.setBounds(564, 318, 211, 14);
		add(labelBairro);
		
		textField_Numero = new JTextField();
		textField_Numero.setBounds(468, 338, 86, 20);
		add(textField_Numero);
		textField_Numero.setColumns(10);
		
		labelCidade = new JLabel("Cidade");
		labelCidade.setBounds(10, 369, 104, 14);
		add(labelCidade);
		
		textField_Cidade = new JTextField();
		textField_Cidade.setBounds(10, 388, 448, 20);
		add(textField_Cidade);
		textField_Cidade.setColumns(10);
		
		labelUf = new JLabel("UF");
		labelUf.setBounds(468, 369, 56, 14);
		add(labelUf);
		
		comboBox_Uf = new JComboBox<String>();
		comboBox_Uf.setBounds(468, 388, 89, 20);
		String[] UFs = {"", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", 
				"GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", 
				"RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
		for(int i = 0; i < UFs.length; i++) {
			comboBox_Uf.addItem(UFs[i]);
		}	
		
		add(comboBox_Uf);
		
		labelCep = new JLabel("CEP");
		labelCep.setBounds(564, 369, 67, 14);
		add(labelCep);
		
		textField_Cep = new JFormattedTextField();
		textField_Cep.setBounds(564, 388, 211, 20);
		add(textField_Cep);
		textField_Cep.setColumns(10);
		try {
			MaskFormatter formatter = new MaskFormatter("##.###-###");
			formatter.setValidCharacters("0123456789");
			formatter.install(textField_Cep);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		labelTelefones = new JLabel("Telefones");
		labelTelefones.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelTelefones.setBounds(10, 419, 186, 23);
		add(labelTelefones);
		
		labelResidencial = new JLabel("Residencial");
		labelResidencial.setBounds(10, 453, 86, 14);
		add(labelResidencial);
		
		formattedTextField_Residencial = new JFormattedTextField();
		formattedTextField_Residencial.setBounds(10, 473, 143, 20);
		add(formattedTextField_Residencial);
		
		labelCelular = new JLabel("Celular");
		labelCelular.setBounds(163, 453, 86, 14);
		add(labelCelular);
		
		formattedTextField_Celular = new JFormattedTextField();
		formattedTextField_Celular.setBounds(316, 473, 143, 20);
		add(formattedTextField_Celular);
		
		labelComercial = new JLabel("Comercial");
		labelComercial.setBounds(316, 453, 86, 14);
		add(labelComercial);
		
		formattedTextField_Comercial = new JFormattedTextField();
		formattedTextField_Comercial.setBounds(163, 473, 143, 20);
		add(formattedTextField_Comercial);
		
		try {
			MaskFormatter formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Residencial);
			formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Celular);
			formatter = new MaskFormatter("(##)#####-####");
			formatter.setValidCharacters("0123456789");
			formatter.install(formattedTextField_Comercial);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		/** BOTOES */
		
		buttonCadastrar = new JButton("Cadastrar");
		buttonCadastrar.addActionListener(new ActionListener() {
	 		public void actionPerformed(ActionEvent e) {
	 			String nome = textField_Nome.getText();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String dataNascimento = dateFormat.format(chooser_DataNascimento.getDate()).replaceAll("-", "/");
				String logradouro = textField_Logradouro.getText();
				String numero = textField_Numero.getText();
				String bairro = textField_Bairro.getText();
				String cidade = textField_Cidade.getText();
				String uf = (String) comboBox_Uf.getSelectedItem();
				String cep = textField_Cep.getText().replaceAll("-", "").replaceAll("\\.", "");
				int matricula = Integer.parseInt(textField_Matricula.getText());
				
				int matriculaSupervisor = 0;
				if (lista.size() > 1){
					matriculaSupervisor = Integer.parseInt((String) comboBox_Matricula_Supervisor.getSelectedItem());
				}
				String dataAdmissao = dateFormat.format(chooser_DataAdmissao.getDate()).replaceAll("-", "/");
				double salario =  Double.parseDouble(formattedTextField_Salario.getText());
				
				byte[] foto = null;
				
				if(nomeFoto != null){
					
					BufferedImage imagem = null;
					try {
						imagem = ImageIO.read(new File(nomeFoto));
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					
					ByteArrayOutputStream bytesImg = new ByteArrayOutputStream();  
					try {
						ImageIO.write((BufferedImage)imagem, "png", bytesImg);
						bytesImg.flush();  
			            foto = bytesImg.toByteArray(); 
			            bytesImg.close();
						
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				String telResidencial = formattedTextField_Residencial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");
				String telComercial = formattedTextField_Comercial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");
				String telCelular = formattedTextField_Celular.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");
				
				List<Telefone> telefonesList = new ArrayList<Telefone>();
				
				if(telResidencial.length() != 0){
					String dddResidencial = telResidencial.substring(0, 2);
					String numeroResidencial = telResidencial.substring(2, telResidencial.length());
					Telefone telefoneResidencial = new Telefone(dddResidencial, numeroResidencial);
					telefonesList.add(telefoneResidencial);
				}

				if(telCelular.length() != 0){
					String dddCelular = telCelular.substring(0, 2);
					String numeroCelular = telCelular.substring(2, telCelular.length());
					Telefone telefoneCelular = new Telefone(dddCelular, numeroCelular);
					telefonesList.add(telefoneCelular);
				}
				
				if(telComercial.length() != 0){
					String dddComercial = telComercial.substring(0, 2);
					String numeroComercial = telComercial.substring(2, telComercial.length());
					Telefone telefoneComercial = new Telefone(dddComercial, numeroComercial);
					telefonesList.add(telefoneComercial);
				}
				
				Telefone [] telefones = new Telefone[telefonesList.size()];
				telefonesList.toArray(telefones);
				
				try {
					controlador.cadastrar(nome, dataNascimento, logradouro, numero, bairro, cidade, uf, cep, telefones, matricula, matriculaSupervisor, salario, dataAdmissao, foto);
					alerta = new AlertDialog("Cadastro efetuado com sucesso!");
					alerta.setVisible(true);
					resetCampos();
				} catch (SQLException ex) {
					alerta = new AlertDialog("Ocorreu um erro ao cadastrar!");
					alerta.setVisible(true);
					ex.printStackTrace();
				}
	 		}
	 	});
		buttonCadastrar.setBounds(671, 526, 104, 23);
		add(buttonCadastrar);
		
		buttonCancelar = new JButton("Cancelar");
		buttonCancelar.setBounds(572, 526, 89, 23);
		add(buttonCancelar);
		
	}
	
	public void resetCampos(){
		textField_Nome.setText("");
		textField_Matricula.setText("");
		comboBox_Matricula_Supervisor.setSelectedIndex(0);
		textField_Logradouro.setText("");
		textField_Numero.setText("");
		textField_Bairro.setText("");
		textField_Cidade.setText("");
		comboBox_Uf.setSelectedIndex(0);
		textField_Cep.setText("");
		formattedTextField_Salario.setValue(0);
		chooser_DataNascimento.setCalendar(null);
		chooser_DataAdmissao.setCalendar(null);
		formattedTextField_Residencial.setText("");
		formattedTextField_Residencial.setText("");
		formattedTextField_Comercial.setText("");
		Image img = new ImageIcon(this.getClass().getResource(caminho)).getImage();
		label_Foto.setIcon(new ImageIcon(img));
	}
}
