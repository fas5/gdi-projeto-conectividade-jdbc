package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.text.MaskFormatter;

import controladores.ControladorServico;

public class CadastrarServicoPanel extends JPanel {

	ControladorServico controladorServico;
	
	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JTextField textField_Nome;
	private JFormattedTextField formattedTextField_Preco;
	private JCheckBox checkBox_Autorizado;
	
	private JLabel labelCadastrarServico;
	private JLabel labelNome;
	private JLabel labelPreco;
	
	private JButton buttonCadastrar;
	private JButton buttonCancelar;
	
	private AlertDialog alerta;

	public CardLayout getCardLayout() {
		return this.cardLayout;
	}
	
	public JPanel getContentPane() {
		return this.contentPanel;
	}
	
	/**
	 * Create the panel.
	 */
	public CadastrarServicoPanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelCadastrarServico = new JLabel("Cadastrar Servi\u00E7o");
		labelCadastrarServico.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelCadastrarServico.setBounds(10, 33, 239, 31);
		add(labelCadastrarServico);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		labelNome = new JLabel("Nome");
		labelNome.setBounds(10, 94, 143, 14);
		add(labelNome);
		
		textField_Nome = new JTextField();
		textField_Nome.setBounds(10, 114, 601, 20);
		add(textField_Nome);
		textField_Nome.setColumns(10);
		
		labelPreco = new JLabel("Pre\u00E7o (R$)");
		labelPreco.setBounds(621, 95, 154, 14);
		add(labelPreco);
		
		formattedTextField_Preco = new JFormattedTextField();
		formattedTextField_Preco.setBounds(621, 114, 154, 20);
		add(formattedTextField_Preco);
		
		checkBox_Autorizado = new JCheckBox("Autorizado");
		checkBox_Autorizado.setBounds(10, 141, 97, 23);
		add(checkBox_Autorizado);
		
		/** BOTOES */
		
		buttonCadastrar = new JButton("Cadastrar");
		buttonCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = textField_Nome.getText();
				double preco =  Double.parseDouble(formattedTextField_Preco.getText());
				boolean selecionado = checkBox_Autorizado.isSelected();
				int autorizado = 0;
				if(selecionado){
					autorizado = 1;
				}

				try {
					controladorServico = new ControladorServico();
					controladorServico.cadastrar(nome, preco, autorizado);
					alerta = new AlertDialog("Cadastro efetuado com sucesso!");
					alerta.setVisible(true);
					resetCampos();
				} catch (SQLException e1) {
					alerta = new AlertDialog("Ocorreu um erro ao cadastrar!");
					alerta.setVisible(true);
					e1.printStackTrace();
				}
			}
		});
		buttonCadastrar.setBounds(671, 526, 104, 23);
		add(buttonCadastrar);
		
		buttonCancelar = new JButton("Cancelar");
		buttonCancelar.setBounds(572, 526, 89, 23);
		add(buttonCancelar);
	}
	
	public void resetCampos(){
		textField_Nome.setText("");
		checkBox_Autorizado.setSelected(false);
		formattedTextField_Preco.setValue(0);
	}
}
