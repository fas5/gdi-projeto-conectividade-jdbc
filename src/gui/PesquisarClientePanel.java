package gui;

import java.awt.CardLayout;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;

import util.PessoaFisicaNomeComparator;
import util.PessoaJuridicaNomeComparator;
import controladores.ControladorPessoaFisica;
import controladores.ControladorPessoaJuridica;
import entidades.PessoaFisica;
import entidades.PessoaJuridica;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

public class PesquisarClientePanel extends JPanel {

	private EditarPessoaFisicaDialog editarPfDialog;
	private EditarPessoaJuridicaDialog editarPjDialog;
	
	private JRadioButton radioPessoaFisica;
	private JRadioButton radioPessoaJuridica;
	private JRadioButton radioPorNome;
	private JRadioButton radioPorCpf;
	private JTextField textField_nome;
	private JFormattedTextField formattedTextField_cpf;
	private DefaultListModel<String> listModel;
	private JList<String> listaGUI;
	private List<Object> listaPessoaFisica;
	private List<Object> listaPessoaJuridica;
	private int listIndex;
	
	private AlertDialog alerta;
	
	private JLabel labelPesquisarCliente;
	
	private JButton buttonPesquisar;

	/**
	 * Create the panel.
	 */
	
	public PesquisarClientePanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		labelPesquisarCliente = new JLabel("Pesquisar Cliente");
		labelPesquisarCliente.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelPesquisarCliente.setBounds(10, 33, 239, 31);
		add(labelPesquisarCliente);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		radioPessoaFisica = new JRadioButton("Pessoa F\u00EDsica");
		radioPessoaFisica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioPessoaFisica.setSelected(true);
				radioPessoaJuridica.setSelected(false);
				
				radioPorNome.setText("Por Nome");
				radioPorCpf.setText("Por CPF");
				
				buttonPesquisar.setEnabled(true);

				if(!radioPorNome.isEnabled() && !radioPorCpf.isEnabled()) {
					radioPorNome.setEnabled(true);
					radioPorCpf.setEnabled(true);
					textField_nome.setEnabled(true);
				}
			}
		});
		radioPessoaFisica.setBounds(10, 97, 109, 23);
		add(radioPessoaFisica);
		
		radioPessoaJuridica = new JRadioButton("Pessoa Jur\u00EDdica");
		radioPessoaJuridica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioPessoaFisica.setSelected(false);
				radioPessoaJuridica.setSelected(true);
				
				radioPorNome.setText("Por Raz�o Social");
				radioPorCpf.setText("Por CNPJ");
				
				buttonPesquisar.setEnabled(true);
				
				if(!radioPorNome.isEnabled() && !radioPorCpf.isEnabled()) {
					radioPorNome.setEnabled(true);
					radioPorCpf.setEnabled(true);
					textField_nome.setEnabled(true);
				}
			
			}
		});
		radioPessoaJuridica.setBounds(121, 97, 142, 23);
		add(radioPessoaJuridica);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 137, 765, 15);
		add(separator_1);
		
		radioPorNome = new JRadioButton("Por Nome");
		radioPorNome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioPorNome.setSelected(true);
				radioPorCpf.setSelected(false);
				
				textField_nome.setEnabled(true);
				formattedTextField_cpf.setEnabled(false);
				listaGUI.setEnabled(true);
				listaGUI.setVisible(true);
			}
		});
		radioPorNome.setSelected(true);
		radioPorNome.setEnabled(false);
		radioPorNome.setBounds(10, 163, 149, 23);
		add(radioPorNome);
		
		textField_nome = new JTextField();
		textField_nome.setEnabled(false);
		textField_nome.setBounds(165, 163, 385, 22);
		add(textField_nome);
		textField_nome.setColumns(10);
		
		radioPorCpf = new JRadioButton("Por CPF");
		radioPorCpf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioPorNome.setSelected(false);
				radioPorCpf.setSelected(true);
				
				textField_nome.setEnabled(false);
				formattedTextField_cpf.setEnabled(true);
				listaGUI.setEnabled(false);
				listaGUI.setVisible(false);
			}
		});
		radioPorCpf.setEnabled(false);
		radioPorCpf.setBounds(10, 207, 149, 23);
		add(radioPorCpf);
		
		formattedTextField_cpf = new JFormattedTextField();
		formattedTextField_cpf.setEnabled(false);
		formattedTextField_cpf.setBounds(165, 207, 230, 22);
		add(formattedTextField_cpf);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(10, 252, 765, 15);
		add(separator_2);
		
		listModel = new DefaultListModel<String>();
		listaGUI = new JList<String>(listModel);
		listaGUI.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        JList<?> list = (JList<?>)evt.getSource();
		        if (evt.getClickCount() == 2) {
		            // Clique duplo detectado
		            listIndex = list.locationToIndex(evt.getPoint());
		            
		            // Se a JList nao estiver vazia, continue:
		            if(listIndex > -1) {
		            	if(radioPessoaFisica.isSelected()) {
		            		PessoaFisica pf = (PessoaFisica) listaPessoaFisica.get(listIndex);
		            		editarPfDialog = new EditarPessoaFisicaDialog(pf);
		            		editarPfDialog.abrir();
		            	} else {
		            		PessoaJuridica pj = (PessoaJuridica) listaPessoaJuridica.get(listIndex);
		            		editarPjDialog = new EditarPessoaJuridicaDialog(pj);
		            		editarPjDialog.abrir();
		            	}
		            }
		        }
		    }
		});
		listaGUI.setBounds(10, 273, 540, 267);
		add(listaGUI);
		
		buttonPesquisar = new JButton("Pesquisar");
		buttonPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ControladorPessoaFisica controladorPf = new ControladorPessoaFisica();
					ControladorPessoaJuridica controladorPj = new ControladorPessoaJuridica();
					
					if(radioPessoaFisica.isSelected()) {						
						PessoaFisica pf = null;
						
						if(radioPorNome.isSelected()) {
							// Limpa a lista:
							listModel.clear();
							// Pesquisa no banco:
							listaPessoaFisica = controladorPf.pesquisarPorNome(textField_nome.getText().toLowerCase());
							if(listaPessoaFisica.size() == 0){
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}else{
								// Ordena lista por nome:
								Collections.sort(listaPessoaFisica, new PessoaFisicaNomeComparator());
								// Imprime na GUI:
								for(int i = 0; i < listaPessoaFisica.size(); i++) {
									listModel.addElement(((PessoaFisica)listaPessoaFisica.get(i)).getNome());
								}
							}
							
						} else if(radioPorCpf.isSelected()) {
							pf = (PessoaFisica)controladorPf.pesquisarPorCpf(formattedTextField_cpf.getText().replaceAll("\\.", "").replaceAll("-", ""));
							
							if(pf != null) {
								editarPfDialog = new EditarPessoaFisicaDialog(pf);
								editarPfDialog.abrir();
							}else{
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}
						}
					} else if(radioPessoaJuridica.isSelected()) {
						PessoaJuridica pj = null;
						
						if(radioPorNome.isSelected()) {
							// Limpa a lista:
							listModel.clear();
							// Pesquisa no banco:
							listaPessoaJuridica = controladorPj.pesquisarPorNome(textField_nome.getText().toLowerCase());
							
							if(listaPessoaJuridica.size() == 0){
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}else{
								// Ordena lista por nome:
								Collections.sort(listaPessoaJuridica, new PessoaJuridicaNomeComparator());
								// Imprime na GUI:
								for(int i = 0; i < listaPessoaJuridica.size(); i++) {
									listModel.addElement(((PessoaJuridica)listaPessoaJuridica.get(i)).getNome());
								}
							}
						} else if(radioPorCpf.isSelected()) {
							pj = (PessoaJuridica)controladorPj.pesquisarPorCnpj(formattedTextField_cpf.getText().replaceAll("//.", "").replaceAll("-", ""));
							
							if(pj != null) {
								editarPjDialog = new EditarPessoaJuridicaDialog(pj);
								editarPjDialog.abrir();
							}else{
								alerta = new AlertDialog("N\u00E3o Encontrado!");
								alerta.setVisible(true);
							}
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		buttonPesquisar.setEnabled(false);
		buttonPesquisar.setBounds(671, 207, 104, 23);
		add(buttonPesquisar);
	}
}
