package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import controladores.ControladorRelatorios;
import controladores.ControladorServico;
import entidades.RelatorioAtendimento;
import entidades.Servico;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RelatorioPorServicoPanel extends JPanel {

	private CardLayout cardLayout;
	private JPanel contentPanel;
	
	private JLabel labelRelatorioPorServico;
	
	private JButton buttonPesquisar;
	private JComboBox<String> comboBox_servicos;
	private JTable table;
	private DefaultTableModel dtm;
	private JSeparator separator_1;
	
	private AlertDialog alerta;
	
	/**
	 * Create the panel.
	 */
	public RelatorioPorServicoPanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelRelatorioPorServico = new JLabel("Relat�rio de Atendimentos (Por Servi�o)");
		labelRelatorioPorServico.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelRelatorioPorServico.setBounds(10, 33, 765, 31);
		add(labelRelatorioPorServico);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		comboBox_servicos = new JComboBox<String>();
		comboBox_servicos.addItem("Selecione um Servi�o...");
		try {
			ControladorServico cs = new ControladorServico();
			List<Object> servicos = cs.buscarTodos();
			for (int i = 0; i < servicos.size(); i++) {
				comboBox_servicos.addItem("("+String.valueOf(((Servico)servicos.get(i)).getCodigo())+")"+((Servico)servicos.get(i)).getDescricao());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		comboBox_servicos.setBounds(169, 101, 421, 20);
		add(comboBox_servicos);
		
		table = new JTable();
		dtm = new DefaultTableModel(0, 0);
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("Pessoa Nome");
		columnNames.add("Escrevente Nome");
		columnNames.add("Escrevente Matricula");
		columnNames.add("Servi�o Descricao");
		columnNames.add("Servi�o Preco");
		columnNames.add("Servi�o Codigo");
		columnNames.add("Atendimento Quantidade");
		columnNames.add("Atendimento Desconto");
		columnNames.add("Presta Data e Hora");
		dtm.setColumnIdentifiers(columnNames);
		table.setModel(dtm);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(10, 204, 765, 345);
		table.setBounds(10, 250, 1000, 305);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		scrollPane.setViewportView(table);
		add(scrollPane);
		
		buttonPesquisar = new JButton("Pesquisar");
		buttonPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String selecionado = comboBox_servicos.getSelectedItem().toString();
				if(selecionado.equals("Selecione um Servi�o...")){
					//Validacao
				}else{
					int codigo = Integer.parseInt(selecionado.substring(selecionado.indexOf("(")+1, selecionado.indexOf(")")));
					ControladorRelatorios cr = new ControladorRelatorios();
					try {
						for(int i = dtm.getRowCount() - 1; i >=0; i--)
						{
							dtm.removeRow(i); 
						}
						List<RelatorioAtendimento> resultado = cr.buscarPorCodigo(codigo);
						if(resultado.size() == 0){
							alerta = new AlertDialog("N\u00E3o Encontrado!");
							alerta.setVisible(true);
						}else{
							for (int i = 0; i < resultado.size(); i++) {
						        dtm.addRow(new Object[] { resultado.get(i).getPfNome(), resultado.get(i).geteNome(),
						        		resultado.get(i).geteMatricula(), resultado.get(i).getsDescricao(),
						        		resultado.get(i).getsPreco(), resultado.get(i).getsCodigo(),
						        		resultado.get(i).getaQuantidade(), resultado.get(i).getaDesconto(),
						        		resultado.get(i).getPrDataHora()});
							}
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});
		buttonPesquisar.setBounds(486, 132, 104, 23);
		add(buttonPesquisar);
		
		separator_1 = new JSeparator();
		separator_1.setBounds(10, 178, 765, 15);
		add(separator_1);
	}

}
