package gui;

import java.awt.BorderLayout;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import com.toedter.calendar.JDateChooser;

import controladores.ControladorServico;
import controladores.ControladorTabeliao;
import entidades.Escrevente;
import entidades.Servico;
import entidades.Tabeliao;
import entidades.Telefone;

import javax.swing.JSeparator;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditarTabeliaoDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private static Object objetoSelecionado;
	private JTextField textField_Nome;
	private JTextField textField_Matricula;
	private JTextField textField_Logradouro;
	private JTextField textField_Cidade;
	private JTextField textField_Numero;
	private JTextField textField_Bairro;
	private JTextField textField_Cep;
	private JComboBox<String> comboBox_Uf;
	private JDateChooser dateChooser_DataAdmissao;
	private JDateChooser dateChooser_DataNascimento;
	private JComboBox<String> comboBox_MatriculaSupervisor;
	private JFormattedTextField formattedTextField_Salario;
	private JFormattedTextField formattedTextField_Residencial;
	private JFormattedTextField formattedTextField_Celular;
	private JFormattedTextField formattedTextField_Comercial;
	private JComboBox comboBox_Funcao;
	
	private JLabel label_Foto;
	private JLabel label_nome;
	private JLabel label_matricula;
	private JLabel label_matriculaSupervisor;
	private JLabel label_salario;
	private JLabel label_dataNascimento;
	private JLabel label_dataAdmissao;
	private JLabel label_endereco;
	private JLabel label_logradouro;
	private JLabel label_cidade;
	private JLabel label_numero;
	private JLabel label_bairro;
	private JLabel label_uf;
	private JLabel label_cep;
	private JLabel label_telefones;
	private JLabel label_residencial;
	private JLabel label_celular;
	private JLabel label_comercial;
	private JLabel lblEditarTabeliao;
	private JLabel lblFuncao;
	
	public JButton btnDeletar;
	private JButton btnAtualizar;
	private JButton btnCancelar;
	
	private JSeparator separator;
	
	private String nomeFoto;
	
	private AlertDialog alerta;

	private String caminho;

	/**
	 * @return the objetoSelecionado
	 */
	public Object getObjetoSelecionado() {
		return objetoSelecionado;
	}

	/**
	 * @param objetoSelecionado the objetoSelecionado to set
	 */
	public void setObjetoSelecionado(Object objetoSelecionado) {
		this.objetoSelecionado = objetoSelecionado;
	}

	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	
	ControladorTabeliao controlador;
	
	public EditarTabeliaoDialog(Object objetoSelecionado) throws SQLException {
		
		caminho = "/anonimo.png";
		
		controlador = new ControladorTabeliao();
		
		this.objetoSelecionado = objetoSelecionado;
		
		setModal(true);
		setType(Type.POPUP);
		setAlwaysOnTop(true);
		setResizable(false);
		setTitle("Dados do Tabeliao");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		textField_Nome = new JTextField();
		textField_Nome.setColumns(10);
		textField_Nome.setBounds(240, 111, 345, 20);
		contentPanel.add(textField_Nome);
		
		label_nome = new JLabel("Nome");
		label_nome.setBounds(240, 91, 345, 14);
		contentPanel.add(label_nome);
		
		label_matricula = new JLabel("Matr\u00EDcula");
		label_matricula.setBounds(240, 144, 108, 14);
		contentPanel.add(label_matricula);
		
		textField_Matricula = new JTextField();
		textField_Matricula.setEditable(false);
		textField_Matricula.setColumns(10);
		textField_Matricula.setBounds(240, 162, 224, 20);
		contentPanel.add(textField_Matricula);
		
		label_matriculaSupervisor = new JLabel("Matr\u00EDcula do Supervisor");
		label_matriculaSupervisor.setBounds(573, 196, 143, 14);
		contentPanel.add(label_matriculaSupervisor);
		
		comboBox_MatriculaSupervisor = new JComboBox<String>();
		comboBox_MatriculaSupervisor.setBounds(573, 216, 186, 20);
		List<Tabeliao> lista = controlador.buscarMatriculas();
		comboBox_MatriculaSupervisor.removeAllItems();
		comboBox_MatriculaSupervisor.addItem("Selecione...");
		String matricula;
		for (int i = 0; i < lista.size(); i++) {
			matricula = Integer.toString(lista.get(i).getMatricula());
			comboBox_MatriculaSupervisor.addItem(matricula);
		}
		contentPanel.add(comboBox_MatriculaSupervisor);
		
		label_salario = new JLabel("Sal\u00E1rio (R$)");
		label_salario.setBounds(479, 145, 67, 14);
		contentPanel.add(label_salario);
		
		formattedTextField_Salario = new JFormattedTextField();
		formattedTextField_Salario.setBounds(477, 163, 108, 20);
		contentPanel.add(formattedTextField_Salario);
		
		label_dataNascimento = new JLabel("Data de Nascimento");
		label_dataNascimento.setBounds(605, 92, 154, 14);
		contentPanel.add(label_dataNascimento);
		
		dateChooser_DataNascimento = new JDateChooser();
		dateChooser_DataNascimento.setBounds(605, 111, 154, 20);
		contentPanel.add(dateChooser_DataNascimento);
		
		label_dataAdmissao = new JLabel("Data de Admiss\u00E3o");
		label_dataAdmissao.setBounds(605, 145, 149, 14);
		contentPanel.add(label_dataAdmissao);
		
		dateChooser_DataAdmissao = new JDateChooser();
		dateChooser_DataAdmissao.setBounds(605, 163, 155, 20);
		contentPanel.add(dateChooser_DataAdmissao);
		
		label_endereco = new JLabel("Endere\u00E7o");
		label_endereco.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label_endereco.setBounds(21, 261, 186, 23);
		contentPanel.add(label_endereco);
		
		label_logradouro = new JLabel("Logradouro");
		label_logradouro.setBounds(21, 292, 89, 14);
		contentPanel.add(label_logradouro);
		
		textField_Logradouro = new JTextField();
		textField_Logradouro.setColumns(10);
		textField_Logradouro.setBounds(21, 312, 412, 20);
		contentPanel.add(textField_Logradouro);
		
		label_cidade = new JLabel("Cidade");
		label_cidade.setBounds(21, 343, 104, 14);
		contentPanel.add(label_cidade);
		
		textField_Cidade = new JTextField();
		textField_Cidade.setColumns(10);
		textField_Cidade.setBounds(21, 362, 412, 20);
		contentPanel.add(textField_Cidade);
		
		label_numero = new JLabel("N\u00FAmero");
		label_numero.setBounds(452, 292, 86, 14);
		contentPanel.add(label_numero);
		
		label_bairro = new JLabel("Bairro");
		label_bairro.setBounds(558, 292, 201, 14);
		contentPanel.add(label_bairro);
		
		textField_Numero = new JTextField();
		textField_Numero.setColumns(10);
		textField_Numero.setBounds(452, 312, 86, 20);
		contentPanel.add(textField_Numero);
		
		textField_Bairro = new JTextField();
		textField_Bairro.setColumns(10);
		textField_Bairro.setBounds(558, 312, 201, 20);
		contentPanel.add(textField_Bairro);
		
		label_uf = new JLabel("UF");
		label_uf.setBounds(452, 343, 56, 14);
		contentPanel.add(label_uf);
		
		comboBox_Uf = new JComboBox<String>();
		comboBox_Uf.setBounds(452, 362, 89, 20);
		
		String[] UFs = {"", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", 
			 	"GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", 
			 	"RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
			 	 
	 	for(int i = 0; i < UFs.length; i++) {
	 	comboBox_Uf.addItem(UFs[i]);
	 	} 
	 	
	 	lblFuncao = new JLabel("Func\u00E3o");
		lblFuncao.setBounds(240, 196, 46, 14);
		contentPanel.add(lblFuncao);
		
		comboBox_Funcao = new JComboBox();
		comboBox_Funcao.setBounds(240, 216, 311, 20);
		comboBox_Funcao.addItem("");
		comboBox_Funcao.addItem("Tabeliao chefe");
		comboBox_Funcao.addItem("Primeiro substituto");
		comboBox_Funcao.addItem("Segundo substituto");
		contentPanel.add(comboBox_Funcao);
			 	
		contentPanel.add(comboBox_Uf);
		
		label_cep = new JLabel("CEP");
		label_cep.setBounds(558, 343, 67, 14);
		contentPanel.add(label_cep);
		
		textField_Cep = new JTextField();
		textField_Cep.setColumns(10);
		textField_Cep.setBounds(558, 362, 201, 20);
		contentPanel.add(textField_Cep);
		
		label_telefones = new JLabel("Telefones");
		label_telefones.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label_telefones.setBounds(21, 412, 186, 23);
		contentPanel.add(label_telefones);
		
		label_residencial = new JLabel("Residencial");
		label_residencial.setBounds(21, 446, 86, 14);
		contentPanel.add(label_residencial);
		
		formattedTextField_Residencial = new JFormattedTextField();
		formattedTextField_Residencial.setBounds(21, 466, 143, 20);
		contentPanel.add(formattedTextField_Residencial);
		
		label_celular = new JLabel("Celular");
		label_celular.setBounds(174, 446, 86, 14);
		contentPanel.add(label_celular);
		
		formattedTextField_Celular = new JFormattedTextField();
		formattedTextField_Celular.setBounds(174, 466, 143, 20);
		contentPanel.add(formattedTextField_Celular);
		
		label_comercial = new JLabel("Comercial");
		label_comercial.setBounds(327, 446, 86, 14);
		contentPanel.add(label_comercial);
		
		formattedTextField_Comercial = new JFormattedTextField();
		formattedTextField_Comercial.setBounds(327, 466, 143, 20);
		contentPanel.add(formattedTextField_Comercial);
		
		if(((Tabeliao)getObjetoSelecionado()).getSupervisor() == null){
			comboBox_MatriculaSupervisor.setEnabled(false);
		}
		
		btnAtualizar = new JButton("Atualizar");
		btnAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = textField_Nome.getText();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String dataNascimento = dateFormat.format(dateChooser_DataNascimento.getDate()).replaceAll("-", "/");
				String logradouro = textField_Logradouro.getText();
				String numero = textField_Numero.getText();
				String bairro = textField_Bairro.getText();
				String cidade = textField_Cidade.getText();
				String uf = (String) comboBox_Uf.getSelectedItem();
				String cep = textField_Cep.getText().replaceAll("-", "").replaceAll("\\.", "");
				int matricula = Integer.parseInt(textField_Matricula.getText());
				int matriculaSupervisor = 0;
				if(((Tabeliao)getObjetoSelecionado()).getSupervisor() != null){
					matriculaSupervisor = Integer.parseInt((String) comboBox_MatriculaSupervisor.getSelectedItem());
				}
				String dataAdmissao = dateFormat.format(dateChooser_DataAdmissao.getDate()).replaceAll("-", "/");
				double salario =  Double.parseDouble(formattedTextField_Salario.getText());
				String funcao = (String) comboBox_Funcao.getSelectedItem();
				
				byte[] foto = null;
				
				if(nomeFoto != null){
					
					BufferedImage imagem = null;
					try {
						imagem = ImageIO.read(new File(nomeFoto));
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					
					ByteArrayOutputStream bytesImg = new ByteArrayOutputStream();  
					try {
						ImageIO.write((BufferedImage)imagem, "png", bytesImg);
						bytesImg.flush();  
			            foto = bytesImg.toByteArray(); 
			            bytesImg.close();
						
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				String telResidencial = formattedTextField_Residencial.getText().replaceAll("-", "").replaceAll("[()]", "");

				List<Telefone> telefonesList = new ArrayList<Telefone>();
				
				if(!telResidencial.contains(" ") && telResidencial.length() != 0){
					String dddResidencial = telResidencial.substring(0, 2);
					String numeroResidencial = telResidencial.substring(2, telResidencial.length());
					Telefone telefoneResidencial = new Telefone(dddResidencial, numeroResidencial);
					telefonesList.add(telefoneResidencial);
				}

				String telComercial = formattedTextField_Comercial.getText().replaceAll("-", "").replaceAll("[()]", "");
				
				if(!telComercial.contains(" ") && telComercial.length() != 0){
					String dddComercial = telComercial.substring(0, 2);
					String numeroComercial = telComercial.substring(2, telComercial.length());
					Telefone telefoneComercial = new Telefone(dddComercial, numeroComercial);
					telefonesList.add(telefoneComercial);
				}
				
				String telCelular = formattedTextField_Celular.getText().replaceAll("-", "").replaceAll("[()]", "");
				
				if(!telCelular.contains(" ") && telCelular.length() != 0){
					String dddCelular = telCelular.substring(0, 2);
					String numeroCelular = telCelular.substring(2, telCelular.length());
					Telefone telefoneCelular = new Telefone(dddCelular, numeroCelular);
					telefonesList.add(telefoneCelular);
				}
				
				Telefone [] telefones = new Telefone[telefonesList.size()];
				telefonesList.toArray(telefones);
				
				ControladorTabeliao controlador = new ControladorTabeliao();
				
				try {
					controlador.atualizar(nome, dataNascimento, logradouro, numero, bairro, cidade, uf, cep, telefones, matricula, matriculaSupervisor, salario, dataAdmissao, funcao, foto);
					alerta = new AlertDialog("Tabeli\u00E3o atualizado com sucesso!");
					alerta.setVisible(true);
					dispose();
				} catch (SQLException ex) {
					alerta = new AlertDialog("Ocorreu um erro ao atualizar!");
					alerta.setVisible(true);
					ex.printStackTrace();
				}
			}
		});
		btnAtualizar.setBounds(655, 516, 104, 23);
		contentPanel.add(btnAtualizar);
		
		btnDeletar = new JButton("Deletar");
		btnDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					controlador.remover(((Tabeliao)getObjetoSelecionado()).getMatricula());
					alerta = new AlertDialog("Tabeli\u00E3o deletado com sucesso!");
					alerta.setVisible(true);
					dispose();
				} catch (SQLException e1) {
					alerta = new AlertDialog("Ocorreu um erro ao deletar tabeli\u00E3o!");
					alerta.setVisible(true);
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnDeletar.setBounds(556, 516, 89, 23);
		contentPanel.add(btnDeletar);
		
		label_Foto = new JLabel("");
		label_Foto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFileChooser fc = new JFileChooser();
				setAlwaysOnTop(true);
				fc.showOpenDialog(contentPanel);
				File f = fc.getSelectedFile();
				Image imgNova = null;
				if(f != null){
					try {
						nomeFoto = f.getAbsolutePath();
						imgNova = ImageIO.read(new File(nomeFoto));
					} catch (IOException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
					label_Foto.setIcon(new ImageIcon(imgNova));
				}
			}
		});
		label_Foto.setBounds(21, 86, 192, 164);
		contentPanel.add(label_Foto);
		
		//Puxa do objeto e seta nos campos TextField
		
		String nome = (String)((Tabeliao) getObjetoSelecionado()).getNome();
		textField_Nome.setText(nome);
		int matriculaTabeliao = (int)((Tabeliao) getObjetoSelecionado()).getMatricula();
		int matriculaSupervisor = 0;
		if(((Tabeliao) getObjetoSelecionado()).getSupervisor() != null){
			matriculaSupervisor = (int)((Tabeliao) getObjetoSelecionado()).getSupervisor().getMatricula();
		}
		textField_Matricula.setText(Integer.toString(matriculaTabeliao));
		comboBox_MatriculaSupervisor.setSelectedItem(Integer.toString(matriculaSupervisor));
		String logradouro = (String)((Tabeliao) getObjetoSelecionado()).getEndereco().getLogradouro();
		String numero = (String)((Tabeliao) getObjetoSelecionado()).getEndereco().getNumero();
		String bairro = (String)((Tabeliao) getObjetoSelecionado()).getEndereco().getBairro();
		String cep = (String)((Tabeliao) getObjetoSelecionado()).getEndereco().getCep();
		String cidade = (String)((Tabeliao) getObjetoSelecionado()).getEndereco().getCidade();
		String uf = (String)((Tabeliao) getObjetoSelecionado()).getEndereco().getUf();
		textField_Logradouro.setText(logradouro);
		textField_Cidade.setText(cidade);
		textField_Numero.setText(numero);
		textField_Bairro.setText(bairro);
		textField_Cep.setText(cep);
		comboBox_Uf.setSelectedItem(uf);
		String dataNascimento = (String)((Tabeliao) getObjetoSelecionado()).getDataNascimento();
		String dataAdmissao = (String)((Tabeliao) getObjetoSelecionado()).getDataAdmissao();
		double salario =  (double)((Tabeliao) getObjetoSelecionado()).getSalario();
		formattedTextField_Salario.setText(Double.toString(salario));
		
		BufferedImage img = null; 
        
		if(((Tabeliao) getObjetoSelecionado()).getFoto() != null){
		 	try {  
	            img = ImageIO.read(new ByteArrayInputStream(((Tabeliao) getObjetoSelecionado()).getFoto()));  
	            label_Foto.setIcon(new ImageIcon(img));
	        } catch (IOException e) {  
	            // TODO Auto-generated catch block  
	            e.printStackTrace();  
	        }  
		}else{
			Image imgAnonima = new ImageIcon(this.getClass().getResource(caminho)).getImage();
			label_Foto.setIcon(new ImageIcon(imgAnonima));
		}	
	        
		String funcao = (String)((Tabeliao) getObjetoSelecionado()).getFuncao();
		comboBox_Funcao.setSelectedItem(funcao);
		try {
			dateChooser_DataAdmissao.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(dataAdmissao));
			dateChooser_DataNascimento.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(dataNascimento));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		String dddResidencial= "", numeroResidencial = "", dddCelular= "", numeroCelular= "", dddComercial= "", numeroComercial= "";
		
		if(((Tabeliao) getObjetoSelecionado()).getTelefones()[0] != null){
			dddResidencial = ((Tabeliao) getObjetoSelecionado()).getTelefones()[0].getDdd();
			numeroResidencial = ((Tabeliao) getObjetoSelecionado()).getTelefones()[0].getNumero();
		}
		
		if(((Tabeliao) getObjetoSelecionado()).getTelefones()[1] != null){
			dddCelular = ((Tabeliao) getObjetoSelecionado()).getTelefones()[1].getDdd();
			numeroCelular = ((Tabeliao) getObjetoSelecionado()).getTelefones()[1].getNumero();
		}
		
		if(((Tabeliao) getObjetoSelecionado()).getTelefones()[2] != null){
			dddComercial = ((Tabeliao) getObjetoSelecionado()).getTelefones()[2].getDdd();
			numeroComercial = ((Tabeliao) getObjetoSelecionado()).getTelefones()[2].getNumero();
		}
		
		String telResidencial, telCelular, telComercial;
		
		telResidencial = dddResidencial+ numeroResidencial;
		telCelular = dddCelular+numeroCelular;		
		telComercial = dddComercial+numeroComercial;
		
		formattedTextField_Residencial.setText(telResidencial);
		formattedTextField_Celular.setText(telCelular);
		formattedTextField_Comercial.setText(telComercial);
		
		lblEditarTabeliao = new JLabel("Editar Tabeli\u00E3o");
		lblEditarTabeliao.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblEditarTabeliao.setBounds(21, 26, 239, 31);
		contentPanel.add(lblEditarTabeliao);
		
		separator = new JSeparator();
		separator.setBounds(21, 57, 738, 15);
		contentPanel.add(separator);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancelar.setBounds(452, 516, 89, 23);
		contentPanel.add(btnCancelar);

	}
}
