package gui;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import controladores.ControladorPessoaFisica;
import entidades.PessoaFisica;
import entidades.Telefone;

import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class EditarPessoaFisicaDialog extends JDialog {
	private ControladorPessoaFisica controlador;
	private PessoaFisica pessoaFisica;
	
	private AlertDialog alert;
	
	private JTextField textField_Nome;
	private JTextField textField_Profissao;
	private JTextField textField_Logradouro;
	private JTextField textField_Cidade;
	private JTextField textField_Numero;
	private JTextField textField_Bairro;
	private JFormattedTextField formattedTextField_cpf;
	private JDateChooser chooser_DataNascimento;
	private JComboBox<String> comboBox_Sexo;
	private JFormattedTextField formattedTextField_Renda;
	private JComboBox<String> comboBox_Uf;
	private JFormattedTextField formattedTextField_Cep;
	private JFormattedTextField formattedTextField_Celular;
	private JFormattedTextField formattedTextField_Residencial;
	private JFormattedTextField formattedTextField_Comercial;
	
	private JSeparator separator;
	private JLabel label;
	private JLabel labelNome;
	private JLabel labelDataNascimento;
	private JLabel labelCpf;
	private JLabel labelSexo;
	private JLabel labelRenda;
	private JLabel labelProfissao;
	private JLabel labelBairro;
	private JLabel labelCidade;
	private JLabel labelUf;
	private JLabel labelEndereco;
	private JLabel labelLogradouro;
	private JLabel labelNumero;
	private JLabel labelCep;
	private JLabel labelComercial;
	private JLabel labelCelular;
	private JLabel labelResidencial;
	private JLabel labelTelefones;
	
	private JButton button_Atualizar;
	
	private AlertDialog alerta;
	
	public void abrir() {
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Create the dialog.
	 */
	public EditarPessoaFisicaDialog(PessoaFisica pf) {
		this.pessoaFisica = pf;
		
		setAlwaysOnTop(true);
		setModal(true);
		setTitle("Editar Pessoa F\u00EDsica");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(null);
		
		label = new JLabel("Editar Pessoa F\u00EDsica");
		label.setFont(new Font("Tahoma", Font.BOLD, 17));
		label.setBounds(10, 34, 239, 31);
		getContentPane().add(label);
		
		separator = new JSeparator();
		separator.setBounds(10, 76, 765, 15);
		getContentPane().add(separator);
		
		labelNome = new JLabel("Nome");
		labelNome.setBounds(10, 95, 143, 14);
		getContentPane().add(labelNome);
		
		textField_Nome = new JTextField();
		textField_Nome.setText(pf.getNome());
		textField_Nome.setColumns(10);
		textField_Nome.setBounds(10, 115, 601, 20);
		getContentPane().add(textField_Nome);
		
		labelDataNascimento = new JLabel("Data de Nascimento");
		labelDataNascimento.setBounds(621, 96, 154, 14);
		getContentPane().add(labelDataNascimento);
		
		chooser_DataNascimento = new JDateChooser();
		String dataNascimento = pf.getDataNascimento();
		try {
			chooser_DataNascimento.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(dataNascimento));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		chooser_DataNascimento.setBounds(621, 115, 154, 20);
		getContentPane().add(chooser_DataNascimento);
		
		labelCpf = new JLabel("CPF");
		labelCpf.setBounds(10, 149, 108, 14);
		getContentPane().add(labelCpf);
		
		formattedTextField_cpf = new JFormattedTextField();
		formattedTextField_cpf.setText(pf.getCpf());
		formattedTextField_cpf.setEditable(false);
		formattedTextField_cpf.setBounds(10, 167, 224, 20);
		getContentPane().add(formattedTextField_cpf);
		
		labelSexo = new JLabel("Sexo");
		labelSexo.setBounds(244, 149, 46, 14);
		getContentPane().add(labelSexo);
		
		comboBox_Sexo = new JComboBox<String>();
		comboBox_Sexo.addItem("F");
		comboBox_Sexo.addItem("M");
		comboBox_Sexo.setSelectedItem(String.valueOf(pf.getSexo()));
		comboBox_Sexo.setBounds(244, 167, 50, 20);
		getContentPane().add(comboBox_Sexo);
		
		labelRenda = new JLabel("Renda (R$)");
		labelRenda.setBounds(304, 149, 67, 14);
		getContentPane().add(labelRenda);
		
		formattedTextField_Renda = new JFormattedTextField();
		formattedTextField_Renda.setText(String.valueOf(pf.getRenda()));
		formattedTextField_Renda.setBounds(304, 167, 141, 20);
		getContentPane().add(formattedTextField_Renda);
		
		labelProfissao = new JLabel("Profiss\u00E3o");
		labelProfissao.setBounds(455, 149, 149, 14);
		getContentPane().add(labelProfissao);
		
		textField_Profissao = new JTextField();
		textField_Profissao.setText(pf.getProfissao());
		textField_Profissao.setColumns(10);
		textField_Profissao.setBounds(455, 167, 321, 20);
		getContentPane().add(textField_Profissao);
		
		labelEndereco = new JLabel("Endere\u00E7o");
		labelEndereco.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelEndereco.setBounds(10, 226, 186, 23);
		getContentPane().add(labelEndereco);
		
		labelLogradouro = new JLabel("Logradouro");
		labelLogradouro.setBounds(10, 257, 89, 14);
		getContentPane().add(labelLogradouro);
		
		textField_Logradouro = new JTextField();
		textField_Logradouro.setText(pf.getEndereco().getLogradouro());
		textField_Logradouro.setColumns(10);
		textField_Logradouro.setBounds(10, 277, 448, 20);
		getContentPane().add(textField_Logradouro);
		
		labelNumero = new JLabel("N\u00FAmero");
		labelNumero.setBounds(468, 257, 86, 14);
		getContentPane().add(labelNumero);
		
		textField_Numero = new JTextField();
		textField_Numero.setText(pf.getEndereco().getNumero());
		textField_Numero.setColumns(10);
		textField_Numero.setBounds(468, 277, 86, 20);
		getContentPane().add(textField_Numero);
		
		labelBairro = new JLabel("Bairro");
		labelBairro.setBounds(564, 257, 211, 14);
		getContentPane().add(labelBairro);
		
		textField_Bairro = new JTextField();
		textField_Bairro.setText(pf.getEndereco().getBairro());
		textField_Bairro.setColumns(10);
		textField_Bairro.setBounds(564, 277, 211, 20);
		getContentPane().add(textField_Bairro);
		
		labelCidade = new JLabel("Cidade");
		labelCidade.setBounds(10, 308, 104, 14);
		getContentPane().add(labelCidade);
		
		textField_Cidade = new JTextField();
		textField_Cidade.setText(pf.getEndereco().getCidade());
		textField_Cidade.setColumns(10);
		textField_Cidade.setBounds(10, 327, 448, 20);
		getContentPane().add(textField_Cidade);
		
		labelUf = new JLabel("UF");
		getContentPane().add(labelUf);
		
		comboBox_Uf = new JComboBox<String>();
		labelUf.setBounds(468, 308, 56, 14);
		String[] UFs = {"", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", 
				"GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", 
				"RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
		for(int i = 0; i < UFs.length; i++) {
			comboBox_Uf.addItem(UFs[i]);
		}
		comboBox_Uf.setSelectedItem(pf.getEndereco().getUf());
		comboBox_Uf.setBounds(468, 327, 89, 20);
		getContentPane().add(comboBox_Uf);
		
		labelCep = new JLabel("CEP");
		labelCep.setBounds(564, 308, 67, 14);
		getContentPane().add(labelCep);
		
		formattedTextField_Cep = new JFormattedTextField();
		formattedTextField_Cep.setText(pf.getEndereco().getCep());
		formattedTextField_Cep.setColumns(10);
		formattedTextField_Cep.setBounds(564, 327, 211, 20);
		getContentPane().add(formattedTextField_Cep);
		
		labelTelefones = new JLabel("Telefones");
		labelTelefones.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelTelefones.setBounds(10, 377, 186, 23);
		getContentPane().add(labelTelefones);
		
		Telefone[] telefones = pf.getTelefones();
		
		labelResidencial = new JLabel("Residencial");
		labelResidencial.setBounds(10, 411, 86, 14);
		getContentPane().add(labelResidencial);
		
		formattedTextField_Residencial = new JFormattedTextField();
		if(telefones[0] != null)
			formattedTextField_Residencial.setText(telefones[0].getDdd() + telefones[0].getNumero());
		formattedTextField_Residencial.setBounds(10, 431, 143, 20);
		getContentPane().add(formattedTextField_Residencial);
		
		labelCelular = new JLabel("Celular");
		labelCelular.setBounds(163, 411, 86, 14);
		getContentPane().add(labelCelular);
		
		formattedTextField_Celular = new JFormattedTextField();
		if(telefones[1] != null)
			formattedTextField_Celular.setText(telefones[1].getDdd() + telefones[1].getNumero());
		formattedTextField_Celular.setBounds(163, 431, 143, 20);
		getContentPane().add(formattedTextField_Celular);
		
		labelComercial = new JLabel("Comercial");
		labelComercial.setBounds(316, 411, 86, 14);
		getContentPane().add(labelComercial);
		
		formattedTextField_Comercial = new JFormattedTextField();
		if(telefones[2] != null)
			formattedTextField_Comercial.setText(telefones[2].getDdd() + telefones[2].getNumero());
		formattedTextField_Comercial.setBounds(316, 431, 143, 20);
		getContentPane().add(formattedTextField_Comercial);
		
		button_Atualizar = new JButton("Atualizar");
		button_Atualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nome = textField_Nome.getText();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String dataNascimento = dateFormat.format(chooser_DataNascimento.getDate()).replaceAll("-", "/");
				String cpf = formattedTextField_cpf.getText().replaceAll("-", "").replaceAll("\\.", "");
				String sexoS = (String) comboBox_Sexo.getSelectedItem();
				char sexo = sexoS.charAt(0);
				double renda =  Double.parseDouble(formattedTextField_Renda.getText());
				String profissao = textField_Profissao.getText();
				String logradouro = textField_Logradouro.getText();
				String numero = textField_Numero.getText();
				String bairro = textField_Bairro.getText();
				String cidade = textField_Cidade.getText();
				String uf = (String) comboBox_Uf.getSelectedItem();
				String cep = formattedTextField_Cep.getText().replaceAll("-", "").replaceAll("\\.", "");
				
				String telResidencial = formattedTextField_Residencial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");
				String telComercial = formattedTextField_Comercial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");
				String telCelular = formattedTextField_Celular.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", "");

				List<Telefone> telefonesList = new ArrayList<Telefone>();
				
				if(telResidencial.length() != 0){
					String dddResidencial = telResidencial.substring(0, 2);
					String numeroResidencial = telResidencial.substring(2, telResidencial.length());
					Telefone telefoneResidencial = new Telefone(dddResidencial, numeroResidencial);
					telefonesList.add(telefoneResidencial);
				}

				if(telCelular.length() != 0){
					String dddCelular = telCelular.substring(0, 2);
					String numeroCelular = telCelular.substring(2, telCelular.length());
					Telefone telefoneCelular = new Telefone(dddCelular, numeroCelular);
					telefonesList.add(telefoneCelular);
				}
				
				if(telComercial.length() != 0){
					String dddComercial = telComercial.substring(0, 2);
					String numeroComercial = telComercial.substring(2, telComercial.length());
					Telefone telefoneComercial = new Telefone(dddComercial, numeroComercial);
					telefonesList.add(telefoneComercial);
				}
				
				Telefone [] telefones = new Telefone[telefonesList.size()];
				telefonesList.toArray(telefones);
				
				try {
					ControladorPessoaFisica controlador = new ControladorPessoaFisica();
					controlador.atualizar(nome, dataNascimento, cpf, sexo, renda, profissao, logradouro, numero, bairro, cidade, uf, cep, telefones);
					alerta = new AlertDialog("Cliente atualizado com sucesso!");
					alerta.setVisible(true);
					dispose();
				} catch (SQLException e) {
					alerta = new AlertDialog("Erro ao atualizar cliente!");
					alerta.setVisible(true);
					e.printStackTrace();
				}
			}
		});
		button_Atualizar.setBounds(671, 527, 104, 23);
		getContentPane().add(button_Atualizar);
		
		JButton button_Deletar = new JButton("Deletar");
		button_Deletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					controlador = new ControladorPessoaFisica();
					controlador.deletar(pessoaFisica.getCpf());
					alert = new AlertDialog("Cliente deletado com sucesso!");
					alert.abrir();
					dispose();
				} catch (SQLException e) {
					alert = new AlertDialog("Falha ao deletar cliente!");
					alert.abrir();
					e.printStackTrace();
				}
			}
		});
		button_Deletar.setBounds(572, 527, 89, 23);
		getContentPane().add(button_Deletar);
		
		JButton button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		button_Cancelar.setBounds(473, 527, 89, 23);
		getContentPane().add(button_Cancelar);
	}
}
