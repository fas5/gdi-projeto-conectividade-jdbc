package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import entidades.Endereco;
import entidades.PessoaJuridica;
import entidades.Telefone;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import controladores.ControladorPessoaFisica;
import controladores.ControladorPessoaJuridica;

import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class EditarPessoaJuridicaDialog extends JDialog {
	private ControladorPessoaJuridica controlador;
	private PessoaJuridica pessoaJuridica;
	
	private AlertDialog alert;
	
	private JTextField textField_RazaoSocial;
	private JTextField textField_AreaAtuacao;
	private JTextField textField_Logradouro;
	private JTextField textField_Numero;
	private JTextField textField_Bairro;
	private JTextField textField_Cidade;
	private JDateChooser chooser_DataFundadacao;
	private JFormattedTextField formattedTextField_cnpj;
	private JComboBox<String> comboBox_Uf;
	private JFormattedTextField formattedTextField_Cep;
	private JFormattedTextField formattedTextField_Residencial;
	private JFormattedTextField formattedTextField_Celular;
	private JFormattedTextField formattedTextField_Comercial;
	
	private JSeparator separator;
	private JLabel labelEditarPessoaJuridica;
	private JLabel labelRazaoSocial;
	private JLabel labelDataFundacao;
	private JLabel labelCnpj;
	private JLabel labelAreaAtuacao;
	private JLabel labelEndereco;
	private JLabel labelLogradouro;
	private JLabel labelNumero;
	private JLabel labelBairro;
	private JLabel labelCidade;
	private JLabel labelUf;
	private JLabel labelTelefones;
	private JLabel labelResidencial;
	private JLabel labelCelular;
	private JLabel labelComercial;
	private JLabel labelCep;
	
	public void abrir() {
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Create the dialog.
	 */
	public EditarPessoaJuridicaDialog(PessoaJuridica pj) {
		this.pessoaJuridica = pj;
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Editar Pessoa Jur\u00EDdica");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(null);
		
		labelEditarPessoaJuridica = new JLabel("Editar Pessoa Jur\u00EDdica");
		labelEditarPessoaJuridica.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelEditarPessoaJuridica.setBounds(10, 34, 239, 31);
		getContentPane().add(labelEditarPessoaJuridica);
		
		separator = new JSeparator();
		separator.setBounds(10, 76, 765, 15);
		getContentPane().add(separator);
		
		labelRazaoSocial = new JLabel("Raz\u00E3o Social");
		labelRazaoSocial.setBounds(10, 95, 143, 14);
		getContentPane().add(labelRazaoSocial);
		
		textField_RazaoSocial = new JTextField();
		textField_RazaoSocial.setText(pj.getNome());
		textField_RazaoSocial.setColumns(10);
		textField_RazaoSocial.setBounds(10, 115, 601, 20);
		getContentPane().add(textField_RazaoSocial);
		
		labelDataFundacao = new JLabel("Data de Funda\u00E7\u00E3o");
		labelDataFundacao.setBounds(621, 96, 154, 14);
		getContentPane().add(labelDataFundacao);
		
		chooser_DataFundadacao = new JDateChooser();
		String dataFundadacao = pj.getDataNascimento();
		try {
			chooser_DataFundadacao.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(dataFundadacao));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		chooser_DataFundadacao.setBounds(621, 115, 154, 20);
		getContentPane().add(chooser_DataFundadacao);
		
		labelCnpj = new JLabel("CNPJ");
		labelCnpj.setBounds(10, 149, 108, 14);
		getContentPane().add(labelCnpj);
		
		formattedTextField_cnpj = new JFormattedTextField();
		formattedTextField_cnpj.setText(pj.getCnpj());
		formattedTextField_cnpj.setBounds(10, 167, 224, 20);
		getContentPane().add(formattedTextField_cnpj);
		
		labelAreaAtuacao = new JLabel("\u00C1rea de Atua\u00E7\u00E3o");
		labelAreaAtuacao.setBounds(244, 149, 149, 14);
		getContentPane().add(labelAreaAtuacao);
		
		textField_AreaAtuacao = new JTextField();
		textField_AreaAtuacao.setText(pj.getAreaAtuacao());
		textField_AreaAtuacao.setColumns(10);
		textField_AreaAtuacao.setBounds(244, 167, 531, 20);
		getContentPane().add(textField_AreaAtuacao);
		
		labelEndereco = new JLabel("Endere\u00E7o");
		labelEndereco.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelEndereco.setBounds(10, 226, 186, 23);
		getContentPane().add(labelEndereco);
		
		labelLogradouro = new JLabel("Logradouro");
		labelLogradouro.setBounds(10, 257, 89, 14);
		getContentPane().add(labelLogradouro);
		
		textField_Logradouro = new JTextField();
		textField_Logradouro.setText(pj.getEndereco().getLogradouro());
		textField_Logradouro.setColumns(10);
		textField_Logradouro.setBounds(10, 277, 448, 20);
		getContentPane().add(textField_Logradouro);
		
		labelNumero = new JLabel("N\u00FAmero");
		labelNumero.setBounds(468, 257, 86, 14);
		getContentPane().add(labelNumero);
		
		textField_Numero = new JTextField();
		textField_Numero.setText(pj.getEndereco().getNumero());
		textField_Numero.setColumns(10);
		textField_Numero.setBounds(468, 277, 86, 20);
		getContentPane().add(textField_Numero);
		
		labelBairro = new JLabel("Bairro");
		labelBairro.setBounds(564, 257, 211, 14);
		getContentPane().add(labelBairro);
		
		textField_Bairro = new JTextField();
		textField_Bairro.setText(pj.getEndereco().getBairro());
		textField_Bairro.setColumns(10);
		textField_Bairro.setBounds(564, 277, 211, 20);
		getContentPane().add(textField_Bairro);
		
		labelCidade = new JLabel("Cidade");
		labelCidade.setBounds(10, 308, 104, 14);
		getContentPane().add(labelCidade);
		
		textField_Cidade = new JTextField();
		textField_Cidade.setText(pj.getEndereco().getCidade());
		textField_Cidade.setColumns(10);
		textField_Cidade.setBounds(10, 327, 448, 20);
		getContentPane().add(textField_Cidade);
		
		labelUf = new JLabel("UF");
		labelUf.setBounds(468, 308, 56, 14);
		getContentPane().add(labelUf);
		
		comboBox_Uf = new JComboBox<String>();
		String[] UFs = {"", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", 
				"GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", 
				"RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
		for(int i = 0; i < UFs.length; i++) {
			comboBox_Uf.addItem(UFs[i]);
		}
		comboBox_Uf.setSelectedItem(pj.getEndereco().getUf());
		comboBox_Uf.setBounds(468, 327, 89, 20);
		getContentPane().add(comboBox_Uf);
		
		labelCep = new JLabel("CEP");
		labelCep.setBounds(564, 308, 67, 14);
		getContentPane().add(labelCep);
		
		formattedTextField_Cep = new JFormattedTextField();
		formattedTextField_Cep.setText(pj.getEndereco().getCep());
		formattedTextField_Cep.setColumns(10);
		formattedTextField_Cep.setBounds(564, 327, 211, 20);
		getContentPane().add(formattedTextField_Cep);
		
		labelTelefones = new JLabel("Telefones");
		labelTelefones.setFont(new Font("Tahoma", Font.PLAIN, 17));
		labelTelefones.setBounds(10, 377, 186, 23);
		getContentPane().add(labelTelefones);
		
		Telefone[] telefones = pj.getTelefones();
		
		labelResidencial = new JLabel("Residencial");
		labelResidencial.setBounds(10, 411, 86, 14);
		getContentPane().add(labelResidencial);
		
		formattedTextField_Residencial = new JFormattedTextField();
		if(telefones[0] != null)
			formattedTextField_Residencial.setText(telefones[0].getDdd() + telefones[0].getNumero());
		formattedTextField_Residencial.setBounds(10, 431, 143, 20);
		getContentPane().add(formattedTextField_Residencial);
		
		labelCelular = new JLabel("Celular");
		labelCelular.setBounds(163, 411, 86, 14);
		getContentPane().add(labelCelular);
		
		formattedTextField_Celular = new JFormattedTextField();
		if(telefones[1] != null)
			formattedTextField_Celular.setText(telefones[1].getDdd() + telefones[1].getNumero());
		formattedTextField_Celular.setBounds(163, 431, 143, 20);
		getContentPane().add(formattedTextField_Celular);
		
		labelComercial = new JLabel("Comercial");
		labelComercial.setBounds(316, 411, 86, 14);
		getContentPane().add(labelComercial);
		
		formattedTextField_Comercial = new JFormattedTextField();
		if(telefones[2] != null)
			formattedTextField_Comercial.setText(telefones[2].getDdd() + telefones[2].getNumero());
		formattedTextField_Comercial.setBounds(316, 431, 143, 20);
		getContentPane().add(formattedTextField_Comercial);
		
		/** BOTOES */
		
		JButton button_Atualizar = new JButton("Atualizar");
		button_Atualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Endereco endereco = new Endereco(textField_Logradouro.getText(), textField_Numero.getText(), 
						textField_Bairro.getText(), textField_Cidade.getText(), (String)comboBox_Uf.getSelectedItem(), 
						formattedTextField_Cep.getText().replaceAll("\\.", "").replaceAll("-", ""));
				
				JTextFieldDateEditor chooser_DataFundacaoEditor = (JTextFieldDateEditor) chooser_DataFundadacao.getDateEditor();
			try {
				controlador = new ControladorPessoaJuridica();					
				controlador.atualizar(
						textField_RazaoSocial.getText(),
						chooser_DataFundacaoEditor.getText(),
						formattedTextField_cnpj.getText().replaceAll("-", "").replaceAll("\\.", "").replaceAll("/", ""),
						textField_AreaAtuacao.getText(), 
						endereco,
						formattedTextField_Residencial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", ""),
						formattedTextField_Celular.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", ""),
						formattedTextField_Comercial.getText().replaceAll("-", "").replaceAll("[()]", "").replaceAll(" ", ""));
				
				alert = new AlertDialog("Cliente atualizado com sucesso!");
				alert.abrir();
				dispose();
			} catch (SQLException ex) {
				alert = new AlertDialog("Erro ao atualizar cliente!");
				alert.abrir();
				ex.printStackTrace();
			}
			}
		});
		button_Atualizar.setBounds(671, 527, 104, 23);
		getContentPane().add(button_Atualizar);
		
		JButton button_Deletar = new JButton("Deletar");
		button_Deletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					controlador = new ControladorPessoaJuridica();
					controlador.deletar(pessoaJuridica.getCnpj());
					alert = new AlertDialog("Cliente deletado com sucesso!");
					alert.abrir();
					dispose();
				} catch (SQLException e) {
					alert = new AlertDialog("Falha ao deletar cliente!");
					alert.abrir();
					e.printStackTrace();
				}
				
			}
		});
		button_Deletar.setBounds(572, 527, 89, 23);
		getContentPane().add(button_Deletar);
		
		JButton button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		button_Cancelar.setBounds(473, 527, 89, 23);
		getContentPane().add(button_Cancelar);
	}
}
