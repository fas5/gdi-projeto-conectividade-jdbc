package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import controladores.ControladorRelatorios;
import entidades.RelatorioAtendimento;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RelatorioPorDataPanel extends JPanel {

	private CardLayout cardLayout;
	
	private JPanel contentPanel;
	
	private JLabel labelRelatorioPorData;
	private JLabel labelDataInicial;
	private JLabel labelDataFinal;
	
	private JDateChooser dataInicial;
	private JDateChooser dataFinal;
	
	private JTable table;
	private DefaultTableModel dtm;
	
	private JButton buttonGerar;
	
	private AlertDialog alerta;
	
	/**
	 * Create the panel.
	 */
	public RelatorioPorDataPanel(CardLayout cardLayout, JPanel contentPanel) {
		setLayout(null);
		
		this.cardLayout = cardLayout;
		this.contentPanel = contentPanel;
		
		labelRelatorioPorData = new JLabel("Relat�rio de Atendimentos (Por Data)");
		labelRelatorioPorData.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelRelatorioPorData.setBounds(10, 33, 765, 31);
		add(labelRelatorioPorData);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 75, 765, 15);
		add(separator);
		
		labelDataInicial = new JLabel("Data Inicial");
		labelDataInicial.setBounds(243, 101, 130, 14);
		add(labelDataInicial);
		
		dataInicial = new JDateChooser();
		dataInicial.setBounds(243, 120, 130, 20);
		dataInicial.setMaxSelectableDate(new Date());
		JTextFieldDateEditor dataInicialEditor = (JTextFieldDateEditor) dataInicial.getDateEditor();
		dataInicialEditor.setEditable(false);
		add(dataInicial);

		labelDataFinal = new JLabel("Data Final");
		labelDataFinal.setBounds(383, 101, 130, 14);
		add(labelDataFinal);
		
		dataFinal = new JDateChooser();
		dataFinal.setBounds(383, 120, 130, 20);
		dataFinal.setMaxSelectableDate(new Date());
		JTextFieldDateEditor dataFinalEditor = (JTextFieldDateEditor) dataFinal.getDateEditor();
		dataFinalEditor.setEditable(false);
		add(dataFinal);		
		
		buttonGerar = new JButton("Gerar");
		buttonGerar.setBounds(322, 151, 104, 23);
		add(buttonGerar);
		
		table = new JTable();
		dtm = new DefaultTableModel(0, 0);
		Vector<String> columnNames = new Vector<String>();
		columnNames.add("Pessoa Nome");
		columnNames.add("Escrevente Nome");
		columnNames.add("Escrevente Matricula");
		columnNames.add("Servi�o Descricao");
		columnNames.add("Servi�o Preco");
		columnNames.add("Servi�o Codigo");
		columnNames.add("Atendimento Quantidade");
		columnNames.add("Atendimento Desconto");
		columnNames.add("Presta Data e Hora");
		dtm.setColumnIdentifiers(columnNames);
		table.setModel(dtm);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(10, 221, 765, 328);
		table.setBounds(10, 250, 1000, 305);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		scrollPane.setViewportView(table);
		add(scrollPane);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 195, 767, 15);
		add(separator_1);
			
		buttonGerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControladorRelatorios controlador = new ControladorRelatorios();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				String initialDate = dateFormat.format(dataInicial.getDate()).replaceAll("-", "/");
				String endDate = dateFormat.format(dataFinal.getDate()).replaceAll("-", "/");
				if(initialDate != null && endDate != null){
					try {
						for(int i = dtm.getRowCount() - 1; i >=0; i--)
						{
							dtm.removeRow(i); 
						}						
						List<RelatorioAtendimento> resultado = controlador.buscarPorData(initialDate, endDate);
						if(resultado.size() == 0){
							alerta = new AlertDialog("N\u00E3o Encontrado!");
							alerta.setVisible(true);
						}else{
							for (int i = 0; i < resultado.size(); i++) {
						        dtm.addRow(new Object[] { resultado.get(i).getPfNome(), resultado.get(i).geteNome(),
						        		resultado.get(i).geteMatricula(), resultado.get(i).getsDescricao(),
						        		resultado.get(i).getsPreco(), resultado.get(i).getsCodigo(),
						        		resultado.get(i).getaQuantidade(), resultado.get(i).getaDesconto(),
						        		resultado.get(i).getPrDataHora()});
							}
						}
						
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else{
//					Validacao
				}
			}
		});
		
	}
}
