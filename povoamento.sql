DELETE FROM tb_pessoafisica;
DELETE FROM tb_pessoajuridica;
DELETE FROM tb_atendimento;
DELETE FROM tb_escrevente;
DELETE FROM tb_servico_escrevente_cliente;
DELETE FROM tb_tabeliao;
DELETE FROM tb_servico;

DROP SEQUENCE tb_pessoa_seq;
DROP SEQUENCE tb_servico_seq;
DROP SEQUENCE tb_atendimento_seq;

CREATE SEQUENCE tb_pessoa_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

CREATE SEQUENCE tb_servico_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

CREATE SEQUENCE tb_atendimento_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

-- PESSOA FISICA
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Jean Morgan', TO_DATE('02/01/1968','DD/MM/YYYY'),tp_endereco('Ramsey', '41', 'Casa Amarela', 'Recife', 'PE', '18514'),tp_telefones(tp_telefone('81', '79029366')),'67840765750','M', 13500.00,'Odontologista'));
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Lillian Kennedy', TO_DATE('30/04/1969','DD/MM/YYYY'),tp_endereco('Bay', '415', 'Leblon', 'Rio de Janeiro', 'RJ', '91841'),tp_telefones(tp_telefone('81', '25272378')),'15840190004','F', 10000.00,'Professor'));
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Sara Palmer', TO_DATE('30/07/1988','DD/MM/YYYY'),tp_endereco('Columbus', '79', 'Casa Amarela', 'Recife', 'PE', '88006'),tp_telefones(tp_telefone('81','96557768'),tp_telefone('81', '59461623')),'87583120217','F', 25000.00,'Juiz'));
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Brandon Hall', TO_DATE('15/03/1940','DD/MM/YYYY'),tp_endereco('Pierstorff', '0', 'Cordeiro', 'Recife', 'PE', '21275'),tp_telefones(tp_telefone('81', '77026709'),tp_telefone('81', '75893665')),'17562045607','M', 800.00,'Vendedor'));
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Jason Stewart', TO_DATE('13/12/1973','DD/MM/YYYY'),tp_endereco('Maryland', '28115', 'Casa Forte', 'Recife', 'PE', '22234'),tp_telefones(tp_telefone('81', '94182746')),'80664194303','M', 2300.00,'Motorista'));

INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Ruth Thompson', TO_DATE('23/10/1959','DD/MM/YYYY'),tp_endereco('Northfield', '71922', 'Casa Amarela', 'Recife', 'PE', '27635'),tp_telefones(tp_telefone('81', '18652927'),tp_telefone('81','28648654')),'93327767920','F', 4700.00,'Taxista'));
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Dorothy Smith', TO_DATE('28/11/1967','DD/MM/YYYY'),tp_endereco('Maryland', '1026', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ', '70124'),tp_telefones(tp_telefone('81', '40322602'),tp_telefone('81', '91358449')),'73434294422','F', 1400.00,'Autonomo'));
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Brenda Phillips', TO_DATE('05/06/1997','DD/MM/YYYY'),tp_endereco('Shopko', '79926', 'Cordeiro', 'Recife', 'PE', '20546'),tp_telefones(tp_telefone('81', '90231379')),'84181765806','F', 764.00,'Auxiliar de Servico'));
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Heather Perry', TO_DATE('08/02/1977','DD/MM/YYYY'),tp_endereco('Maywood', '2', 'Rosarinho', 'Recife', 'PE', '06510'),tp_telefones(tp_telefone('81', '55747495')),'99404346420','F', 3000.00,'Designer'));
INSERT INTO tb_pessoafisica VALUES (tp_pessoafisica(tb_pessoa_seq.NEXTVAL, 'Sean Chavez', TO_DATE('14/08/1965','DD/MM/YYYY'),tp_endereco('Wayridge', '7249', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ', '79977'),tp_telefones(tp_telefone('81', '66118486')),'08455486465','M', 1800.00,'Aposentado'));

-- PESSOAJURIDICA
INSERT INTO tb_pessoajuridica VALUES (tp_pessoajuridica(tb_pessoa_seq.NEXTVAL,'Catherine Watkins',TO_DATE('24/07/2005','DD/MM/YYYY'),tp_endereco('Shelley', '623', 'Cordeiro', 'Recife', 'PE', '90076'),tp_telefones(tp_telefone('81','51576279'),tp_telefone('81','42868144')),'97146637000122','Saúde'));
INSERT INTO tb_pessoajuridica VALUES (tp_pessoajuridica(tb_pessoa_seq.NEXTVAL,'Juan Freeman',TO_DATE('12/08/1970','DD/MM/YYYY'),tp_endereco('Rutledge', '206', 'Engenho do Meio', 'Recife', 'PE', '29615'),tp_telefones(tp_telefone('81','65857383')),'22313475000170','Educação'));
INSERT INTO tb_pessoajuridica VALUES (tp_pessoajuridica(tb_pessoa_seq.NEXTVAL,'Susan Adams',TO_DATE('17/12/1978','DD/MM/YYYY'),tp_endereco('Parkside', '4', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ', '98166'),tp_telefones(tp_telefone('81','70526408')),'83819161000139','Beleza'));
INSERT INTO tb_pessoajuridica VALUES (tp_pessoajuridica(tb_pessoa_seq.NEXTVAL,'Sean Cunningham',TO_DATE('15/10/2001','DD/MM/YYYY'),tp_endereco('8th', '23023', 'Casa Forte', 'Recife', 'PE', '55811'),tp_telefones(tp_telefone('81','74156977')),'53813643000110','Estética'));
INSERT INTO tb_pessoajuridica VALUES (tp_pessoajuridica(tb_pessoa_seq.NEXTVAL,'Robin Johnson',TO_DATE('31/05/1984','DD/MM/YYYY'),tp_endereco('Sloan', '95', 'Varzea', 'Recife', 'PE', '77713'),tp_telefones(tp_telefone('81','40829961'),tp_telefone('81','53536042'),tp_telefone('81','40028279')),'15798820000148','Varejo'));

--ESCREVENTE
INSERT INTO tb_escrevente VALUES (tp_escrevente(tb_pessoa_seq.NEXTVAL,'Frank Berry',TO_DATE('04/07/1966','DD/MM/YYYY'),tp_endereco('Vernon', '2', 'Leblon', 'Rio de Janeiro', 'RJ', '93111'),tp_telefones(tp_telefone('81','46272400'),tp_telefone('81','90104279')),4, null,3000.00,TO_DATE('02/04/1988','DD/MM/YYYY'), null));
INSERT INTO tb_escrevente VALUES (tp_escrevente(tb_pessoa_seq.NEXTVAL,'Jacqueline Price',TO_DATE('06/06/1952','DD/MM/YYYY'),tp_endereco('Huxley', '5485', 'Cordeiro', 'Recife', 'PE', '21239'),tp_telefones(tp_telefone('81','77363353')),5,(select ref(e) from tb_escrevente e where (e.matricula = 4)),3000.00,TO_DATE('10/06/1992','DD/MM/YYYY'), null));
INSERT INTO tb_escrevente VALUES (tp_escrevente(tb_pessoa_seq.NEXTVAL,'Katherine Webb',TO_DATE('07/07/1960','DD/MM/YYYY'),tp_endereco('Heath', '0089', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ', '52809'),tp_telefones(tp_telefone('82','95993928'), tp_telefone('82','80884206'),tp_telefone('82','74791534')),6,(select ref(e) from tb_escrevente e where (e.matricula = 4)),3500.00,TO_DATE('07/12/1985','DD/MM/YYYY'), null));
INSERT INTO tb_escrevente VALUES (tp_escrevente(tb_pessoa_seq.NEXTVAL,'Ruby Palmer',TO_DATE('02/12/1977','DD/MM/YYYY'),tp_endereco('Debs', '036', 'Cordeiro', 'Recife', 'PE', '73157'),tp_telefones(tp_telefone('82','73098816')),7, (select ref(e) from tb_escrevente e where (e.matricula = 4)),3000.00,TO_DATE('04/05/1991','DD/MM/YYYY'),null));

-- SERVICO
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Registro de Títulos', 500, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Reconhecimento de Firma', 40, 1));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Abertura de Firma', 100, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Autenticação de Documentos', 20, 1));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Certidão de Casamento', 400, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, '2 Via - Certidão de Casamento', 1000, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Certidão de Divórcio', 2000, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, '2 Via - Certidão de Divórcio', 4000, 0));

INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Certidão de Nascimento', 0, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, '2 Via - Certidão de Nascimento', 200, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Certidão de Óbito', 0, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, '2 Via - Certidão de Óbito', 100, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Procuração', 800, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Escritura de Imóvel', 15000, 0));
INSERT INTO tb_servico VALUES ( tp_servico(tb_servico_seq.NEXTVAL, 'Xerox', 0.5, 1));

-- ATENDIMENTO
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 3, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) ); 
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 2, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 15, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 10, 10, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );	
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 11, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );	
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 13, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 7)) ) );	
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 15, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 5)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 14, 30, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 6)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 15, 30, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 6)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 5, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );	
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 6, 5, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );  
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 7, 20, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );	
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 9, 10, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 5, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 1, 10, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 5)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 15, 10, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 5)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 4, 20, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 10)) ) );

INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 4, 12, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 8)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 15, 16, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 8)) ) ); 
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 14, 20, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 15, 17, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 10)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 4, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 10)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 8, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 4, 10, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 12, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 4, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 15, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 9)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 2, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 15, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 6)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 10, 10, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 2, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 4, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 11)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 4, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 3)) ) );

INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 4, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 2)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 13, 20, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) ); 
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 12, 15, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) ); 
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 11, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 10, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) ); 
INSERT INTO tb_atendimento VALUES ( tp_atendimento(tb_atendimento_seq.NEXTVAL, 9, 0, (SELECT REF(P) FROM tb_servico P WHERE (P.codigo = 1)) ) );

-- SERVICO-ESCREVENTE_CLIENTE
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 17)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 1)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=2)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=2)), TO_DATE('21/04/2015 08:30','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 17)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 14)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=2)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=2)), TO_DATE('21/04/2015 08:30','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 16)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 11)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=2)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=2)), TO_DATE('22/04/2015 10:30','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 18)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 4)),  ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=6)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=6)), TO_DATE('22/03/2015 10:30','DD/MM/YYYY HH24:MI')) );

INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 18)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 5)),  ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=5)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=5)), TO_DATE('22/03/2015 12:05','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 19)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 6)),  ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=7)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=7)), TO_DATE('22/04/2015 14:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 19)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 35)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=7)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=7)), TO_DATE('22/03/2015 14:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 18)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 8)),  ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=14)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=14)), TO_DATE('23/01/2015 08:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 18)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 20)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=14)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=14)), TO_DATE('23/02/2015 08:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 18)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 10)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=14)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=14)), TO_DATE('23/03/2015 08:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 18)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 12)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=14)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=14)), TO_DATE('23/02/2015 08:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 16)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 13)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=15)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=15)), TO_DATE('23/05/2015 10:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 16)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 15)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=8)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=8)), TO_DATE('23/06/2015 10:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 17)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 23)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=5)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=5)), TO_DATE('23/08/2015 16:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 17)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 25)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=5)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=5)), TO_DATE('23/02/2015 16:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 18)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 36)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=5)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=5)), TO_DATE('23/03/2015 17:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 17)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 30)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=14)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=14)), TO_DATE('24/04/2015 10:30','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 16)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 37)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=11)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=11)), TO_DATE('24/05/2015 12:05','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 19)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 38)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=10)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=10)), TO_DATE('24/06/2015 14:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_servico_escrevente_cliente VALUES ( tp_servico_escrevente_cliente((SELECT REF(P) FROM tb_escrevente P WHERE (P.id = 18)), (SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 39)), ((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=8)UNION(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=8)), TO_DATE('25/03/2015 14:45','DD/MM/YYYY HH24:MI')) );

-- TABELIAO

INSERT INTO tb_tabeliao VALUES (
	tp_tabeliao(
		tb_pessoa_seq.NEXTVAL,'Pamela Chavez', 
		TO_DATE('21/04/1968','DD/MM/YYYY'),
		tp_endereco('Comanche', '51', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ', '55428'),
		tp_telefones(tp_telefone('81', '36409450')), 
		1, null,
		13000.00, 
		TO_DATE('08/08/1987','DD/MM/YYYY'),
		null,
		'Tabeliao chefe', 
		tp_nt_sec(
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 1)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 14)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 11)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 11)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 8)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 20)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 10)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 12)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 23)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 25)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 30)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 39))
		)
	)
);


INSERT INTO tb_tabeliao VALUES (
	tp_tabeliao(
		tb_pessoa_seq.NEXTVAL, 'Susan Miller',
		TO_DATE('18/09/1990','DD/MM/YYYY'), 
		tp_endereco('Reindahl', '51605', 'Casa Amarela', 'Recife', 'PE', '63180'),
		tp_telefones(tp_telefone('81', '65162309'), tp_telefone('81', '34230777'), tp_telefone('81', '92406849')),
		2,(SELECT REF(X) FROM tb_tabeliao X WHERE Matricula = 1),
		10000.00, 
		TO_DATE('15/09/1992','DD/MM/YYYY'),
		null,
		'Primeiro substituto',
		tp_nt_sec((SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 4)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 5)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 13)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 36)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 37))
		)
	)
);
	
INSERT INTO tb_tabeliao VALUES (
	tp_tabeliao(
		tb_pessoa_seq.NEXTVAL, 'Kimberly Miller',
		TO_DATE('08/08/1983','DD/MM/YYYY'), 
		tp_endereco('Derek', '58546', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ', '59623'), 
		tp_telefones(tp_telefone('81', '30075281'), tp_telefone('81', '48355285'), tp_telefone('81', '25956967')),
		3,(SELECT REF(X) FROM tb_tabeliao X WHERE Matricula = 1),
		7000.00,
		TO_DATE('14/09/1990','DD/MM/YYYY'),
		null,
		'Segundo substituto',
		tp_nt_sec((SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 6)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 35)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 15)),
		(SELECT REF(X) FROM tb_servico_escrevente_cliente X WHERE (X.Atendimento.Id = 38))
		)
	)
);

-- PAGAMENTO
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 1)), TO_DATE('21/04/2015 11:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 2)), TO_DATE('21/04/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 3)), TO_DATE('21/04/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 4)), TO_DATE('22/03/2015 11:30','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 5)), TO_DATE('22/03/2015 12:35','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 6)), TO_DATE('22/04/2015 15:25','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 7)), TO_DATE('23/03/2015 09:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 8)), TO_DATE('23/01/2015 10:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 9)), TO_DATE('24/02/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 10)), TO_DATE('23/03/2015 10:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 11)), TO_DATE('22/04/2015 11:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 12)), TO_DATE('23/02/2015 10:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 13)), TO_DATE('23/05/2015 11:45','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 14)), TO_DATE('21/04/2015 11:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 15)), TO_DATE('23/06/2015 12:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 16)), TO_DATE('25/03/2015 10:15','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 17)), TO_DATE('25/02/2015 10:15','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 18)), TO_DATE('26/05/2015 11:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 19)), TO_DATE('26/06/2015 11:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 20)), TO_DATE('23/02/2015 10:45','DD/MM/YYYY HH24:MI')) );

INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 21)), TO_DATE('26/06/2015 17:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 22)), TO_DATE('26/04/2015 17:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 23)), TO_DATE('23/08/2015 18:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 24)), TO_DATE('27/02/2015 12:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 25)), TO_DATE('23/02/2015 18:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 26)), TO_DATE('27/05/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 27)), TO_DATE('20/02/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 28)), TO_DATE('21/03/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 29)), TO_DATE('21/02/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 30)), TO_DATE('24/04/2015 10:55','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 31)), TO_DATE('21/01/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 32)), TO_DATE('30/03/2015 09:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 33)), TO_DATE('01/04/2015 10:05','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 34)), TO_DATE('01/05/2015 10:05','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 35)), TO_DATE('22/03/2015 15:25','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 36)), TO_DATE('23/03/2015 18:00','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 37)), TO_DATE('24/05/2015 15:05','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 38)), TO_DATE('24/06/2015 15:15','DD/MM/YYYY HH24:MI')) );
INSERT INTO tb_pagamento VALUES ( tp_pagamento((SELECT REF(P) FROM tb_atendimento P WHERE (P.id = 39)), TO_DATE('25/03/2015 16:10','DD/MM/YYYY HH24:MI')) );

-- PRESTA
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='2'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='13')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='13')),TO_DATE('21/04/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='3'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='13')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='13')),TO_DATE('21/04/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='19'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='7'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='10')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='10')),TO_DATE('23/03/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='9'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='10')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='10')),TO_DATE('24/02/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='16'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='4')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='4')),TO_DATE('25/03/2015 09:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='17'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='4')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='4')),TO_DATE('25/02/2015 09:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='18'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='4')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='4')),TO_DATE('26/05/2015 10:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='19'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='4')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='4')),TO_DATE('26/06/2015 10:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='21'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='2')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='2')),TO_DATE('26/06/2015 16:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='19'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='22'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='2')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='2')),TO_DATE('26/04/2015 16:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='19'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='24'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='7')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='7')),TO_DATE('27/02/2015 11:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='26'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='9')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='9')),TO_DATE('27/05/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='27'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='13')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='13')),TO_DATE('20/02/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='28'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='13')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='13')),TO_DATE('21/03/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='29'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='3')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='3')),TO_DATE('21/02/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='31'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='11')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='11')),TO_DATE('21/01/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='32'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='7')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='7')),TO_DATE('30/03/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='33'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='7')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='7')),TO_DATE('01/04/2015 09:35','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='34'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='7')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='7')),TO_DATE('01/05/2015 09:35','DD/MM/YYYY HH24:MI')));

INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='1'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='2')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='2')),TO_DATE('21/04/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='14'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='2')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='2')),TO_DATE('21/04/2015 08:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='11'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='2')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='2')),TO_DATE('22/04/2015 10:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='4'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='6')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='6')),TO_DATE('22/03/2015 10:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='5'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='5')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='5')),TO_DATE('22/03/2015 12:05','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='19'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='6'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='7')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='7')),TO_DATE('22/04/2015 14:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='19'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='35'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='7')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='7')),TO_DATE('22/03/2015 14:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='8'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id=' 14')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id=' 14')),TO_DATE('23/01/2015 08:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='20'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='14')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='14')),TO_DATE('23/02/2015 08:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='10'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='14')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='14')),TO_DATE('23/03/2015 08:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='12'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='14')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='14')),TO_DATE('23/02/2015 08:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='13'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='15')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='15')),TO_DATE('23/05/2015 10:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='15'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='8')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='8')),TO_DATE('23/06/2015 10:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='23'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='5')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='5')),TO_DATE('23/08/2015 16:00','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='25'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='5')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='5')),TO_DATE('23/02/2015 16:00','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='36'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='5')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='5')),TO_DATE('23/03/2015 17:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='17'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='30'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='14')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='14')),TO_DATE('24/04/2015 10:30','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='16'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='37'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='11')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='11')),TO_DATE('24/05/2015 12:05','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='19'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='38'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='10')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='10')),TO_DATE('24/06/2015 14:45','DD/MM/YYYY HH24:MI')));
INSERT INTO tb_presta VALUES (tp_presta((SELECT REF(G) FROM tb_escrevente G WHERE Id ='18'),(SELECT REF(H) FROM tb_atendimento H WHERE Id='39'),((SELECT REF(X) FROM tb_pessoafisica X WHERE Id='8')UNION ALL(SELECT REF(X) FROM tb_pessoajuridica X WHERE Id='8')),TO_DATE('25/03/2015 14:45','DD/MM/YYYY HH24:MI')));

commit;