-- CRIACAO DAS TABELAS
DROP SEQUENCE tb_endereco_seq;
DROP SEQUENCE tb_pessoa_seq;
DROP SEQUENCE tb_servico_seq;
DROP SEQUENCE tb_atendimento_seq;

DROP TABLE tb_pessoafisica;
DROP TABLE tb_pessoajuridica;
DROP TABLE tb_tabeliao;
DROP TABLE tb_escrevente;
DROP TABLE tb_servico;
DROP TABLE tb_atendimento;
DROP TABLE tb_servico_escrevente_cliente;
DROP TABLE tb_pagamento;
DROP TABLE tb_presta;

CREATE SEQUENCE tb_pessoa_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

CREATE SEQUENCE tb_servico_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

CREATE SEQUENCE tb_atendimento_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

-- PESSOA FISICA
CREATE TABLE tb_pessoafisica OF tp_pessoafisica (
	Id PRIMARY KEY,
	Nome NOT NULL,
	DataNascimento NOT NULL,
	Endereco NOT NULL,
	Telefones NOT NULL,
	CPF NOT NULL,
	Sexo NOT NULL,
	Renda NOT NULL,
	Profissao NOT NULL
);	

-- PESSOA JURIDICA
CREATE TABLE tb_pessoajuridica OF tp_pessoajuridica (
	Id PRIMARY KEY,
	Nome NOT NULL,
	DataNascimento NOT NULL,
	Endereco NOT NULL,
	Telefones NOT NULL,
	CNPJ NOT NULL,
	AreaAtuacao NOT NULL
);

-- TABELIAO
CREATE TABLE tb_tabeliao OF tp_tabeliao (
  	Id PRIMARY KEY,
	Funcao NOT NULL
 ) NESTED TABLE ServicoEscreventeCliente STORE AS tb_servicos_tabeliao;


-- ESCREVENTE
CREATE TABLE tb_escrevente OF tp_escrevente (
	Id PRIMARY KEY,
	Nome NOT NULL,
	DataNascimento NOT NULL,
	Endereco NOT NULL,
	Telefones NOT NULL,
	Matricula UNIQUE,
	Supervisor NULL,
	Salario NOT NULL,
	DataAdmissao NOT NULL
);

-- SERVICO
CREATE TABLE tb_servico OF tp_servico (
	Codigo PRIMARY KEY,
	Descricao NOT NULL,
	Preco NOT NULL,
	Autorizado NOT NULL
);

-- ATENDIMENTO
CREATE TABLE tb_atendimento OF tp_atendimento (
	Id PRIMARY KEY,
	Quantidade NOT NULL,
	Desconto NOT NULL,
	Servico NOT NULL
);

-- SERVICO-ESCREVENTE-CLIENTE
CREATE TABLE tb_servico_escrevente_cliente OF tp_servico_escrevente_cliente (
	Escrevente NOT NULL,
	Atendimento NOT NULL,
	Cliente NOT NULL
);

-- PAGAMENTO
CREATE TABLE tb_pagamento OF tp_pagamento (
	Atendimento NOT NULL,
	Datahora NOT NULL
);

-- PRESTA
CREATE TABLE tb_presta OF tp_presta (
	Escrevente NOT NULL,
	Atendimento NOT NULL,
	Cliente NOT NULL,
	DataHora NOT NULL
);